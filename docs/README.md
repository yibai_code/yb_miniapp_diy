# 壹佰前端-小程序DIY项目说明

### 条件编译

此项目目标是整合公司各个应用的后台小程序DIY模块,然后根据应用不同,打包发布不同的功能模块,因此引入了条件编译机制

此功能是基于js-conditional-compile-loader 使用注释的方式进行条件编译 使用方法如下: 

```js
    /* IFTRUE_isV2 */
      console.log('this is V2 !!!!!!')
    /* FITRUE_isV2 */

    /* IFTRUE_isQingzhan */
     console.log('this is qingzhan !!!!!!')
    /* FITRUE_isQingzhan */

    /* IFTRUE_定义的编译变量 */
     ...具体代码
    /* FITRUE_定义的编译变量 */
```

编译变量在vue.config.js中chainWebpack->js-conditional-compile-loader 中定义,可根据实际项目情况添加修改 

当前项目条件编译变量:

|变量|说明|
|----|----|
|isV2|当前是门店V2的编译环境|
|isQingzhan|当前是轻站的编译环境|
|isVideo|当前是短视频的编译环境|
|isMerchant|当前是多商户的编译环境|

### 运行打包

|命令|说明|
|----|----|
|npm run clean|清除编译文件缓存,开发模式时,切换编译环境前需执行,否则条件编译无法生效|
|npm run serve:v2|运行门店V2编译环境的开发模式|
|npm run serve:qingzhan|运行轻站编译环境的开发模式|
|npm run serve:video|运行短视频编译环境的开发模式|
|npm run serve:merchant|运行多商户编译环境的开发模式|
|npm run build:v2|打包门店V2的正式包|
|npm run build:qingzhan|打包轻站的正式包|
|npm run build:video|打包短视频的正式包|
|npm run build:merchant|打包多商户的正式包|

### 文件结构
* mock<br/>
模拟接口请求, 所有的前端模拟接口都写在mock/controller中, 以模块名称, 页面名称命名
具体写法参照已有实例<br/>

* api<br/>
项目中用到的接口, 参照已有文件书写, 注释和传参必须写清楚, 接口返回的是Promise对象, 具体使用如下<br/>

```js
import { getList } from '@/api/xxxx'
 getList().then((res) => {
    console.log('res') 
}).catch((err) => {
    console.log('err')
})
```

* assets<br/>
图片等资源放这里

* components<br/>
全局通用组件, 一般不会更改

* config

项目配置文件

|文件|说明|
|:----|:----|
|constant.js|静态枚举常量, 定义了弹窗的类型, 位置等信息|
|EveBusKeys.js|静态枚举常量, 定义了全局消息通知, 方便查看哪个通知 哪里发起 那里接收|
|WegtConfig.js|静态枚举常量, 定义了组件类型字符串和组件模板的对应关系, 方便查看项目具体有哪些组件|
|WegtStyles.js|静态枚举常量, 定义了右侧编辑弹窗可编辑选项以及全部可编辑样式|
   
   
* layouts<br/>
页面布局文件, 一般不会改动

* plugins<br/>
全局使用的插件等处理, 一般不会改动

* store<br/>
vuex store文件, 一般不需要改动

* svg<br/>
项目中用到的svg文件, 可以新增, 不可以删除, 搭配Icons组件使用,使用方法如下:<br/>

```vue
<Icons :content="import('@/svg/xxx.svg')"></Icons>
```

* utils<br/>
公共方法等, 一般不需要改动

* wegts<br/>
小程序组件 每个组件一个文件夹 文件以及文件命名如下:

index.vue 组件 

data.vue 数据编辑 

style.vue 高级样式编辑 

type.vue 风格切换编辑

* dev.config.js

开发模式后台接口请求地址和PHPSESSID定义 原来放在.env.development中, 更改后必须重新运行项目, 现在改为单独配置, 更改后立即生效

* .env.development<br/>
线上调试模式配置文件

* .env.mock<br/>
本地模拟接口调试配置文件

* .env.production<br/>
正式打包配置文件

### 具体使用
使用时候, 基本只需要改动一下几个文件夹的内容
1. mock<br/>
接口模拟, 后台没有提供接口时, 模拟数据用 启动时 需要用 npm run serve:mock
2. api<br/>
接口整合, 按模块,页面分好
4. wegts<br/>
具体页面文件
5. .env.development<br/>
线上调试模式配置文件 启动时 需要用 npm run serve

### 全局方法
* 全局加载层
```js
this.$baseLoading()
this.$baseLoading('正在xxx,请等待...')
```
* 全局加载层关闭
```js
this.$baseLoadingClose()
```
* 全局弹出信息提示 默认是MessageType.success
```js
this.$baseMessage('xxxxxxxx', MessageType.info)
this.$baseMessage('xxxxxxxx', MessageType.success)
this.$baseMessage('xxxxxxxx', MessageType.warning)
this.$baseMessage('xxxxxxxx', MessageType.error).then((_) => {
    //展示完成后的回调
})
```
* 全局弹出Alert, 标题默认是: 温馨提示
```js
this.$baseAlert('内容', '标题').then((_) => {
    //展示完成后的回调
})
```
* 全局确认框 标题默认是: 温馨提示 类型默认 MessageType.warning
```js
this.$baseConfirm('内容','标题',MessageType.warning).then((_) => {
    //点击确认的回调
}).catch((_) => {
    //点击取消的回调
})
```
* 全局通知 标题默认是: 温馨提示 类型默认 MessageType.info 默认位置是 NotifyPosition.topRight
```js
this.$baseNotify('内容','标题',MessageType.warning,NotifyPosition.topRight)
```
* 全局弹出框
```js
/**
   * 全局弹出框 弹框的标题是否显示底部按钮 定义到具体的弹出组件中
     底部按钮默认显示, 某些弹窗不想显示底部按钮的, 再定义 dialogFooterShow: false
        export default {
                            title: '弹窗标题',
                            dialogFooterShow: false
                            ....
                        }
   * @param component 具体展示哪个组件 使用import()的方式
   * @param data 携带数据
   * @param width 弹窗宽度
   * @param className 弹窗附加自定义类名
   * @returns {Promise<unknown>} 弹窗默认有确定按钮 点击确定按钮 会执行展示的component中的onSubmit方法, 
   * 如果执行成功, 需要在onSubmit方法中执行 this.callBack && this.callBack(arr) 把需要传出的数据传递出来
   */
this.$baseDialog(import('@/view/xxx/xxx.vue'),  {id: 1}).then((res) => {
    //弹窗中组件执行方法成功后的回调
})
```
* 全局图片选择弹窗 
```js
/**
   * 全局图片选择
   * @param maxNum 最大选择数量
   * @returns {Promise<unknown>}
   */
this.$baseImageChoose(9).then((res) => {
    //选择完毕后的回调
})
```
* 全局位置坐标选择 
```js
/**
   * 全局位置坐标选择
   * @param location {city:'', address:'', lat: '', lng: ''}
   * @returns {Promise<unknown>}
   */
this.$baseLocationChoose().then((res) => {
    //选择完毕后的回调
})
```
* 全局事件总线
```js
this.$baseEventBus.$on('EventName', (data) => {
    //接收到事件后的执行方法
})
// 发送全局事件 所有注册过EventName的地方都可以接收到
this.$baseEventBus.$emit('EventName', {a: 0, b: 1})
```

### 通用组件
#### CKEditer
富文本编辑器
```vue
import Ckeditor from '@/components/Ckeditor'
components: { Ckeditor },
<Ckeditor v-model="form.content"></Ckeditor>
```
#### CommonListBox 
通用列表

```vue
import CommonListBox from '@/components/CommonListBox'
<CommonListBox
      :show-export="true"
      :show-choose="true"
      :add-component="import('./components/add')"  
      :theader="theader"
      :querys="{type: 1}"
      :api-list="apiList"
      :api-delete="apiDelete"
      :api-search="apiSearch"
    >
      <template slot="tool" slot-scope="selectedRow">
        <el-button>添加</el-button>
      </template>

      <template slot="封面图" slot-scope="item">
        <el-image
          :preview-src-list="[item.data.image]"
          :src="item.data.image"
        ></el-image>
      </template>

</CommonListBox>
```
##### Props

|prop|类型|说明|
|:----|:----|:----|
|show-export|Boolean|是否显示导出按钮 默认显示|
|show-choose|Boolean|是否显示列表前面的选择框 默认显示|
|api-list|Function|列表接口|
|api-delete|Function|删除接口|
|api-search|Function|搜索接口|
|add-component|Promise|添加编辑弹窗 默认无 不显示添加编辑 import('xxx.vue')|
|add-permission|Number|添加的权限id|
|edit-permission|Number|编辑的权限id|
|del-permission|Number|删除的权限id|
|querys|Object|获取列表数据时携带的参数|
|theader|Object|需要展示的数据|

##### theader
需要展示的数据 格式如下
```js
[
          {
            label: '文章ID',
            prop: 'id',
            show: true,
          },
          {
            label: '封面图',
            prop: null,
            show: true,
          },
          {
            label: '操作',
            prop: null,
            show: true,
            width: '120px',
            align: 'center',
          },
        ]
```
label: 显示标题<br/>
prop: 对应的数据里面的字段 设置成null的需要自己使用slot处理<br/>
show: 是否显示 默认true就行<br/>
width: 列宽<br/>
align: 列对齐方式 left | center | right<br/>
##### slot
* tool
左侧工具条 会传递所有选择的项
##### 其他
theader 中prop为null的时候, 会定义一个name为theader中label的slot 会把每行数据传递进来 然后自定义处理要显示的内容

```vue
      <template slot="封面图" slot-scope="item">
        <el-image
          :preview-src-list="[item.data.image]"
          :src="item.data.image"
        ></el-image>
      </template>
```

点击编辑按钮时, 会向打开的组件传递{ data: { edit: { id: 0, ... } } }, 组件接收到后,执行编辑的相关方法, 具体可参见文章添加编辑

#### SearchBox 
通用搜索项

```vue
import SearchBox from '@/components/SearchBox'
<SearchBox     
      :api="apiSearch"
      :show="searchShow"
      :on-search-submit="onSearchSubmit"
    ></SearchBox>
```

##### Props

|prop|类型|说明|
|:----|:----|:----|
|api|Function|搜索接口|
|show|Boolean|是否展开显示|
|on-search-submit|Function|确定或取消按钮点击时的回调|

#### CommonExportBar 
搭配列表使用的搜索导出字段控制显示工具条
```vue
import CommonExportBar from '@/components/CommonExportBar'
<CommonExportBar
          v-model="keyword"
          :export-choose="selectRows"
          :api-list="apiList"
          :theader="theader"
          :querys="querys"
        >
</CommonExportBar>
```

##### Props

|prop|类型|说明|
|:----|:----|:----|
|value|String|搜索词|
|theader|Object|表单显示字段|
|exportChoose|Array|导出选定行时的选定行数据|
|apiList|Function|导出全部数据时调用的获取数据的接口 最多获取100万行数据|
|querys|Object|导出全部数据时附加的请求数据|

##### Slot 插槽 
left | right

左侧或右侧的插槽

#### SwitchVal 
数据开关组件
```vue
import SwitchVal from '@/components/SwitchVal'
<SwitchVal
          :key-id="item.data['id']"
          key-flag="status"
          :value="item.data['status']"
          on="2"
          off="1"
          :api="apiSwitch"
        ></SwitchVal>
```
##### Props

|prop|类型|说明|
|:----|:----|:----|
|key-id|Number|数据对象的id|
|key-flag|String|字段标识, 一般和数据库表字段一致|
|value|Number / String|当前值|
|on|Number / String|开启时候的值|
|off|Number / String|关闭时候的值|
|api|Function|接口方法 import自api文件夹|

#### CommonImageSelecter 
表单项中的图片选择组件
```vue
import ImageSelecter from '@/components/CommonImageSelecter'
<ImageSelecter v-model="images" :num="5"></ImageSelecter>
```
##### Props

|prop|类型|说明|
|:----|:----|:----|
|value/v-model|Array|初始化的数据|
|num|Number|最多选择几张图片 默认1张|

value/v-model

初始化的数据 内容格式如下: 
```js
[{id: 1, image: 'http://www.abc.com/ccc.jpg'}]
```

### 组件添加方法

wegts文件夹新增组件文件夹, 新建组件文件index.vue, 初始化内容如下:

```vue
<template>
  <DragDrop
    :id="wegtData.id"
    :drag-drop-disable="dragDropDisable"
    :type="wegtData.type"
    :draggable="false"
    :align="false"
    :rotatable="false"
    :resizable="false"
    class="block"
  >
    <div></div>
  </DragDrop>
</template>

<script>
  import store from '@/store'
  import { SettingItems, StyleEdit } from '@/config/WegtStyles'
  // 组件mixins
  import { WegtMixins } from '@/wegts/WegtMixins'

  export default {
    components: {},
    mixins: [WegtMixins],
    data: function () {
      return {
        // 组件id
        id: '',
        // 组件样式索引 一个组件多个样式时使用
        styleIndex: 0,
        // 初始化样式
        css: [],

        // 组件标题
        title: '宫格导航',
        // 组件右侧可设置栏目
        settingItem: [
          SettingItems.style,
          SettingItems.data,
          SettingItems.animo,
        ],
        // 组件样式可编辑选项
        styleItem: [StyleEdit.bg, StyleEdit.bd, StyleEdit.navigation],
        // 高级样式编辑页面 右侧编辑区会根据此处引用的文件进行显示
        styleSet: import('./style'),
        // 组件数据编辑页面 右侧编辑区会根据此处引用的文件进行显示
        dataSet: import('./data'),        
        // 风格切换编辑页面 右侧编辑区会根据此处引用的文件进行显示
        typeSet: import('./type'),
      }
    },
    watch: {},
    mounted() {},
    methods: {
      /**
       * 初始化样式 WegtMixins里有默认实现 如需自己实现 自行填充initStyle方法
       */
      initStyle() {
         ....
      },
      /**
       * 初始化数据
       * @param index
       */
      initData(index) {
        let item = {
                  id: this.id,      // 组件id
                  type: 'headline', // 组件类型字符串
                  name: '标题名称',
                  style_type: this.styleIndex,
                }
        // 添加数据数据list中
        store.commit('diy/ALL_DATA_ADD', { item: item, index: index })
      },
    },
  }
</script>

```

### 原有组件转换

1. 在老项目中找到定义的组件 一般是在老项目的 core/addons/miniapp/public/js/vue-components.js 
和 core/addons/miniapp/public/js/components文件夹

2. 把原有组件转成成当前项目的单文件组件 

3. 原有组件的初始化样式 一般是在老项目 core/addons/miniapp/public/js/index.js  
中的 window.toolBarVM -> styleInit 方法

4. 原有组件的初始化数据 一般是在老项目 core/addons/miniapp/public/js/index.js  
中的 window.toolBarVM -> doadd 方法

5. 基础样式设置只需要在组件的styleItem中设置即可 基础样式设置包括

|标识|说明|
|----|----|
|StyleEdit.size|位置尺寸|
|StyleEdit.bg|背景|
|StyleEdit.bd|边框|
|StyleEdit.br|圆角|
|StyleEdit.sd|阴影|
|StyleEdit.image|背景图片|
|StyleEdit.font|字体|
|StyleEdit.fontbase|基础字体|

6. 高级样式,数据,风格切换

一. 在组件文件夹内新建:

style.vue 高级样式编辑

data.vue  组件数据编辑

type.vue  风格编辑

二. 在组件中设置:

```js
// 引用样式设置文件
import StyleSet1 from './style1'
import StyleSet2 from './style2'
// 引用数据设置文件
import DataSet from './data'
// 引用风格设置文件
import TypeSet from './type'

// 组件data中:
data: {
    styleSet: [
        {
            name: '样式设置1',
            component: StyleSet1
        },
        {
            name: '样式设置2',
            component: StyleSet2
        }
    ],  
    dataSet: DataSet,
    typeSet: TypeSet
}

```


