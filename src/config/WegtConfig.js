export const Wegts = {
  block: import('@/wegts/common/Block'),
  text: import('@/wegts/common/Text'),
  image: import('@/wegts/common/Image'),
  button: import('@/wegts/common/Button'),
  blankline: import('@/wegts/common/BlankLine'),
  module_tabbar_list: import('@/wegts/common/Tab'),
  image_group1: import('@/wegts/common/ImageGroup'),
  image_group2: import('@/wegts/common/ImageGroup2'),
  album: import('@/wegts/common/Album'),
  album2: import('@/wegts/common/Album2'),
  album3: import('@/wegts/common/Album3'),
  album4: import('@/wegts/common/Album4'),
  gzh_concern: import('@/wegts/common/Concern'),
  diy_qq_advert: import('@/wegts/common/QqAdvert'),
  diy_map: import('@/wegts/common/Map'),
  announcement: import('@/wegts/common/AnnounCement'),
  articles1: import('@/wegts/common/Articles'),
  articles2: import('@/wegts/common/Articles2'),

  /* IFTRUE_isQingzhan
  headline: import('@/wegts/qingzhan/HeadLine'),
  navigation: import('@/wegts/qingzhan/GridNav'),
  diy_form: import('@/wegts/qingzhan/Form'),
  recruit: import('@/wegts/qingzhan/Recruits'),
  vote: import('@/wegts/qingzhan/Vote'),
  product1: import('@/wegts/qingzhan/Product2'),
  product2: import('@/wegts/qingzhan/Product'),
  product3: import('@/wegts/qingzhan/Product3'),
  services1: import('@/wegts/qingzhan/Services'),
  diy_comment: import('@/wegts/qingzhan/Comment'),
  diy_search: import('@/wegts/qingzhan/Search'),
  diy_wx_advert: import('@/wegts/qingzhan/WxAdvert'),
  banner: import('@/wegts/qingzhan/Banner'),
  my_video: import('@/wegts/qingzhan/Video'),
  rich_text: import('@/wegts/v2/RichText'),
   FITRUE_isQingzhan */

  /* IFTRUE_isV2
  headline: import('@/wegts/common/HeadLine'),
  navigation: import('@/wegts/v2/GridNav'),
  navigation2: import('@/wegts/v2/GridNav2'),
  navigation3: import('@/wegts/v2/GridNav3'),
  navigation4: import('@/wegts/v2/GridNav4'),
  diy_form: import('@/wegts/v2/Form'),
  diy_search: import('@/wegts/v2/Search'),
  diy_wx_advert: import('@/wegts/v2/WxAdvert'),
  banner: import('@/wegts/v2/Banner'),
  my_video: import('@/wegts/v2/Video'),
  services1: import('@/wegts/v2/Services'),
  services2: import('@/wegts/v2/Services2'),
  video_list: import('@/wegts/v2/Vine'),
  video_list2: import('@/wegts/v2/Vine2'),
  video_list3: import('@/wegts/v2/Vine3'),
  live_list: import('@/wegts/v2/Live'),
  live_list2: import('@/wegts/v2/Live2'),
  live_list3: import('@/wegts/v2/Live3'),
  live_list4: import('@/wegts/v2/Live4'),
  live_list5: import('@/wegts/v2/Live5'),
  tencent_live_list: import('@/wegts/v2/TencentLive'),
  tencent_live_list2: import('@/wegts/v2/TencentLive2'),
  tencent_live_list3: import('@/wegts/v2/TencentLive3'),
  tencent_live_list4: import('@/wegts/v2/TencentLive4'),
  tencent_live_list5: import('@/wegts/v2/TencentLive5'),
  diy_audio: import('@/wegts/v2/Audio'),
  vote: import('@/wegts/v2/Vote'),
  building_list: import('@/wegts/v2/House'),
  decorate_list: import('@/wegts/v2/Decoration'),
  designer_list: import('@/wegts/v2/Designer'),
  diy_lecturer: import('@/wegts/v2/Instructor'),
  diy_bargain: import('@/wegts/v2/Bargaining'),
  bargaining2: import('@/wegts/v2/Bargaining2'),
  bargaining3: import('@/wegts/v2/Bargaining3'),
  rich_text: import('@/wegts/v2/RichText'),
  diy_seckill: import('@/wegts/v2/Seckill'),
  seckill2: import('@/wegts/v2/Seckill2'),
  seckill3: import('@/wegts/v2/Seckill3'),
  seckill4: import('@/wegts/v2/Seckill4'),
  diy_course: import('@/wegts/v2/Class'),
  diy_series: import('@/wegts/v2/Special'),
  diy_goods_style1: import('@/wegts/v2/Shop'),
  diy_goods_style2: import('@/wegts/v2/Shop2'),
  diy_goods_style3: import('@/wegts/v2/Shop3'),
  diy_room_list: import('@/wegts/v2/HotelRoom'),
  diy_coupon_style1: import('@/wegts/v2/Coupon'),
  diy_coupon_style2: import('@/wegts/v2/Coupon2'),
  diy_integral: import('@/wegts/v2/Integral'),
  diy_integral2: import('@/wegts/v2/Integral2'),
  diy_integral3: import('@/wegts/v2/Integral3'),
  hotelCheckIn: import('@/wegts/v2/HotelCheckIn'),
  house_type_list: import('@/wegts/v2/HouseType'),
  diy_group: import('@/wegts/v2/Groups'),
  diy_group2: import('@/wegts/v2/Groups2'),
  diy_group3: import('@/wegts/v2/Groups3'),
  diy_group4: import('@/wegts/v2/Groups4'),
  diy_group5: import('@/wegts/v2/Groups5'),
  diy_list: import('@/wegts/v2/list'),
  diy_member: import('@/wegts/v2/DiyMember'),
  diy_good_class: import('@/wegts/v2/GoodClassify'),
  diy_new_coupon: import('@/wegts/v2/diy_new_coupon'),
  diy_order: import('@/wegts/v2/diy_order'),
  diy_new_coupon1: import('@/wegts/v2/diy_new_coupon1'),
  diy_new_coupon2: import('@/wegts/v2/diy_new_coupon2'),
   FITRUE_isV2 */

  /* IFTRUE_isCommunity
  headline: import('@/wegts/common/HeadLine'),
  navigation: import('@/wegts/v2/GridNav'),
  diy_form: import('@/wegts/v2/Form'),
  diy_search: import('@/wegts/v2/Search'),
  diy_wx_advert: import('@/wegts/v2/WxAdvert'),
  banner: import('@/wegts/v2/Banner'),
  my_video: import('@/wegts/v2/Video'),
  video_list: import('@/wegts/v2/Vine'),
  video_list2: import('@/wegts/v2/Vine2'),
  video_list3: import('@/wegts/v2/Vine3'),
  live_list: import('@/wegts/v2/Live'),
  live_list2: import('@/wegts/v2/Live2'),
  live_list3: import('@/wegts/v2/Live3'),
  diy_audio: import('@/wegts/v2/Audio'),
  diy_bargain: import('@/wegts/v2/Bargaining'),
  bargaining2: import('@/wegts/v2/Bargaining2'),
  bargaining3: import('@/wegts/v2/Bargaining3'),
  rich_text: import('@/wegts/v2/RichText'),
  diy_seckill: import('@/wegts/v2/Seckill'),
  seckill2: import('@/wegts/v2/Seckill2'),
  seckill3: import('@/wegts/v2/Seckill3'),
  seckill4: import('@/wegts/v2/Seckill4'),
  diy_goods_style1: import('@/wegts/v2/Shop'),
  diy_goods_style2: import('@/wegts/v2/Shop2'),
  diy_goods_style3: import('@/wegts/v2/Shop3'),
  diy_coupon_style1: import('@/wegts/v2/Coupon'),
  diy_coupon_style2: import('@/wegts/v2/Coupon2'),
  diy_integral: import('@/wegts/v2/Integral'),
  diy_integral2: import('@/wegts/v2/Integral2'),
  diy_integral3: import('@/wegts/v2/Integral3'),
  diy_group: import('@/wegts/v2/Groups'),
  diy_group2: import('@/wegts/v2/Groups2'),
  diy_group3: import('@/wegts/v2/Groups3'),
  diy_group4: import('@/wegts/v2/Groups4'),
  diy_group5: import('@/wegts/v2/Groups5'),
   FITRUE_isCommunity */

  /* IFTRUE_isRestaurant
  headline: import('@/wegts/common/HeadLine'),
  navigation: import('@/wegts/v2/GridNav'),
  navigation2: import('@/wegts/v2/GridNav2'),
  navigation3: import('@/wegts/v2/GridNav3'),
  navigation4: import('@/wegts/v2/GridNav4'),
  diy_form: import('@/wegts/v2/Form'),
  diy_search: import('@/wegts/v2/Search'),
  diy_wx_advert: import('@/wegts/v2/WxAdvert'),
  banner: import('@/wegts/v2/Banner'),
  my_video: import('@/wegts/v2/Video'),
  video_list: import('@/wegts/v2/Vine'),
  video_list2: import('@/wegts/v2/Vine2'),
  video_list3: import('@/wegts/v2/Vine3'),
  live_list: import('@/wegts/v2/Live'),
  live_list2: import('@/wegts/v2/Live2'),
  live_list3: import('@/wegts/v2/Live3'),
  diy_audio: import('@/wegts/v2/Audio'),
  diy_bargain: import('@/wegts/v2/Bargaining'),
  bargaining2: import('@/wegts/v2/Bargaining2'),
  bargaining3: import('@/wegts/v2/Bargaining3'),
  rich_text: import('@/wegts/v2/RichText'),
  diy_seckill: import('@/wegts/v2/Seckill'),
  seckill2: import('@/wegts/v2/Seckill2'),
  seckill3: import('@/wegts/v2/Seckill3'),
  seckill4: import('@/wegts/v2/Seckill4'),
  diy_goods_style1: import('@/wegts/v2/Shop'),
  diy_goods_style2: import('@/wegts/v2/Shop2'),
  diy_goods_style3: import('@/wegts/v2/Shop3'),
  diy_coupon_style1: import('@/wegts/v2/Coupon'),
  diy_coupon_style2: import('@/wegts/v2/Coupon2'),
  diy_integral: import('@/wegts/v2/Integral'),
  diy_integral2: import('@/wegts/v2/Integral2'),
  diy_integral3: import('@/wegts/v2/Integral3'),
  diy_group: import('@/wegts/v2/Groups'),
  diy_group2: import('@/wegts/v2/Groups2'),
  diy_group3: import('@/wegts/v2/Groups3'),
  diy_group4: import('@/wegts/v2/Groups4'),
  diy_group5: import('@/wegts/v2/Groups5'),
  diy_new_coupon1: import('@/wegts/v2/diy_new_coupon1'),
  diy_new_coupon2: import('@/wegts/v2/diy_new_coupon2'),
  diy_new_coupon: import('@/wegts/v2/diy_new_coupon'),
   FITRUE_isRestaurant */
  /* IFTRUE_isPlatform
  headline: import('@/wegts/common/HeadLine'),
  navigation: import('@/wegts/v2/GridNav'),
  navigation2: import('@/wegts/v2/GridNav2'),
  navigation3: import('@/wegts/v2/GridNav3'),
  navigation4: import('@/wegts/v2/GridNav4'),
  diy_form: import('@/wegts/v2/Form'),
  diy_search: import('@/wegts/v2/Search'),
  diy_wx_advert: import('@/wegts/v2/WxAdvert'),
  banner: import('@/wegts/v2/Banner'),
  my_video: import('@/wegts/v2/Video'),
  video_list: import('@/wegts/v2/Vine'),
  video_list2: import('@/wegts/v2/Vine2'),
  video_list3: import('@/wegts/v2/Vine3'),
  live_list: import('@/wegts/v2/Live'),
  live_list2: import('@/wegts/v2/Live2'),
  live_list3: import('@/wegts/v2/Live3'),
  diy_audio: import('@/wegts/v2/Audio'),
  diy_bargain: import('@/wegts/v2/Bargaining'),
  bargaining2: import('@/wegts/v2/Bargaining2'),
  bargaining3: import('@/wegts/v2/Bargaining3'),
  rich_text: import('@/wegts/v2/RichText'),
  diy_seckill: import('@/wegts/v2/Seckill'),
  seckill2: import('@/wegts/v2/Seckill2'),
  seckill3: import('@/wegts/v2/Seckill3'),
  seckill4: import('@/wegts/v2/Seckill4'),
  diy_goods_style1: import('@/wegts/v2/Shop'),
  diy_goods_style2: import('@/wegts/v2/Shop2'),
  diy_goods_style3: import('@/wegts/v2/Shop3'),
  diy_coupon_style1: import('@/wegts/v2/Coupon'),
  diy_coupon_style2: import('@/wegts/v2/Coupon2'),
  diy_integral: import('@/wegts/v2/Integral'),
  diy_integral2: import('@/wegts/v2/Integral2'),
  diy_integral3: import('@/wegts/v2/Integral3'),
  diy_group: import('@/wegts/v2/Groups'),
  diy_group2: import('@/wegts/v2/Groups2'),
  diy_group3: import('@/wegts/v2/Groups3'),
  diy_group4: import('@/wegts/v2/Groups4'),
  diy_group5: import('@/wegts/v2/Groups5'),
  diy_new_coupon1: import('@/wegts/v2/diy_new_coupon1'),
  diy_new_coupon2: import('@/wegts/v2/diy_new_coupon2'),
  diy_new_coupon: import('@/wegts/v2/diy_new_coupon'),
  diy_platform_product: import('@/wegts/platform/DiyProduct/index'),
   FITRUE_isPlatform */

  /* IFTRUE_isMerchant
  headline: import('@/wegts/common/HeadLine'),
  navigation: import('@/wegts/v2/GridNav'),
  navigation2: import('@/wegts/v2/GridNav2'),
  navigation3: import('@/wegts/v2/GridNav3'),
  navigation4: import('@/wegts/v2/GridNav4'),
  diy_form: import('@/wegts/v2/Form'),
  diy_search: import('@/wegts/v2/Search'),
  diy_wx_advert: import('@/wegts/v2/WxAdvert'),
  banner: import('@/wegts/v2/Banner'),
  my_video: import('@/wegts/v2/Video'),
  video_list: import('@/wegts/v2/Vine'),
  video_list2: import('@/wegts/v2/Vine2'),
  video_list3: import('@/wegts/v2/Vine3'),
  live_list: import('@/wegts/v2/Live'),
  live_list2: import('@/wegts/v2/Live2'),
  live_list3: import('@/wegts/v2/Live3'),
  diy_audio: import('@/wegts/v2/Audio'),
  diy_bargain: import('@/wegts/v2/Bargaining'),
  bargaining2: import('@/wegts/v2/Bargaining2'),
  bargaining3: import('@/wegts/v2/Bargaining3'),
  rich_text: import('@/wegts/v2/RichText'),
  diy_seckill: import('@/wegts/v2/Seckill'),
  seckill2: import('@/wegts/v2/Seckill2'),
  seckill3: import('@/wegts/v2/Seckill3'),
  seckill4: import('@/wegts/v2/Seckill4'),
  diy_goods_style1: import('@/wegts/v2/Shop'),
  diy_goods_style2: import('@/wegts/v2/Shop2'),
  diy_goods_style3: import('@/wegts/v2/Shop3'),
  diy_goods_style6: import('@/wegts/v2/Shop4'),
  diy_coupon_style1: import('@/wegts/v2/Coupon'),
  diy_coupon_style2: import('@/wegts/v2/Coupon2'),
  diy_integral: import('@/wegts/v2/Integral'),
  diy_integral2: import('@/wegts/v2/Integral2'),
  diy_integral3: import('@/wegts/v2/Integral3'),
  diy_group: import('@/wegts/v2/Groups'),
  diy_group2: import('@/wegts/v2/Groups2'),
  diy_group3: import('@/wegts/v2/Groups3'),
  diy_group4: import('@/wegts/v2/Groups4'),
  diy_group5: import('@/wegts/v2/Groups5'),
  diy_shop_list: import('@/wegts/merchant/ShopList')
   FITRUE_isMerchant */

  /* IFTRUE_isChainstore
  headline: import('@/wegts/common/HeadLine'),
  navigation: import('@/wegts/v2/GridNav'),
  navigation2: import('@/wegts/v2/GridNav2'),
  navigation3: import('@/wegts/v2/GridNav3'),
  navigation4: import('@/wegts/v2/GridNav4'),
  diy_form: import('@/wegts/v2/Form'),
  diy_search: import('@/wegts/v2/Search'),
  diy_newSearch: import('@/wegts/v2/NewSearch'),
  diy_wx_advert: import('@/wegts/v2/WxAdvert'),
  banner: import('@/wegts/v2/Banner'),
  my_video: import('@/wegts/v2/Video'),
  video_list: import('@/wegts/v2/Vine'),
  video_list2: import('@/wegts/v2/Vine2'),
  video_list3: import('@/wegts/v2/Vine3'),
  live_list: import('@/wegts/v2/Live'),
  live_list2: import('@/wegts/v2/Live2'),
  live_list3: import('@/wegts/v2/Live3'),
  diy_audio: import('@/wegts/v2/Audio'),
  diy_bargain: import('@/wegts/v2/Bargaining'),
  bargaining2: import('@/wegts/v2/Bargaining2'),
  bargaining3: import('@/wegts/v2/Bargaining3'),
  rich_text: import('@/wegts/v2/RichText'),
  diy_seckill: import('@/wegts/v2/Seckill'),
  seckill2: import('@/wegts/v2/Seckill2'),
  seckill3: import('@/wegts/v2/Seckill3'),
  seckill4: import('@/wegts/v2/Seckill4'),
  diy_goods_style1: import('@/wegts/v2/Shop'),
  diy_goods_style2: import('@/wegts/v2/Shop2'),
  diy_goods_style3: import('@/wegts/v2/Shop3'),
  diy_goods_style6: import('@/wegts/v2/Shop4'),
  diy_coupon_style1: import('@/wegts/v2/Coupon'),
  diy_coupon_style2: import('@/wegts/v2/Coupon2'),
  diy_integral: import('@/wegts/v2/Integral'),
  diy_integral2: import('@/wegts/v2/Integral2'),
  diy_integral3: import('@/wegts/v2/Integral3'),
  diy_group: import('@/wegts/v2/Groups'),
  diy_group2: import('@/wegts/v2/Groups2'),
  diy_group3: import('@/wegts/v2/Groups3'),
  diy_group4: import('@/wegts/v2/Groups4'),
  diy_group5: import('@/wegts/v2/Groups5'),
  diy_shop_list: import('@/wegts/merchant/ShopList'),
  diy_new_coupon: import('@/wegts/v2/diy_new_coupon'),
  diy_new_coupon1: import('@/wegts/v2/diy_new_coupon1'),
  diy_new_coupon2: import('@/wegts/v2/diy_new_coupon2'),
  FITRUE_isChainstore */
}
