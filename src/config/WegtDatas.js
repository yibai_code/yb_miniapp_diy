const WegtDatas = function (type, id, type_index) {
  const emptyLink = {
    type: 'none',
    type_name: '无链接',
    id: '',
    url: '',
    title: '无链接',
  }
  const defaultImg = require('@/assets/image/default_c.png')
  let new_item = {}
  new_item['type'] = type
  new_item['id'] = id

  if (type === 'headline') {
    new_item['name'] = '标题名称'
    new_item['style_type'] = parseInt(type_index) + 1
    new_item['bg_color'] = '#000000'
    new_item['text_color'] = '#333333'
    new_item['font_size'] = '18'
    new_item['bg_color'] = '#ffffff'
  } else if (type === 'banner') {
    new_item['showdots'] = true
    new_item['dots-color'] = 'rgba(0, 0, 0, 0.3)'
    new_item['dots-active-color'] = 'rgba(0, 0, 0, 1)'
    new_item['list'] = [
      {
        image: defaultImg,
        link: emptyLink,
      },
      {
        image: defaultImg,
        link: emptyLink,
      },
    ]
  } else if (type === 'navigation') {
    new_item['columnnum'] = 4
    new_item['list'] = []
    let titles = [
      '超市便利',
      '新鲜果蔬',
      '美食烘培',
      '鲜花蛋糕',
      '汉堡披萨',
      '鸭脖卤味',
      'VIP会员',
      '品牌优惠',
    ]
    for (let i = 0; i < 8; i++) {
      let item = {
        image: require(`@/assets/image/nav_icon${i}.png`),
        title: titles[i],
        link: emptyLink,
      }
      new_item['list'].push(item)
    }
  } else if (type === 'image_group1' || type === 'image_group2') {
    new_item['showtitle'] = true
    new_item['list'] = []
    for (let i = 1; i < 5; i++) {
      let item = {
        image: defaultImg,
        title: '图片描述',
        link: emptyLink,
      }
      new_item['list'].push(item)
    }
  } else if (
    type === 'articles1' ||
    type === 'articles2' ||
    type === 'product1' ||
    type === 'product2' ||
    type === 'product3'
  ) {
    new_item['num'] = 5
    new_item['showtitle'] = true
    new_item['order'] = 'create_time'
    new_item['sort'] = 'desc'
    new_item['class_ids'] = '0'
    new_item['data_type'] = ''
    new_item['list'] = []
    let end = type === 'product1' || type === 'product2' ? 5 : 3
    let title =
      type === 'articles1' || type === 'articles2'
        ? '这里显示文章标题'
        : '产品标题'
    for (let i = 1; i < end; i++) {
      let item = {
        image: defaultImg,
        title: title,
        desc: '文章简介',
        time: '2018-11-27',
        link: emptyLink,
      }
      new_item['list'].push(item)
    }
  } else if (type === 'services1') {
    new_item['num'] = 5
    new_item['showtitle'] = true
    new_item['order'] = 'create_time'
    new_item['sort'] = 'desc'
    new_item['class_ids'] = '0'
    new_item['data_type'] = ''
    new_item['list'] = []
    let end = 3
    for (let i = 1; i < end; i++) {
      let item = {
        image: defaultImg,
        title: '预约名称',
        price: '0.00',
        price_type: 1,
        original_price: '0.00',
        order_count: 0,
        services_count: 0,
        sell_count: 0,
        link: emptyLink,
      }
      new_item['list'].push(item)
    }
  } else if (type === 'recruit') {
    new_item['num'] = 5
    new_item['showtitle'] = true
    new_item['order'] = 'create_time'
    new_item['sort'] = 'desc'
    new_item['class_ids'] = '0'
    new_item['data_type'] = ''
    new_item['list'] = []
    let end = 5
    for (let i = 1; i < end; i++) {
      let item = {
        image: defaultImg,
        title: '职位名称',
        low_salary: '8000',
        height_salary: '15000',
        edu: '本科以上学历',
        city: '上海',
        work_time: '2年以上',
        welfare: ['五险一金', '电话补助', '年底双薪'],
        link: JSON.parse(),
      }
      new_item['list'].push(item)
    }
  } else if (type === 'vote') {
    new_item['num'] = 5
    new_item['showtitle'] = true
    new_item['order'] = 'create_time'
    new_item['sort'] = 'desc'
    new_item['class_ids'] = '0'
    new_item['data_type'] = ''
    new_item['list'] = []
    let end = 2
    for (let i = 1; i < end; i++) {
      let item = {
        title: '投票名称',
        times: '09天03时44分01秒',
        status: 1,
        option: [
          {
            title: '投票选项',
            imgurl: defaultImg,
            sum: 0,
            number: i,
          },
          {
            title: '投票选项',
            imgurl: defaultImg,
            sum: 0,
            number: i + 1,
          },
        ],
        link: emptyLink,
      }
      new_item['list'].push(item)
    }
  } else if (type === 'my_video') {
    new_item['url'] = ''
    new_item['cover'] = ''
    new_item['height'] = 190
    new_item['video_type'] = '1'
  } else if (type === 'diy_form') {
    new_item['num'] = 1
    new_item['showtitle'] = true
    new_item['order'] = 'create_time'
    new_item['sort'] = 'desc'
    new_item['class_ids'] = '0'
    new_item['data_type'] = ''
    new_item['list'] = []
    new_item['btnText'] = '马上提交'

    // 默认值
    let values = [
      {
        name: '单行文本',
        option: '',
        type: 'text',
        key: 'text_0',
        show: 1,
        required: 1,
        count: 1,
        placeholder: '提醒文字',
        options: [],
      },
      {
        name: '多行文本',
        option: '',
        type: 'textarea',
        key: 'textarea_1',
        show: 1,
        required: 1,
        count: 1,
        options: [],
        placeholder: '提醒文字',
      },
      {
        name: '单选按钮',
        option: '选项一\n选项二\n选项三',
        type: 'radio',
        key: 'radio_2',
        show: 1,
        required: 1,
        count: 1,
        options: [
          { op_name: '选项一', checked: false },
          { op_name: '选项二', checked: false },
          { op_name: '选项三', checked: false },
        ],
      },
      {
        name: '多选按钮',
        option: '选项一\n选项二\n选项三',
        type: 'checkbox',
        key: 'checkbox_3',
        show: 1,
        required: 1,
        count: 1,
        options: [
          { op_name: '选项一', checked: false },
          { op_name: '选项二', checked: false },
          { op_name: '选项三', checked: false },
        ],
      },
      {
        name: '文件上传',
        option: '',
        type: 'file',
        key: 'file_4',
        show: 1,
        required: 1,
        count: 1,
        placeholder: '提醒文字',
        options: [],
      },
      {
        name: '日期时间',
        option: '',
        type: 'date',
        key: 'date_5',
        show: 1,
        required: 1,
        count: 1,
        placeholder: '提醒文字',
        options: [],
      },
      {
        name: '手机号码',
        option: '',
        type: 'phone',
        key: 'phone_6',
        show: 1,
        required: 1,
        count: 1,
        placeholder: '提醒文字',
        options: [],
      },
    ]
    let item = {
      values: values,
      title: '表单名称',
      submit_times: 2,
      link: emptyLink,
    }
    new_item['list'].push(item)
  } else if (type === 'diy_wx_advert') {
    new_item['wx_ad_id'] = ''
    new_item['wx_ad_cover'] = ''
  } else if (type === 'diy_qq_advert') {
    new_item['wx_qq_id'] = ''
    new_item['wx_qq_cover'] = ''
  } else if (type === 'diy_search') {
    new_item['search_tip'] = '请输入关键词'
    new_item['search_keyword'] = ''
    new_item['search_article'] = ''
    new_item['search_product'] = ''
  } else if (type === 'diy_map') {
    new_item['is_details'] = '2'
    new_item['phone'] = ''
    new_item['address'] = ''
    new_item['job_time'] = ''
    new_item['desc'] = ''
    new_item['postion'] = {
      lat: '39.916527',
      lng: '116.397128',
      city: '',
      address: '',
    }
  } else if (type === 'announcement') {
    new_item['icon'] = ''
    new_item['vertical'] = '1'
    new_item['icon'] = require('@/assets/image/announcement.png')
    new_item['list'] = []
    let item = {
      content: '这是一条公告内容，可设置向上或向左滚动哦！',
      link: emptyLink,
    }
    new_item['list'].push(item)
  }
  return new_item
}
export default WegtDatas
