// 全局消息键 发送全局消息统一用此键 不要直接写字符串
export const EveBusKeys = {
  // 删除页面后 需要全局刷新页面数据
  APP_NEED_REFRESH: 'APP_NEED_REFRESH',
  // 点击顶部保存按钮
  APP_SAVE: 'APP_SAVE',
  // 普通页面无数据时
  DiyPage_Empty: 'DiyPage_Empty',
  // 编辑DIY页面
  DiyPage_Edit: 'DiyPage_Edit',
  // 页头页尾类型改变
  HeadFoot_Style_Change: 'HeadFoot_Style_Change',
  // Diy页面选择的页头页尾改变
  DiyPage_HeadFoot_Change: 'DiyPage_HeadFoot_Change',
  // 系统页面选择的页头页尾改变
  SysetmPage_HeadFoot_Change: 'SysetmPage_HeadFoot_Change',
  // 页面预览
  APP_Preview: 'APP_Preview',
  //系统页面编辑
  SystemPage_Edit: 'SystemPage_Edit',
  //头尾页面编辑
  HeadFoot_Edit: 'HeadFoot_Edit',
  // 系统页面 页面设置
  SystemPageConfig_Edit: 'SystemPageConfig_Edit',
  Config_Edit: 'Config_Edit',
  // diy页面 页面设置
  PageConfig_Edit: 'PageConfig_Edit',
  // 页头页尾页面 页面设置
  HeadFootConfig_Edit: 'HeadFootConfig_Edit',
  // 组件从工具条拖动出来
  DragFromToolBar: 'DragFromToolBar',
  // 组件点击选择
  XDrage_Selected: 'XDrage_Selected',
  // 操作历史-回退
  HistoryBack: 'HistoryBack',
  // 操作历史-前进
  HistoryGo: 'HistoryGo',
  // 单页模板商店弹出
  PageMarketShow: 'PageMarketShow',
  PlatformChange: 'PlatformChange',
  PSDImport: 'PSDImport',
  // 拖动相关方法
  DragStart: 'DragStart',
  DragResize: 'DragResize',
  DragResizeEnd: 'DragResizeEnd',
  DragDrag: 'DragDrag',
  DragDragEnd: 'DragDragEnd',
  DragRotate: 'DragRotate',
  DragRotateEnd: 'DragRotateEnd',
  DragAddedToDrop: 'DragAddedToDrop',
  DragEnd: 'DragEnd',
  DragClean: 'DragClean',
  DragClick: 'DragClick',
  DragDoubleClick: 'DragDoubleClick',
  DragDeSelect: 'DragDeSelect',
  DragContextMenu: 'DragContextMenu',
}
