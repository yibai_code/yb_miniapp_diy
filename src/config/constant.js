/**
 * 全局常量 定义的各种枚举值等
 **/

export const MessageType = {
  success: 'success',
  warning: 'warning',
  info: 'info',
  error: 'error',
}

export const NotifyPosition = {
  topRight: 'top-right',
  topLeft: 'top-left',
  bottomRight: 'bottom-right',
  bottomLeft: 'bottom-left',
}
