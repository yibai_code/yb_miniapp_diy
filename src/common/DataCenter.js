class DataCenter {
  constructor() {
    this.DragInit = false
    this.CopyData = null
    this.HistoryCurrent = {}
    this.AllDragVm = {}
  }

  // 页面上所有VM
  set AllDragVm(v) {
    this._AllDragVm = v
  }
  get AllDragVm() {
    return this._AllDragVm
  }

  //是否组件工具条拖动
  set HistoryCurrent(v) {
    this._HistoryCurrent = v
  }
  get HistoryCurrent() {
    return this._HistoryCurrent
  }

  //是否组件工具条拖动
  set DragInit(v) {
    this._DragInit = v
  }
  get DragInit() {
    return this._DragInit
  }

  //设置组件当前编辑index 主要用于组件点击添加
  set WegtEditIndex(v) {
    this._WegtEditIndex = v
  }
  get WegtEditIndex() {
    return this._WegtEditIndex
  }

  //拖动时 组件占位符
  set WegtHover(v) {
    this._WegtHover = v
  }
  get WegtHover() {
    return this._WegtHover
  }

  //拖动时 组件占位符对应的vm
  set WegtHoverVM(v) {
    this._WegtHoverVM = v
  }
  get WegtHoverVM() {
    return this._WegtHoverVM
  }

  //拖动时 组件占位符对应的vm
  set CopyData(v) {
    this._CopyData = v
  }
  get CopyData() {
    return this._CopyData
  }

  //拖动时 组件占位符对应的vm
  set PastePostion(v) {
    this._PastePostion = v
  }
  get PastePostion() {
    return this._PastePostion
  }
}
export default DataCenter
