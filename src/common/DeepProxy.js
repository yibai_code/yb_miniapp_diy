import { isEqual } from 'lodash'
export const isProxy = Symbol('isProxy')
export default function DeepProxy(obj, cb) {
  if (typeof obj === 'object') {
    for (let key in obj) {
      if (typeof obj[key] === 'object') {
        if (!obj[key][isProxy]) {
          obj[key] = DeepProxy(obj[key], cb)
          obj[key][isProxy] = true
        }
      }
    }
  }
  if (obj[isProxy]) {
    return obj
  }
  obj[isProxy] = true
  return new Proxy(obj, {
    set: function (target, key, value, receiver) {
      if (typeof value === 'object') {
        value = DeepProxy(value, cb)
      }
      if (key !== isProxy && !isEqual(target[key], value)) {
        cb && cb('set', ...arguments)
      } else {
      }
      return Reflect.set(target, key, value, receiver)
    },
    deleteProperty(target, key) {
      cb && cb('delete', ...arguments)
      return Reflect.deleteProperty(...arguments)
    },
  })
}
