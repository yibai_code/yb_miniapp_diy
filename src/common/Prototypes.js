import Vue from 'vue'
String.prototype.colorHex = function () {
  if (this.indexOf('rgba') < 0) return this
  let values = this.replace(/rgba?\(/, '')
    .replace(/\)/, '')
    .replace(/[\s+]/g, '')
    .split(',')
  let a = parseFloat(values[3] || 1),
    r = Math.floor(a * parseInt(values[0]) + (1 - a) * 255),
    g = Math.floor(a * parseInt(values[1]) + (1 - a) * 255),
    b = Math.floor(a * parseInt(values[2]) + (1 - a) * 255)
  return (
    '#' +
    ('0' + r.toString(16)).slice(-2) +
    ('0' + g.toString(16)).slice(-2) +
    ('0' + b.toString(16)).slice(-2)
  )
}
String.prototype.colorRgba = function () {
  // 十六进制颜色值的正则表达式
  let reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/
  /* 16进制颜色转为RGB格式 */
  let sColor = this.toLowerCase()
  if (sColor && reg.test(sColor)) {
    //处理例如#123 这种简写的
    if (sColor.length === 4) {
      let sColorNew = '#'
      for (let i = 1; i < 4; i += 1) {
        sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1))
      }
      sColor = sColorNew
    }
    //  处理六位的颜色值
    let sColorChange = []
    for (let i = 1; i < 7; i += 2) {
      sColorChange.push(parseInt('0x' + sColor.slice(i, i + 2)))
    }
    // 处理透明度
    if (sColor.length > 7) {
      let a = parseInt('0x' + sColor.slice(7, 9))
      a = parseFloat(a / 255.0).toFixed(2)
      sColorChange.push(a)
    } else {
      sColorChange.push(1)
    }
    return 'rgba(' + sColorChange.join(',') + ')'
  } else {
    return sColor
  }
}
String.prototype.colorAlpha = function () {
  let sColor = this.toLowerCase()
  if (sColor.indexOf('#') >= 0) {
    if (sColor.length > 7) {
      let a = parseInt('0x' + sColor.slice(7, 9))
      a = parseFloat(a / 255.0).toFixed(2)
      return a
    }
    return 1
  } else if (sColor.indexOf('rgba') >= 0) {
    let a = sColor.replace('rgba(', '').replace(')', '').split(',')[3]
    if (a) {
      a = parseFloat(a)
      return a
    }
  }

  return 1
}
String.prototype.copy = function () {
  function CreateElementForExecCommand(textToClipboard) {
    let forExecElement = document.createElement('div')
    forExecElement.style.position = 'absolute'
    forExecElement.style.left = '-10000px'
    forExecElement.style.top = '-10000px'
    forExecElement.textContent = textToClipboard
    document.body.appendChild(forExecElement)
    forExecElement.contentEditable = true
    return forExecElement
  }
  function SelectContent(element) {
    let rangeToSelect = document.createRange()
    rangeToSelect.selectNodeContents(element)
    let selection = window.getSelection()
    selection.removeAllRanges()
    selection.addRange(rangeToSelect)
  }
  let textToClipboard = this
  let success = true
  if (window.clipboardData) {
    // Internet Explorer
    window.clipboardData.setData('Text', textToClipboard)
  } else {
    let forExecElement = CreateElementForExecCommand(textToClipboard)
    SelectContent(forExecElement)
    try {
      if (window.netscape && netscape.security) {
        netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect')
      }
      success = document.execCommand('copy', false, null)
    } catch (e) {
      success = false
    }
    document.body.removeChild(forExecElement)
  }
  return success
}
String.prototype.is = function () {
  for (let arg of arguments) {
    if (this === arg) {
      return true
    }
  }
  return false
}
