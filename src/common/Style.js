import { isValid } from '@/utils/validate'
import { addClass, domGet, domGets } from '@/components/VueDragDropAlign/dom'
class Style {
  constructor(data, target) {
    this.css = {}
    this.common = {}
  }

  findById(id) {
    return this.css[id]
      ? this.css[id]
      : this.common[id]
      ? this.common[id]
      : null
  }
  findAllById(id) {
    let obj = {}
    for (let status of ['css', 'common']) {
      for (let eid in this[status]) {
        if (eid.indexOf(id) >= 0) {
          obj[eid] = this[status][eid]
        }
      }
    }
    return obj
  }
  init() {
    this.css = {}
    this.common = {}
  }
  jsonStr() {
    let d = {
      css: this.css,
      common: this.common,
    }
    return JSON.stringify(d)
  }
  remove(id, st) {
    let arr = st ? [st] : ['css', 'common']
    for (let status of arr) {
      for (let k in this[status]) {
        if (k.indexOf(id) >= 0) {
          delete this[status][k]
        }
      }
    }
    let kk = id.replace(RegExp('[^0-9a-zA-Z]', 'g'), '')
    domGets(`style[id*="${kk}"]`).forEach((item) => {
      item.remove()
    })
  }

  /**
   * 复制样式
   * @param from id
   * @param to id
   */
  copy(from, to) {
    let arr = ['css', 'common']
    for (let status of arr) {
      for (let k in this[status]) {
        if (k.indexOf(from) >= 0) {
          let nid = k.replace(new RegExp(from, 'g'), to)
          this[status][nid] = JSON.parse(JSON.stringify(this[status][k]))
        }
      }
    }
  }
  removeAll() {
    for (let status of ['css', 'common']) {
      for (let eid in this[status]) {
        delete this[status][eid]
      }
    }
    this.styleClean()
  }

  styleClean() {
    domGets(`style[id]`).forEach((item) => {
      item.remove()
    })
  }

  //把所有样式写到页面里
  makeAll() {
    this.styleClean()
    for (let eid in this.css) {
      this.make(eid, 'css')
    }
    for (let eid in this.common) {
      this.make(eid, 'common')
    }
  }

  /**
   * 构建某个类型的样式
   * @param type: css | common
   */
  makeType(type) {
    for (let eid in this[type]) {
      this.make(eid, type)
    }
  }

  remake() {
    for (let status of ['css', 'common']) {
      for (let eid in this[status]) {
        this.make(eid, status)
      }
    }
  }

  //把某个组件的样式写入页面 id:组件id status: styleVM.data里面的key
  make(id, status) {
    status = status || 'css'
    let jqid = status + '_' + id
    jqid = jqid.replace(RegExp('[^0-9a-zA-Z]', 'g'), '')
    let dom = domGet(`#${jqid}`)
    if (dom) {
      dom.remove()
    }
    if (!this[status][id]) {
      return
    }
    let css_str = this.getCssStr(id, status)
    let style = document.createElement('style')
    style.setAttribute('id', jqid)
    style.innerHTML = css_str
    domGet('head').appendChild(style)
    //轮播图等特殊处理
    if (
      id.indexOf('swiper-pagination-bullet') >= 0 &&
      id.indexOf('active') < 0
    ) {
      let nid = id + '-active'
      this.make(nid, 'css')
    }
  }

  makeAllId(id) {
    if (!id) {
      return
    }
    for (let eid in this.css) {
      if (eid.indexOf(id) >= 0) {
        this.make(eid, 'css')
      }
    }
    for (let eid in this.common) {
      if (eid.indexOf(id) >= 0) {
        this.make(eid, 'common')
      }
    }
  }

  CssStrAll() {
    let str = ''
    for (let eid in this.css) {
      str += this.getCssStr(eid, 'css')
    }
    for (let eid in this.common) {
      str += this.getCssStr(eid, 'common')
    }
    return str
  }

  getCssStr(id, status) {
    let item = this[status][id]
    let css_str = ''
    let css = {}

    for (let k in item) {
      if (k === 'bg') {
        let bg = item['bg']
        if (bg.type === 'color') {
          css['background-color'] = bg.color.color
          css['background'] = bg.color.color
        } else if (bg.type === 'image') {
          css['background-image'] = 'url(' + bg.image.path + ')'
          css['background-position'] = bg.image.px + '% ' + bg.image.py + '%'
          css['background-repeat'] = bg.image.repeat.repeat
          css['background-size'] = bg.image.repeat.size
        } else if (bg.type === 'gradient') {
          let deg = bg.gradient.deg === undefined ? 180 : bg.gradient.deg
          css['background-color'] = 'rgba(255, 255, 255, 0)'
          css['background-image'] =
            'linear-gradient(' +
            deg +
            'deg, ' +
            bg.gradient.begin +
            ', ' +
            bg.gradient.end +
            ')'
        } else if (bg.type === 'none') {
          css['background-color'] = 'rgba(255, 255, 255, 0)'
          css['background'] = 'rgba(255, 255, 255, 0)'
          css['background-image'] = 'url()'
        }
      } else if (k === 'image') {
        let image = item['image']
        css['background-image'] = 'url(' + image.path + ')'
        css['background-position'] = image.px + '% ' + image.py + '%'
        css['background-repeat'] = image.repeat.repeat
        css['background-size'] = image.repeat.size
      } else if (k === 'bd') {
        //边框
        let bd = item['bd']
        if (bd.t) {
          css['border-top'] = bd.t.width + 'px ' + bd.t.style + ' ' + bd.t.color
        }
        if (bd.r) {
          css['border-right'] =
            bd.r.width + 'px ' + bd.r.style + ' ' + bd.r.color
        }
        if (bd.b) {
          css['border-bottom'] =
            bd.b.width + 'px ' + bd.b.style + ' ' + bd.b.color
        }
        if (bd.l) {
          css['border-left'] =
            bd.l.width + 'px ' + bd.l.style + ' ' + bd.l.color
        }
      } else if (k === 'br') {
        //圆角
        let br = item['br']
        css['border-radius'] =
          br.tl + 'px ' + br.tr + 'px ' + br.br + 'px ' + br.bl + 'px '
      } else if (k === 'sd') {
        //阴影
        let sd = item['sd']
        let radian = ((2 * Math.PI) / 360) * sd.sd_angle
        let x = Math.sin(radian) * sd.sd_width * -1
        let y = Math.cos(radian) * sd.sd_width
        css['box-shadow'] =
          x + 'px ' + y + 'px ' + sd.sd_blur + 'px ' + sd.color + ' !important'
      } else if (k === 'font') {
        //字体
        let font = item['font']
        if (font.family) css['font-family'] = font.family
        if (font.size) css['font-size'] = font.size + 'px'
        if (font.color) css['color'] = font.color
        if (font.isB) css['font-weight'] = font.isB ? 600 : 400
        if (font.isI) css['font-style'] = font.isI ? 'italic' : 'normal'
        if (font.isU) css['text-decoration'] = font.isU ? 'underline' : 'unset'
        if (font.align) {
          css['text-align'] = font.align
          let flex = {
            left: 'flex-start',
            center: 'center',
            right: 'flex-end',
          }
          css['justify-content'] = flex[font.align]
        }
        if (font['line-height']) css['line-height'] = font['line-height'] + 'px'
      } else if (k === 'Transform') {
        //字体
        let trans = item['Transform']
        if (!css['transform']) css['transform'] = ''
        if (isValid(trans.translateX))
          css['transform'] += 'translateX(' + trans.translateX + 'px) '
        if (isValid(trans.translateY))
          css['transform'] += 'translateY(' + trans.translateY + 'px) '
        if (isValid(trans.rotate))
          css['transform'] += 'rotate(' + trans.rotate + 'deg) '
        if (isValid(trans.scale))
          css['transform'] += 'scale(' + trans.scale + ') '
        css['-webkit-transform'] = css['transform']
      } else if (
        k === 'line-height' ||
        k === 'width' ||
        k === 'height' ||
        k === 'font-size' ||
        k === 'top' ||
        k === 'left' ||
        k === 'right' ||
        k === 'bottom' ||
        k === 'min-height' ||
        k.indexOf('margin') >= 0 ||
        k.indexOf('padding') >= 0
      ) {
        let value = item[k].value + ''
        value =
          value.indexOf('%') >= 0 || value.indexOf('!important') >= 0
            ? value
            : value + 'px'
        css[k] = value
      } else if (k === 'fixed') {
        let fixed = item['fixed']
        if (fixed.type === 'tl') {
          css['left'] = fixed.left + 'px'
          css['top'] = fixed.top + 'px'
          css['bottom'] = 'auto'
          css['right'] = 'auto'
          css['margin'] = 'auto'
        } else if (fixed.type === 'tc') {
          css['left'] = fixed.left + 'px'
          css['top'] = fixed.top + 'px'
          css['bottom'] = 'auto'
          css['right'] = 0
          css['margin'] = 'auto'
        } else if (fixed.type === 'tr') {
          css['left'] = 'auto'
          css['top'] = fixed.top + 'px'
          css['bottom'] = 'auto'
          css['right'] = fixed.left + 'px'
          css['margin'] = 'auto'
        } else if (fixed.type === 'cl') {
          css['left'] = fixed.left + 'px'
          css['top'] = fixed.top + 'px'
          css['bottom'] = 0
          css['right'] = 'auto'
          css['margin'] = 'auto'
        } else if (fixed.type === 'cr') {
          css['left'] = 'auto'
          css['top'] = fixed.top + 'px'
          css['bottom'] = 0
          css['right'] = fixed.left + 'px'
          css['margin'] = 'auto'
        } else if (fixed.type === 'bl') {
          css['left'] = fixed.left + 'px'
          css['top'] = 'auto'
          css['bottom'] = fixed.top + 'px'
          css['right'] = 'auto'
          css['margin'] = 'auto'
        } else if (fixed.type === 'bc') {
          css['left'] = fixed.left + 'px'
          css['top'] = 'auto'
          css['bottom'] = fixed.top + 'px'
          css['right'] = 0
          css['margin'] = 'auto'
        } else if (fixed.type === 'br') {
          css['left'] = 'auto'
          css['top'] = 'auto'
          css['bottom'] = fixed.top + 'px'
          css['right'] = fixed.left + 'px'
          css['margin'] = 'auto'
        }
      } else if (k === 'animation') {
        let animo = item['animation']
        css['animation-delay'] = animo.delay + 's'
        css['animation-duration'] = animo.duration + 's'
        css['animation-iteration-count'] = animo['iteration-count']
        let node = domGet(`#${id}`)
        if (node) {
          addClass(node, ['xanimate', animo['animo_class']])
          if (node.getAttribute('data-animo') !== animo['animo_class']) {
            node.setAttribute('data-animo', animo['animo_class'])
          }
        }
      } else {
        let value = item[k].value
        css[k] = value
      }
    }
    for (let k in css) {
      css_str += k + ':' + css[k] + ';'
    }
    css_str = '#' + id + '{' + css_str + '}'
    return css_str
  }
}
export default Style
