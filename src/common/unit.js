export function makeId() {
  return (
    'n' +
    (Date.parse(new Date()) / 1000 + '' + Math.round(Math.random() * 100000))
  )
}
