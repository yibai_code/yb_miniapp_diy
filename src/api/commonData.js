/**
 * 组件通用数据获取
 * @author 徐鹏飞
 */
import request from '@yb_fromwork/admin-base/src/utils/request'
import buildConfig from '@/config/BuildConfig'

/**
 * 分类列表
 * @param { type }
 */
export function dataClassesList(data = { type }) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/data_classes_list`,
    method: 'post',
    data: data,
  })
}

/**
 * 内容列表
 * @param { type }
 */
export function dataItemsList(data = { type, num, class_ids, sort, order }) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/data_items_list`,
    method: 'post',
    data: data,
  })
}

/**
 * 内容列表
 * @param { type }
 */
export function dataIdsList(data = { type, ids }) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/data_ids_list`,
    method: 'post',
    data: data,
  })
}

/**
 * 内容详情
 * @param { type }
 */
export function dataItemInfo(data = { type, id }) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/data_item_info`,
    method: 'post',
    data: data,
  })
}

/**
 * 腾讯视频转换
 * @param { videoUrl }
 */
export function videoToMp4(data = { videoUrl }) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/videoToMp4`,
    method: 'post',
    data: data,
  })
}
