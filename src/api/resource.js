import request from '@yb_fromwork/admin-base/src/utils/request'
import buildConfig from '@/config/BuildConfig'

/**
 * 资源管理列表
 * @param data
 * @returns {AxiosPromise}
 */
export function getList(data) {
  return request({
    url: 'resource/Resource/tableList',
    method: 'post',
    data: data,
    permission: 441,
  })
}

/**
 * 资源管理列表搜索项
 * @returns {AxiosPromise}
 */
export function searchBox() {
  return request({
    url: 'resource/Resource/searchBox',
    method: 'post',
  })
}

/**
 * 删除
 * ids: 待删除id串,以逗号间隔
 * @returns {AxiosPromise}
 */
export function fileDel(ids) {
  return request({
    url: 'resource/Resource/del',
    method: 'post',
    permission: 442,
    data: { ids: ids },
  })
}

/**
 * 资源文件上传
 * file_upload: 资源文件上传
 * @returns {AxiosPromise}
 */
export function fileUpload(file, onProgress) {
  let param = new FormData() // 创建form对象
  param.append('file_upload', file)
  let config = {
    timeout: 10 * 60 * 1000,
    headers: { 'Content-Type': 'multipart/form-data' },
    onUploadProgress: (evt) => {
      onProgress && onProgress(evt)
    },
  }
  return request.post('resource/Resource/upload', param, config)
}
