import request from '@yb_fromwork/admin-base/src/utils/request'
import buildConfig from '@/config/BuildConfig'

/**
 * 获取全部相册
 * @param data
 * @returns {AxiosPromise}
 */
export function allAlbum(type) {
  return request({
    url: 'album/Album/allAlbum&select_type=' + type,
    method: 'get',
  })
}

/**
 * 获取某个相册下的全部图片
 * @param data {'album_id': 0}
 * @returns {AxiosPromise}
 */
export function albumPictures(data = { album_id: 0, select_type: 1 }) {
  return request({
    url: 'album/Album/albumPictures',
    method: 'post',
    data: data,
  })
}

/**
 * 添加网络图片
 * @param data { album_id: 0, urls: '' }
 * @returns {AxiosPromise}
 */
export function addNetImage(data = { album_id: 0, urls: '' }) {
  return request({
    url: 'album/Album/addNetImage',
    method: 'post',
    data: data,
  })
}

/**
 * 图片上传
 * @param file
 * @param album_id
 * @returns {Promise<AxiosResponse<T>>}
 */
export function uplaodImage({ file, album_id, upload_type }, onProgress) {
  let param = new FormData() // 创建form对象
  param.append('file_upload', file)
  param.append('album_id', album_id)
  param.append('upload_type', upload_type)
  let config = {
    timeout: 30000,
    headers: { 'Content-Type': 'multipart/form-data' },
    onUploadProgress: (evt) => {
      onProgress && onProgress(evt)
    },
  }
  return request.post('album/Album/imageUpload', param, config)
}

/**
 * 删除相册
 * @param data
 * @returns {AxiosPromise}
 */
export function delAlbum(data) {
  return request({
    url: '/album/Album/images_del',
    method: 'post',
    data,
  })
}

/**
 * 相册添加
 * data: 文章数据
 * @returns {AxiosPromise}
 */
export function add(data) {
  return request({
    url: '/album/Album/add',
    method: 'post',
    data: data,
  })
}
