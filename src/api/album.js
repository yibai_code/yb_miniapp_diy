import request from '@yb_fromwork/admin-base/src/utils/request'
/**
 * 新建相册
 * @param data {name: '', sort: null}
 * @returns {AxiosPromise}
 */
export function add(data = { name: '', sort: null }) {
  return request({
    url: 'album/Album/add',
    method: 'post',
    data: data,
  })
}
