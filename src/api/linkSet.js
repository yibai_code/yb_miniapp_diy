/**
 * 链接选择项
 * @author 徐鹏飞
 */
import request from '@yb_fromwork/admin-base/src/utils/request'
import buildConfig from '@/config/BuildConfig'

/**
 * 分类列表
 * @param { type }
 */
export function allClass(data = { type }) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/link_list_web`,
    method: 'post',
    data: data,
  })
}

/**
 * 分类列表
 * @param { type }
 */
export function listClass(data = { type }) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/link_item_class`,
    method: 'post',
    data: data,
  })
}

/**
 * 详情列表
 * @param { type, page, class_id, sort, sort_flag }
 */
export function lists(data = { type, page, class_id, sort, sort_flag }) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/link_item_list`,
    method: 'post',
    data: data,
  })
}

/**
 * 选择链接列表
 * @param data
 */
export function linkList(data) {
  return request({
    url: `/miniapp/miniapp/getDiyLink`,
    method: 'post',
    data: data,
  })
}
