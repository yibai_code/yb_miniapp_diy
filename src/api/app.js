/**
 * 全局接口
 * @author 徐鹏飞
 */
import request from '@yb_fromwork/admin-base/src/utils/request'
import buildConfig from '@/config/BuildConfig'

/**
 * 所有页面
 * @param { type }
 */
export function allPage() {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/allpage`,
    method: 'post',
  })
}

/**
 * diy页面删除
 * @param id 页面id
 * @returns {AxiosPromise}
 */
export function pageDel(id) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/delpage`,
    method: 'post',
    data: { id: id },
  })
}

/**
 * diy页面新增
 * @param id 页面id
 * @returns {AxiosPromise}
 */
export function pageAdd(data) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/addPageNew`,
    method: 'post',
    data,
  })
}

/**
 * diy页面标题修改
 * @param id 页面id
 * @param title 页面标题
 * @returns {AxiosPromise}
 */
export function pageTitleChange(data = { id, title }) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/titlechange`,
    method: 'post',
    data: data,
  })
}

/**
 * diy页面状态修改
 * @param id 页面id
 * @param title 页面标题
 * @returns {AxiosPromise}
 */
export function pageStatusChange(data = { id, status }) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/statuschange`,
    method: 'post',
    data: data,
  })
}

/**
 * diy页面复制
 * @param id 页面id
 * @returns {AxiosPromise}
 */
export function pageCopy(id) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/copypage`,
    method: 'post',
    data: { id: id },
  })
}

/**
 * 保存数据
 * @param str 页面id
 * @returns {AxiosPromise}
 */
export function doSave(str) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/pagesave`,
    method: 'post',
    data: { data: str },
  })
}

/**
 * h5安装
 * @returns {AxiosPromise}
 */
export function h5Install() {
  return request({
    url: `/channel_h5/Wap/install`,
    method: 'post',
  })
}

/**
 * 检测H5是否需要安装
 * @returns {AxiosPromise}
 */
export function checkH5Package() {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/checkH5Package`,
    method: 'get',
  })
}

/**
 * 获取站点信息, 用户信息
 * @returns {AxiosPromise}
 */
export function appInfo() {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/appInfo`,
    method: 'get',
  })
}
