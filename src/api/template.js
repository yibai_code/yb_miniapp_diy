import request from '@yb_fromwork/admin-base/src/utils/request'
import buildConfig from '@/config/BuildConfig'

/**
 * 模板市场列表
 * @param data {}
 * @returns {AxiosPromise}
 */
export function TmplMarket(data) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/alltmpl`,
    method: 'post',
    data: data,
  })
}

/**
 * 模板市场更换模板
 * @param data {}
 * @returns {AxiosPromise}
 */
export function ReplaceTmpl(data) {
  return request({
    url: `${buildConfig.apiSymbolName}/miniapp/${buildConfig.apiSymbol}/tmpluse`,
    method: 'post',
    data: data,
    timeout: 10 * 60 * 1000,
  })
}
