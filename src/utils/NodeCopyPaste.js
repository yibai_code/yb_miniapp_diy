import Vue from 'vue'
import { DataCenter, StyleCenter } from '@/global'
import { makeId } from '@/common/unit'
import store from '@/store'

export function wegtCopy(wegtData) {
  let oid = wegtData.id
  let nid = makeId()
  let dataClone = JSON.parse(JSON.stringify(wegtData))
  dataClone.id = nid
  let styleClone = {}
  let style = StyleCenter.css
  if (
    store.getters['diy/loginAuthShow'] ||
    store.getters['diy/phoneAuthShow']
  ) {
    style = StyleCenter.common
  }
  for (let id in style) {
    if (id.indexOf(oid) >= 0) {
      let idn = id.replace(new RegExp(oid, 'g'), nid)
      styleClone[idn] = JSON.parse(JSON.stringify(style[id]))
    }
  }
  DataCenter.CopyData = {
    data: dataClone,
    style: styleClone,
  }
}

/**
 * 组件粘贴
 * @param blockWegtData 容器组件数据
 */
export function wegtPaste(blockWegtData) {
  if (!DataCenter.CopyData) {
    return
  }
  let style = StyleCenter.css
  let styleFlag = 'css'
  if (
    store.getters['diy/loginAuthShow'] ||
    store.getters['diy/phoneAuthShow']
  ) {
    if (!DataCenter.CopyData.data.type.is('text', 'image')) {
      return
    }
    style = StyleCenter.common
    styleFlag = 'common'
  }

  let wegtData = JSON.parse(JSON.stringify(DataCenter.CopyData.data))
  let styleCopy = JSON.parse(JSON.stringify(DataCenter.CopyData.style))
  let oid = wegtData.id
  let nid = makeId()
  wegtData.id = nid
  Vue.set(blockWegtData.sub, nid, wegtData)

  for (let id in styleCopy) {
    let idn = id.replace(new RegExp(oid, 'g'), nid)
    style[idn] = styleCopy[id]
  }
  style[nid].left = { value: DataCenter.PastePostion.left }
  style[nid].top = { value: DataCenter.PastePostion.top }
  StyleCenter.make(nid, styleFlag)
}

/**
 * 组件克隆 id替换成新的
 * @param wegtData
 * @returns {{data: any, style: {}}} 数据 和 样式
 */
export function wegtClone(wegtData) {
  let oid = wegtData.id
  let dataClone = JSON.parse(JSON.stringify(wegtData))
  dataClone.id = makeId()
  let styles = {}
  if (dataClone.sub) {
    let sub_new = {}
    for (let sid in dataClone.sub) {
      let nid = makeId()
      let sdata = JSON.parse(JSON.stringify(dataClone.sub[sid]))
      sdata.id = nid
      sub_new[nid] = sdata
      let style = StyleCenter.findAllById(sid)
      for (let eid in style) {
        let idn = eid.replace(sid, nid)
        styles[idn] = JSON.parse(JSON.stringify(style[eid]))
      }
    }
    dataClone.sub = sub_new
  }
  let style = StyleCenter.findAllById(oid)
  for (let eid in style) {
    let idn = eid.replace(oid, dataClone.id)
    styles[idn] = JSON.parse(JSON.stringify(style[eid]))
  }
  return { data: dataClone, style: styles }
}
