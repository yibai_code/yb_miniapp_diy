import Vue from 'vue'
import store from '@/store'
import { StyleCenter } from '@/global'
import {
  addClass,
  css,
  domGet,
  find,
  parents,
  removeClass,
  removeEvent,
} from '@/components/VueDragDropAlign/dom'
import { settingPanleHide } from '@/utils/Page'

/**
 * 组件删除
 * @param wegtData 组件数据
 */
export function wegtDel(wegtData) {
  if (
    !wegtData ||
    !wegtData.id ||
    wegtData.type.is('auth_block', 'button_submit', 'button_cancel')
  ) {
    return
  }
  let id = wegtData.id
  if (wegtData.type.is('text', 'button', 'image')) {
    let blockWegtData = {}
    if (store.getters['diy/loginAuthShow']) {
      blockWegtData = store.getters['diy/common'].login_auth
    } else if (store.getters['diy/phoneAuthShow']) {
      blockWegtData = store.getters['diy/common'].phone_auth
    } else {
      let li = parents(`#${id}`, 'li.listblock')[0]
      let index = parseInt(li.getAttribute('index'))
      blockWegtData = store.getters['diy/allData'][index]
    }
    Vue.delete(blockWegtData.sub, id)
    StyleCenter.remove(id)
  } else {
    if (wegtData.sub) {
      for (let sid in wegtData.sub) {
        StyleCenter.remove(sid)
      }
    }
    let li = parents(`#${id}`, 'li.listblock')[0]
    if (li) {
      let index = parseInt(li.getAttribute('index'))
      store.dispatch('diy/wegtDel', index)
      css('#ctrbox', {
        display: 'none',
      })
    }
  }
  settingPanleHide()
}

/**
 * swiper 更新 1. 数据改变 2. 宽高高边
 * @param id
 */
export function updateSwiper(id) {
  let dom = domGet(`#${id}`)
  if (dom) {
    let swiper = dom.swiper
    swiper && swiper.update()
  }
}

/**
 * 清除DragDropVM 的事件 主要用于选项卡组件 防止选项卡内部组件点击等事件
 * @param vm
 */
export function cleanDragDropVm(vm) {
  vm.alignInit = function () {}
  removeEvent(document.documentElement, 'mousemove', vm.move)
  removeEvent(document.documentElement, 'mousedown', vm.deselect)
  removeEvent(document.documentElement, 'touchend touchcancel', vm.deselect)
  removeEvent(document.documentElement, 'mouseup', vm.handleUp)
  removeEvent(vm.$el, 'contextmenu', vm.contextMenu)
  removeEvent(document.documentElement, 'touchstart', vm.handleUp)
  removeEvent(document.documentElement, 'mousemove', vm.move)
  removeEvent(document.documentElement, 'touchmove', vm.move)
  removeEvent(document.documentElement, 'mouseup', vm.handleUp)
  removeEvent(window, 'resize', vm.checkParentSize)
  removeEvent(vm.$el, 'dblclick', vm.elementDoubleClick)
  removeEvent(vm.$el, 'mousedown', vm.elementMouseDown)
  removeEvent(vm.$el, 'touchstart', vm.elementTouchDown)
  removeClass(vm.$el, ['dropable', vm.classNameContextMenu])
  find(vm.$el, '.dropable').forEach((dom) => {
    removeClass(dom, 'dropable')
  })
  vm.$destroy()
}
