import {
  parents,
  children,
  remove,
  domGet,
  addClass,
  css,
  offset,
} from '@/components/VueDragDropAlign/dom'
import { DataCenter, EventBus, StyleCenter } from '@/global'
import { EveBusKeys } from '@/config/EveBusKeys'
import { authAddWegt, cleanInvalidVm, wegtAdd } from '@/utils/DragDrop'
import store from '@/store'
import { settingPanleHide } from '@/utils/Page'
import { contextMenuHide, contextMenuShow } from '@/utils/ContextMenu'

function _UpdateStyleCenterOnMove(vm, make = false) {
  let id = vm.$parent.wegtData.id
  let positionSet = vm.$parent.positionSet || []
  console.log('positionSet: ', positionSet)
  let style = StyleCenter.findById(id)
  console.log('style: ', style)
  if (positionSet.length > 0) {
    for (let key of positionSet) {
      if (!vm.hasOwnProperty(key)) {
        continue
      }
      if (key === 'rotate') {
        if (!style['Transform']) {
          style['Transform'] = {}
        }
        style['Transform'].rotate = vm.rotate
      } else {
        style[key] = { value: vm[key] }
      }
    }
    if (make) {
      StyleCenter.makeAllId(id)
    }
  }
}

/**
 * 组件单击
 * @param vm
 * @constructor
 */
function DragClick(vm) {
  console.log('DragClick !!!!: ', vm)
  if (
    vm.isToolWegt &&
    !store.getters['diy/loginAuthShow'] &&
    !store.getters['diy/phoneAuthShow']
  ) {
    let wegtData = vm.wegtData
    let type = wegtData.type
    let isSub = ['text', 'image', 'button'].includes(type)
    console.log('isSub: ', isSub)
    let index = DataCenter.WegtEditIndex
    let id = wegtData.id
    wegtAdd(vm, index, !isSub).then(() => {
      let dom = domGet(`#${id}`)
      if (dom) {
        dom.removeAttribute('style')
      }
      setTimeout(() => {
        let data = store.getters['diy/allData'][index]
        console.log(store.getters['diy/allData'], 'datadatadatadatadata')
        console.log(index, 'indexindexindexindexindexindex')
        store.dispatch('diy/setEditData', data)
        store.dispatch('diy/setEditIndex', index)
      }, 100)
    })
  }
}

/**
 * 组件右键点击
 * @param e
 * @param vm
 * @param vms
 * @constructor
 */
function DragContextMenu(e, vm, vms) {
  console.log('DragContextMenu !!!!: ', vm)
  contextMenuShow(vm, e)
}

/**
 * 组件取消选择
 * @param vm
 * @constructor
 */
function DragDeSelect(vm) {
  console.log('DragDeSelect !!!!: ', vm)
  settingPanleHide()
  if (vm.isToolWegt) {
    console.log('vm.isToolWegt: ', vm.isToolWegt)
    cleanInvalidVm(vm)
    return
  }
}

/**
 * 组件双击
 * @param vm
 * @constructor
 */
function DragDoubleClick(vm) {
  let extendData = vm.extendData
  if (extendData.id === 'member') {
    extendData.proxyEdit()
  }
}

/**
 * 组件点击
 * @param vm
 * @constructor
 */
function DragStart(vm, e) {
  console.log('DragStart !!!!: ', vm)
  // 组件是工具条的待放置组件
  if (vm.isToolWegt) {
    return
  }
  // 组件数据
  let wegtData = vm.$parent.wegtData
  // 组件在数据列表的index
  let index = vm.$parent.index
  if (store.getters['diy/editData'].id !== wegtData.id) {
    // 设置vuex当前编辑组件数据和顺序index
    store.dispatch('diy/setEditIndex', index)
    store.dispatch('diy/setEditData', wegtData)
  }
  if (store.getters['diy/settingPanle'].id !== wegtData.id) {
    let title = vm.$parent.title
    let settingItem = vm.$parent.settingItem
    let styleItem = vm.$parent.styleItem
    let styleSet = vm.$parent.styleSet
    let dataSet = vm.$parent.dataSet
    let typeSet = vm.$parent.typeSet
    let positionSet = vm.$parent.positionSet || []

    store.dispatch('diy/setSettingPanle', {
      id: wegtData.id, // 组件id
      title: title, // 显示的编辑标题
      settingItem: settingItem, // 右侧设置栏显示项
      styleItem: styleItem, // 基础样式设置选项
      styleSet: styleSet, // 组件高级样式设置选项
      dataSet: dataSet, // 组件数据设置链接的数据设置页
      typeSet: typeSet, // 组件切换风格链接的风格切换页
      positionSet: positionSet, // 组件移动缩放旋转时允许更改的位置设置 左 右 宽 高 角度
    })
  }
  // 如果是容器组件 每次点击记录下点击位置坐标 用于粘贴组件时确定粘贴位置
  if (wegtData.type === 'block') {
    DataCenter.PastePostion = {
      left: e.offsetX,
      top: e.offsetY,
    }
  }
}

/**
 * 组件结束拖动动作 鼠标抬起
 * @param vm
 * @constructor
 */
function DragEnd(vm, moved) {
  console.log('DragEnd !!!!: ', vm, moved)
  // 有占位符号 添加组件数据到VUEX
  if (DataCenter.WegtHoverVM) {
    new DragAddedToDrop(vm, null)
    DataCenter.DragInit = false
    return
  }
  // 清理 没有占位符 组件还是从工具条拖过来的待放置组件
  if (vm.isToolWegt) {
    cleanInvalidVm(vm)
    return
  }
  DataCenter.WegtHover = null
  DataCenter.WegtHoverVM = null
  // 不是从工具条拖过来的待放置组件
  if (!DataCenter.DragInit) {
    // 如果组件移动了 更新样式数据 添加历史数据
    if (moved) {
      _UpdateStyleCenterOnMove(vm, true)
      store.commit('diy/SET_APP_CHANGED', true)
      store.commit('diy/HistoryAdd')
    }
    // 移除组件的style属性
    vm.$el.removeAttribute('style')
  }
  DataCenter.DragInit = false
}

/**
 * 组件拖动中
 * @param vm
 * @param left
 * @param top
 * @constructor
 */
function DragDrag(vm, left, top) {
  if (!vm.isToolWegt) {
    return
  }
  if (
    store.getters['diy/loginAuthShow'] ||
    store.getters['diy/phoneAuthShow']
  ) {
    remove('.copyhover')
    DataCenter.WegtHover = null
    DataCenter.WegtHoverVM = null
    vm.dataIndex = null
    return
  }
  let id = vm.wegtData.id
  let that = domGet(`#${id}`)
  let mainblock_l = DataCenter.MainBlockPostion.l
  let mainblock_r = DataCenter.MainBlockPostion.r
  let mainblock_t = DataCenter.MainBlockPostion.t
  let mainblock_b = DataCenter.MainBlockPostion.b
  if (
    vm.top + vm.height < mainblock_t ||
    vm.left < mainblock_l ||
    vm.left > mainblock_r ||
    vm.left + vm.width < mainblock_l ||
    vm.top > mainblock_b ||
    vm.top + vm.height < mainblock_t
  ) {
    remove('.copyhover')
    DataCenter.WegtHover = null
    DataCenter.WegtHoverVM = null
    vm.dataIndex = null
    return
  }
  let copy = DataCenter.WegtHover
  if (!copy) {
    copy = that.cloneNode(true)
    addClass(copy, 'copyhover')
    css(copy, {
      opacity: 1,
      position: 'relative',
      left: 'unset',
      top: 'unset',
    })
    DataCenter.WegtHover = copy
  }
  let mainBlock = domGet('#mainblock')
  let arr = children(mainBlock, '.ui-draggable')
  let count = arr.length
  if (arr.length === 0) {
    DataCenter.WegtHoverVM = vm
    DataCenter.WegtHoverVM.dataIndex = 0
    mainBlock.appendChild(copy)
  } else {
    arr.some((node, i) => {
      let bounds = offset(node)
      let t = bounds.top
      let b = t + bounds.height
      let index = parseInt(node.getAttribute('index')) || i
      if (index === 0 && vm.top < t) {
        let cindex = index
        if (
          DataCenter.WegtHoverVM &&
          DataCenter.WegtHoverVM.dataIndex === cindex &&
          domGet('.copyhover')
        ) {
          return true
        }
        remove('.copyhover')
        DataCenter.WegtHoverVM = vm
        DataCenter.WegtHoverVM.dataIndex = cindex
        node.before(copy.cloneNode(true))
        return true
      } else if (vm.top >= t && (vm.top <= b || i === arr.length - 1)) {
        // 组件是容器组件 并且位置是在容器上方
        let type = vm.wegtData.type
        if (
          node.getAttribute('data-type') === 'block' &&
          type.is('text', 'image', 'button')
        ) {
          if (vm.top + vm.height <= b && vm.left + vm.width <= mainblock_r) {
            remove('.copyhover')
            DataCenter.WegtHover = null
            DataCenter.WegtHoverVM = null
            vm.dataIndex = null
            return true
          }
        }
        // 组件下方
        if (vm.top + vm.height > b || (i === arr.length - 1 && vm.top > b)) {
          let cindex = index + 1
          if (
            DataCenter.WegtHoverVM &&
            DataCenter.WegtHoverVM.dataIndex === cindex &&
            domGet('.copyhover')
          ) {
            return true
          }
          remove('.copyhover')
          DataCenter.WegtHoverVM = vm
          DataCenter.WegtHoverVM.dataIndex = cindex
          node.after(copy.cloneNode(true))
          if (cindex === count) {
            domGet('.copyhover').scrollIntoView()
          }
          return true
        }

        let cindex = index
        if (
          DataCenter.WegtHoverVM &&
          DataCenter.WegtHoverVM.dataIndex === cindex &&
          domGet('.copyhover')
        ) {
          return true
        }
        remove('.copyhover')
        DataCenter.WegtHoverVM = vm
        DataCenter.WegtHoverVM.dataIndex = cindex
        node.before(copy.cloneNode(true))
        return true
      } else {
        if (!domGet('.copyhover')) {
          DataCenter.WegtHoverVM = null
          vm.dataIndex = null
        }
      }
      return false
    })
  }
}

/**
 * 组件拖动结束 鼠标抬起
 * @param vm
 * @param left
 * @param top
 * @constructor
 */
function DragDragEnd(vm, left, top) {
  console.log('DragDragEnd !!!!: ', vm)
}

/**
 * 组件尺寸缩放
 * @param vm
 * @param left
 * @param top
 * @param width
 * @param height
 * @constructor
 */
function DragResize(vm, left, top, width, height) {
  console.log('DragResize !!!!!!')
  _UpdateStyleCenterOnMove(vm)
}

/**
 * 组件尺寸缩放结束 鼠标抬起
 * @param vm
 * @param left
 * @param top
 * @param width
 * @param height
 * @constructor
 */
function DragResizeEnd(vm, left, top, width, height) {}

/**
 * 组件旋转
 * @param vm
 * @param rotate
 * @constructor
 */
function DragRotate(vm, rotate) {
  console.log('DragRotate !!!!!!')
  _UpdateStyleCenterOnMove(vm)
}

/**
 * 组件旋转结束 鼠标抬起
 * @param vm
 * @param rotate
 * @constructor
 */
function DragRotateEnd(vm, rotate) {}

/**
 * 组件拖动放置到drop内
 * @param vm
 * @param drop 父组件
 * @constructor
 */
function DragAddedToDrop(vm, drop) {
  console.log('drop: ', drop)
  // 登录和授权手机号弹框内部样式
  if (
    store.getters['diy/loginAuthShow'] ||
    store.getters['diy/phoneAuthShow']
  ) {
    let data = store.getters['diy/loginAuthShow']
      ? store.getters['diy/loginAuth']
      : store.getters['diy/phoneAuth']
    console.log('vm: ', vm)
    authAddWegt(vm, data)
    return
  }
  // 是否是容器内组件
  let isSub = false
  // 数据在手机里的上下位置顺序
  let index = -1
  // 有占位符的情况
  if (DataCenter.WegtHoverVM) {
    console.log('DataCenter.WegtHoverVM: ', DataCenter.WegtHoverVM)
    index = DataCenter.WegtHoverVM.dataIndex
    console.log('index: ', index)
  } else {
    isSub = true
    if (drop.getAttribute('type') !== 'block') {
      return
    }
    index = parseInt(parents(drop, 'li.ui-draggable')[0].getAttribute('index'))
  }
  if (typeof index !== 'number') {
    return
  }
  // 添加组件数据到vuex数据
  let id = vm.wegtData.id
  wegtAdd(vm, index, isSub).then(() => {
    if (isSub && StyleCenter.css[id]) {
      StyleCenter.css[id].left = { value: vm.left }
      StyleCenter.css[id].top = { value: vm.top }
      StyleCenter.make(id)
    }
    let data = store.getters['diy/allData'][index]
    store.dispatch('diy/setEditData', data)
    store.dispatch('diy/setEditIndex', index)
  })
  // 移除占位符
  remove('.copyhover')
  DataCenter.WegtHover = null
  DataCenter.WegtHoverVM = null
  vm.dataIndex = null
}

/**
 * 组件全部取消选择
 * @param vm
 * @constructor
 */
function DragClean(vm) {
  console.log('DragClean !!!!: ', vm)
  if (vm.isToolWegt) {
    cleanInvalidVm(vm)
    return
  }
}

export function EventHandleOpen() {
  EventBus.$on(EveBusKeys.DragStart, DragStart)
  EventBus.$on(EveBusKeys.DragEnd, DragEnd)
  EventBus.$on(EveBusKeys.DragDrag, DragDrag)
  EventBus.$on(EveBusKeys.DragDragEnd, DragDragEnd)
  EventBus.$on(EveBusKeys.DragResize, DragResize)
  EventBus.$on(EveBusKeys.DragResizeEnd, DragResizeEnd)
  EventBus.$on(EveBusKeys.DragRotate, DragRotate)
  EventBus.$on(EveBusKeys.DragRotateEnd, DragRotateEnd)
  EventBus.$on(EveBusKeys.DragAddedToDrop, DragAddedToDrop)
  EventBus.$on(EveBusKeys.DragClean, DragClean)
  EventBus.$on(EveBusKeys.DragClick, DragClick)
  EventBus.$on(EveBusKeys.DragDoubleClick, DragDoubleClick)
  EventBus.$on(EveBusKeys.DragDeSelect, DragDeSelect)
  EventBus.$on(EveBusKeys.DragContextMenu, DragContextMenu)
}

export function EventHandleClose() {
  EventBus.$off(EveBusKeys.DragStart, DragStart)
  EventBus.$off(EveBusKeys.DragEnd, DragEnd)
  EventBus.$off(EveBusKeys.DragDrag, DragDrag)
  EventBus.$off(EveBusKeys.DragDragEnd, DragDragEnd)
  EventBus.$off(EveBusKeys.DragResize, DragResize)
  EventBus.$off(EveBusKeys.DragResizeEnd, DragResizeEnd)
  EventBus.$off(EveBusKeys.DragRotate, DragRotate)
  EventBus.$off(EveBusKeys.DragRotateEnd, DragRotateEnd)
  EventBus.$off(EveBusKeys.DragAddedToDrop, DragAddedToDrop)
  EventBus.$off(EveBusKeys.DragClean, DragClean)
  EventBus.$off(EveBusKeys.DragClick, DragClick)
  EventBus.$off(EveBusKeys.DragDoubleClick, DragDoubleClick)
  EventBus.$off(EveBusKeys.DragDeSelect, DragDeSelect)
  EventBus.$off(EveBusKeys.DragContextMenu, DragContextMenu)
}
