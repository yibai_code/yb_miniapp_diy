import { children, css } from '@/components/VueDragDropAlign/dom'
import { DataCenter, StyleCenter } from '@/global'
import store from '@/store'
import { wegtDel } from '@/utils/DomNode'
import Vue from 'vue'
import DragMenu from '@/components/ContextMenu/index'
import { makeId } from '@/common/unit'
import { wegtCopy, wegtPaste } from '@/utils/NodeCopyPaste'

export function contextMenuShow(vm, e) {
  let wegtData = vm.$parent.wegtData

  const clone = {
    text: '克隆',
    info: 'Ctrl + X',
    func: function () {
      let index = vm.$parent.index
      let dataClone = JSON.parse(JSON.stringify(wegtData))
      dataClone.id = makeId()
      if (dataClone.sub) {
        let sub_new = {}
        for (let sid in dataClone.sub) {
          let nid = makeId()
          let sdata = JSON.parse(JSON.stringify(dataClone.sub[sid]))
          sdata.id = nid
          sub_new[nid] = sdata
          StyleCenter.copy(sid, nid)
          StyleCenter.makeAllId(nid)
        }
        dataClone.sub = sub_new
      }
      store.getters['diy/allData'].splice(index + 1, 0, dataClone)
      StyleCenter.copy(wegtData.id, dataClone.id)
      StyleCenter.makeAllId(dataClone.id)
    },
  }
  const del = {
    text: '删除',
    info: 'Delete',
    func: function () {
      wegtDel(wegtData)
    },
  }
  const cloneToPage = {
    text: '复制到指定页面',
    func: function () {
      vm.$baseDialog(import('@/components/TemplateMaket/copy'))
        .className('dialog_size_600_500')
        .data({
          wegtData: wegtData,
        })
        .show()
    },
  }
  const updateZIndex = (zindex) => {
    if (StyleCenter.css[wegtData.id]) {
      StyleCenter.css[wegtData.id]['z-index'] = { value: zindex }
      StyleCenter.make(wegtData.id)
    }
    if (StyleCenter.common[wegtData.id]) {
      StyleCenter.common[wegtData.id]['z-index'] = { value: zindex }
      StyleCenter.make(wegtData.id, 'common')
    }
  }
  const vmIndex = () => {
    let current_index = parseInt(css(vm.$el, 'z-index'))
    current_index = isNaN(current_index) ? 0 : current_index
    return current_index
  }

  let data = []
  switch (wegtData.type) {
    case 'button_submit':
    case 'button_cancel':
      return
      break
    case 'text':
    case 'image':
    case 'button':
      data = [
        {
          text: '剪切',
          info: 'Ctrl + X',
          func: function () {
            wegtCopy(wegtData)
            wegtDel(wegtData)
          },
        },
        {
          text: '复制',
          info: 'Ctrl + C',
          func: function () {
            wegtCopy(wegtData)
          },
        },
        {},
        {
          text: '层级',
          sub: [
            {
              text: '置于顶层',
              func: function () {
                let doms = children(vm.$el.parentNode)
                let id = vm.$el.id
                let current_index = vmIndex()
                let arr = []
                for (let dom of doms) {
                  if (dom.id === id) {
                    continue
                  }
                  let dom_index = parseInt(css(dom, 'z-index'))
                  dom_index = isNaN(dom_index) ? 0 : dom_index
                  arr.push(dom_index)
                }
                let max = Math.max(...arr)
                if (current_index < max + 1) {
                  updateZIndex(max + 1)
                } else {
                  console.log('已经位于最上层')
                }
              },
            },
            {
              text: '置于底层',
              func: function () {
                let doms = children(vm.$el.parentNode)
                let id = vm.$el.id
                let current_index = vmIndex()
                let arr = []
                for (let dom of doms) {
                  if (dom.id === id) {
                    continue
                  }
                  let dom_index = parseInt(css(dom, 'z-index'))
                  dom_index = isNaN(dom_index) ? 0 : dom_index
                  arr.push(dom_index)
                }
                let min = Math.min(...arr)
                if (current_index > min - 1) {
                  if (min - 1 < 0) {
                    updateZIndex(0)
                  } else {
                    updateZIndex(min - 1)
                  }
                } else {
                  console.log('已经位于最底层')
                }
              },
            },
            {
              text: '上移一层',
              func: function () {
                let doms = children(vm.$el.parentNode)
                let id = vm.$el.id
                let current_index = vmIndex()
                let arr = []
                for (let dom of doms) {
                  if (dom.id === id) {
                    continue
                  }
                  let dom_index = parseInt(css(dom, 'z-index'))
                  dom_index = isNaN(dom_index) ? 0 : dom_index
                  arr.push(dom_index)
                }
                let max = Math.max(...arr)
                if (current_index < max + 1) {
                  updateZIndex(current_index + 1)
                } else {
                  console.log('已经位于最上层')
                }
              },
            },
            {
              text: '下移一层',
              func: function () {
                let doms = children(vm.$el.parentNode)
                let id = vm.$el.id
                let current_index = vmIndex()
                let arr = []
                for (let dom of doms) {
                  if (dom.id === id) {
                    continue
                  }
                  let dom_index = parseInt(css(dom, 'z-index'))
                  dom_index = isNaN(dom_index) ? 0 : dom_index
                  arr.push(dom_index)
                }
                let min = Math.min(...arr)
                if (current_index > min - 1) {
                  if (current_index - 1 < 0) {
                    updateZIndex(0)
                  } else {
                    updateZIndex(current_index - 1)
                  }
                } else {
                  console.log('已经位于最底层')
                }
              },
            },
          ],
        },
        {},
        del,
      ]
      break
    case 'auth_block':
      DataCenter.PastePostion = {
        left: e.offsetX,
        top: e.offsetY,
      }
      data = [
        {
          id: 'menu_ctrlv',
          text: '粘贴',
          disable:
            DataCenter.CopyData === null ||
            !DataCenter.CopyData.data.type.is('text', 'image'),
          info: 'Ctrl + V',
          func: function (event) {
            wegtPaste(wegtData)
          },
        },
      ]
      break
    case 'block':
      DataCenter.PastePostion = {
        left: e.offsetX,
        top: e.offsetY,
      }
      data = [
        clone,
        cloneToPage,
        {
          id: 'menu_ctrlv',
          text: '粘贴',
          disable: DataCenter.CopyData === null,
          info: 'Ctrl + V',
          func: function (event) {
            wegtPaste(wegtData)
          },
        },
        {},
        del,
      ]
      break
    default:
      data = [clone, cloneToPage, {}, del]
      break
  }

  let dom = document.body.querySelector('#drag-menu')
  dom && dom.remove()
  dom = document.createElement('div')
  dom.id = 'drag-menu'
  dom.style.cssText = `position:absolute;width:200px;height:400px;left:${e.pageX}px;top:${e.pageY}px;background: rgba(0,0,0,0.4);`
  document.body.appendChild(dom)

  let Tmpl = Vue.extend(DragMenu)
  let menu = new Tmpl({
    data: { id: 'drag-menu', x: e.pageX, y: e.pageY, item: data },
  }).$mount(dom)
  vm.$el.removeAttribute('style')
}

/**
 * 隐藏右键菜单
 */
export function contextMenuHide() {
  let dom = document.body.querySelector('#drag-menu')
  dom && dom.remove()
}
