import store from '@/store'
import { StyleCenter, DataCenter } from '@/global'
import { settingPanleHide } from '@/utils/Page'

export function historyAdd() {}
export function historyReDo() {
  settingPanleHide()
  let historys = store.getters['diy/history']
  console.log('historys: ', historys)
  let index = historys.index
  let list = historys.list
  if (index < 0 || index >= list.length) {
    return
  }
  let history = JSON.parse(JSON.stringify(list[index]))
  console.log('history: ', history)
  let style = history.style
  StyleCenter.common = style.common || {}
  StyleCenter.css = style.css || {}
  StyleCenter.makeAll()

  DataCenter.HistoryCurrent.allData = true
  DataCenter.HistoryCurrent.tabbar = true
  DataCenter.HistoryCurrent.page = true
  DataCenter.HistoryCurrent.common = true
  // 重新关联 allPage 和 StyleCenter
  let currentStyle = history.allPage[history.pageIndex].data.styles
  if (currentStyle.common) {
    currentStyle.common = StyleCenter.common
  }
  if (currentStyle.css) {
    currentStyle.css = StyleCenter.css
  }
  store.commit('diy/SET_ALL_PAGE', history.allPage)
  store.commit('diy/SET_PAGE_INDEX', history.pageIndex)
  store.commit('diy/SET_PAGE_JUMP_HISTORY', history.page_jump_history)
  store.commit('diy/SET_CURRENT_LINK', history.current_link)
  store.commit('diy/SET_APP_CHANGED', true)
}
export function historyBack() {
  store.commit('diy/HistoryBack')
  historyReDo()
}
export function historyGo() {
  store.commit('diy/HistoryGo')
  historyReDo()
}
