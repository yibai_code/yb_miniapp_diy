import { StyleCenter } from '@/global'
import store from '@/store'
import { SettingItems, StyleEdit } from '@/config/WegtStyles'
import StyleSetNav from '@/wegts/common/NavBar/style'
import StyleSetPage from '@/wegts/common/PageStyle/style'
import StyleSetTabar from '@/wegts/common/Tabbar/style'
import DataSetTabbar from '@/wegts/common/Tabbar/data'
import { css } from '@/components/VueDragDropAlign/dom'

export function resetAllDataByCurrentLink(link) {
  settingPanleHide()
  if (link.type !== 'page') {
    store.commit('diy/SET_PAGE_INDEX', -1)
    StyleCenter.styleClean()
    StyleCenter.css = {}
    StyleCenter.makeType('common')
    return
  }
  let allPage = store.getters['diy/allPage']
  allPage.some((page, index) => {
    if (page.id === link.id) {
      store.commit('diy/SET_PAGE_INDEX', index)
      StyleCenter.css = page.data.styles.css
      StyleCenter.makeAll()
      return true
    }
  })
}

export function settingEditNav() {
  let current_link = store.getters['diy/current_link']
  if (current_link.type !== 'page') {
    return
  }
  store.dispatch('diy/setEditIndex', -2)
  store.dispatch('diy/setEditData', {})
  store.dispatch('diy/setSettingPanle', {
    id: 'navbar',
    title: '顶部导航栏',
    settingItem: [SettingItems.style],
    styleItem: [],
    styleSet: [
      {
        name: '高级',
        component: StyleSetNav,
      },
    ],
  })
}

export function settingEditPage() {
  store.dispatch('diy/setEditIndex', -4)
  store.dispatch('diy/setEditData', {})
  store.dispatch('diy/setSettingPanle', {
    id: 'mainblock',
    title: '页面',
    settingItem: [SettingItems.style],
    styleItem: [StyleEdit.bg],
  })
}

export function settingEditTabbar() {
  store.dispatch('diy/setEditIndex', -3)
  store.dispatch('diy/setEditData', {})
  store.dispatch('diy/setSettingPanle', {
    id: 'tabbar',
    title: '底部导航',
    settingItem: [SettingItems.style, SettingItems.data],
    styleItem: [],
    styleSet: [
      {
        name: '高级',
        component: StyleSetTabar,
      },
    ],
    dataSet: DataSetTabbar,
  })
}

/**
 * 清空当前编辑数据 右侧设置菜单隐藏
 */
export function settingPanleHide() {
  store.dispatch('diy/setEditIndex', null)
  store.dispatch('diy/setEditData', {})
  store.dispatch('diy/setSettingPanle', {})
  css('#ctrbox', {
    display: 'none',
  })
}

/**
 * 页面跳转
 * @param link
 */
export function pageJump(link) {
  store.commit('diy/SET_CURRENT_LINK', link)
  store.commit('diy/PAGE_HISTORY_ADD', link)
  settingPanleHide()
}
