import { StyleCenter } from '@/global'
import { historyBack, historyGo } from '@/utils/History'
import { addEvent, removeEvent } from '@/components/VueDragDropAlign/dom'
import store from '@/store'
import { wegtDel } from '@/utils/DomNode'
import { wegtCopy, wegtPaste } from '@/utils/NodeCopyPaste'

function keyDown(event) {
  if (event.target.tagName.is('INPUT', 'TEXTAREA')) {
    return
  }
  event.returnvalue = false
  event.stopPropagation && event.stopPropagation()
  event.preventDefault && event.preventDefault()
  if ((event.ctrlKey || event.metaKey) && event.keyCode === 90) {
    //Ctrl+Z
    event.returnvalue = false
    // 重做
    if (event.shiftKey) {
      historyGo()
    } else {
      historyBack()
    }
    return
  }
  let wegtData = store.getters['diy/editData']
  if (!wegtData || !wegtData.id) {
    return
  }
  // delete键
  if (event.keyCode === 46) {
    wegtDel(wegtData)
    return
  }
  // 全选
  if ((event.ctrlKey || event.metaKey) && event.keyCode === 65) {
    //Ctrl+A
    return
  }
  // 复制
  if ((event.ctrlKey || event.metaKey) && event.keyCode === 67) {
    //Ctrl+C
    if (wegtData.type.is('text', 'button', 'image')) {
      wegtCopy(wegtData)
    }
    return
  }
  // 剪切
  if ((event.ctrlKey || event.metaKey) && event.keyCode === 88) {
    //Ctrl+X
    if (wegtData.type.is('text', 'button', 'image')) {
      wegtCopy(wegtData)
      wegtDel(wegtData)
    }
    return
  }
  // 粘贴
  if ((event.ctrlKey || event.metaKey) && event.keyCode === 86) {
    //Ctrl+V
    if (wegtData.type.is('block', 'auth_block')) {
      wegtPaste(wegtData)
    }
    return
  }
  if (event.keyCode > 36 && event.keyCode < 41) {
    event.stopPropagation && event.stopPropagation()
    event.preventDefault && event.preventDefault()
    if (wegtData.type.is('text', 'button', 'image')) {
      let css = StyleCenter.findById(wegtData.id)
      if (css) {
        switch (event.keyCode) {
          //左
          case 37:
            if (css.left) {
              css.left.value -= 1
            }
            break
          // 上
          case 38:
            if (css.top) {
              css.top.value -= 1
            }
            break
          // 右
          case 39:
            if (css.left) {
              css.left.value += 1
            }
            break
          // 下
          case 40:
            if (css.top) {
              css.top.value += 1
            }
            break
        }
        StyleCenter.make(wegtData.id)
      }
    }
    return
  }
}

export function UnKeyEventHandle() {
  removeEvent(document.documentElement, 'keydown', keyDown)
}
export function KeyEventHandle() {
  addEvent(document.documentElement, 'keydown', keyDown)
}
