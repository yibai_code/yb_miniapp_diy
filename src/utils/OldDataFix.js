/**
 * 数据处理修复 移除无效数据
 * @param arr
 */
import { makeId } from '@/common/unit'
export function oldDataFix(arr) {
  if (!arr) {
    return
  }
  for (let page of arr) {
    if (page.data.length > 0) {
      page.data = JSON.parse(page.data)
    } else {
      page.data = {}
    }
    // 移除无效数据
    if (page.data.all_data) {
      page.data.all_data = page.data.all_data.filter((data) => {
        if (data.type === 'navigation') {
          if (!data.data_key) {
            data.data_key = 'list'
            data.data_type = 'auto'
            data.new_list = []
          }
        } else if (data.type === 'banner') {
          if (!data.data_key) {
            data.data_key = 'list'
            data.data_type = 'auto'
            data.new_list = []
          }
          if (typeof data.type_index === 'undefined') {
            data.type_index = 0
          }
        } else if (data.type === 'diy_search') {
          if (!data.search_type) {
            data.search_type = [
              'goods',
              'pay_content',
              'activity',
              'service',
              'grass',
              'exposure',
            ]
          }
        } else if (data.type === 'diy_form') {
          if (!data.search_type) {
            data.ids = []
          }
        } else if (data.type === 'diy_bargain') {
          // diy砍价
          if (data.type_index == 2) {
            data.type = 'bargaining2'
          } else if (data.type_index == 3) {
            data.type = 'bargaining3'
          }
        } else if (data.type === 'diy_integral') {
          // 积分商城
          if (data.type_index == 2) {
            data.type = 'diy_integral2'
          } else if (data.type_index == 3) {
            data.type = 'diy_integral3'
          }
        } else if (data.type === 'diy_seckill') {
          // 秒杀
          if (data.type_index == 2) {
            data.type = 'seckill2'
          } else if (data.type_index == 3) {
            data.type = 'seckill3'
          } else if (data.type_index == 4) {
            data.type = 'seckill4'
          }
        } else if (data.type === 'diy_group') {
          // 拼团
          if (data.type_index == 2) {
            data.type = 'diy_group2'
          } else if (data.type_index == 3) {
            data.type = 'diy_group3'
          } else if (data.type_index == 4) {
            data.type = 'diy_group4'
          } else if (data.type_index == 5) {
            data.type = 'diy_group5'
          }
        } else if (data.type === 'hotel_entry') {
          // 酒店入住
          data.type = 'hotelCheckIn'
        } else if (data.type === 'diy_videolist_style1') {
          // 短视频列表1
          data.type = 'video_list'
        } else if (data.type === 'diy_videolist_style2') {
          // 短视频列表2
          data.type = 'video_list2'
        } else if (data.type === 'diy_videolist_style3') {
          // 短视频列表3
          data.type = 'video_list3'
        } else if (data.type === 'diy_live_style1') {
          // 直播列表1
          data.type = 'live_list'
        } else if (data.type === 'diy_live_style2') {
          // 直播列表2
          data.type = 'live_list2'
        } else if (data.type === 'diy_live_style3') {
          // 直播列表3
          data.type = 'live_list3'
        } else if (data.type === 'diy_editor') {
          // 富文本编辑
          data.type = 'rich_text'
        } else if (data.type === 'diy_wx_advert') {
          if (!data.wx_qq_id && data.wx_ad_id) {
            data.wx_qq_id = data.wx_ad_id
          }
        } else if (data.type === 'diy_qq_advert') {
          if (!data.wx_qq_id && data.qq_ad_id) {
            data.wx_qq_id = data.qq_ad_id
          }
        }
        if (data.hasOwnProperty('id') && !data.id) {
          data.id = makeId()
        }
        return (
          data.hasOwnProperty('id') &&
          data.id &&
          data.id.indexOf &&
          data.id.indexOf('n1') >= 0
        )
      })
    } else {
      page.data.all_data = []
    }
    // 移除无效样式
    if (page.data.style) {
      let ids = {}
      let all_data = JSON.stringify(page.data.all_data)
      for (let eid in page.data.style.css) {
        let id = eid.split(' ')[0]
        if (!ids.hasOwnProperty(id)) {
          ids[id] = all_data.indexOf(id) >= 0
        }
        if (!ids[id]) {
          delete page.data.style.css[eid]
        }
      }
    }
  }
}

/*
 * @desc 数据处理修复，保存时兼容原始数据
 * @params arr Array
 * */
export function oldSaveDataFix(arr) {
  if (!arr) {
    return arr
  }
  arr = JSON.parse(JSON.stringify(arr))
  arr.forEach((item) => {
    item.data.all_data = item.data.all_data.map((data) => {
      if (data.type === 'banner') {
        if (typeof data.type_index === 'undefined') {
          data.type_index = 1
        }
      } else if (data.type === 'bargaining2') {
        data.type = 'diy_bargain'
        data.type_index = 2
      } else if (data.type === 'diy_bargain') {
        data.type = 'diy_bargain'
        data.type_index = 1
      } else if (data.type === 'bargaining3') {
        data.type = 'diy_bargain'
        data.type_index = 3
      } else if (data.type === 'diy_integral') {
        data.type = 'diy_integral'
        data.type_index = 1
      } else if (data.type === 'diy_integral2') {
        data.type = 'diy_integral'
        data.type_index = 2
      } else if (data.type === 'diy_integral3') {
        data.type = 'diy_integral'
        data.type_index = 3
      } else if (data.type === 'diy_seckill') {
        data.type = 'diy_seckill'
        data.type_index = 1
      } else if (data.type === 'seckill2') {
        data.type = 'diy_seckill'
        data.type_index = 2
      } else if (data.type === 'seckill3') {
        data.type = 'diy_seckill'
        data.type_index = 3
      } else if (data.type === 'seckill4') {
        data.type = 'diy_seckill'
        data.type_index = 4
      } else if (data.type === 'diy_group') {
        data.type = 'diy_group'
        data.type_index = 1
      } else if (data.type === 'diy_group2') {
        data.type = 'diy_group'
        data.type_index = 2
      } else if (data.type === 'diy_group3') {
        data.type = 'diy_group'
        data.type_index = 3
      } else if (data.type === 'diy_group4') {
        data.type = 'diy_group'
        data.type_index = 4
      } else if (data.type === 'diy_group5') {
        data.type = 'diy_group'
        data.type_index = 5
      } else if (data.type === 'hotelCheckIn') {
        data.type = 'hotel_entry'
      } else if (data.type === 'video_list') {
        data.type = 'diy_videolist_style1'
      } else if (data.type === 'video_list2') {
        data.type = 'diy_videolist_style2'
      } else if (data.type === 'video_list3') {
        data.type = 'diy_videolist_style3'
      } else if (data.type === 'live_list') {
        data.type = 'diy_live_style1'
      } else if (data.type === 'live_list2') {
        data.type = 'diy_live_style2'
      } else if (data.type === 'live_list3') {
        data.type = 'diy_live_style3'
      } else if (data.type === 'tencent_live_list') {
        data.type = 'diy_tencent_live_style1'
      } else if (data.type === 'tencent_live_list2') {
        data.type = 'diy_tencent_live_style2'
      } else if (data.type === 'tencent_live_list3') {
        data.type = 'diy_tencent_live_style3'
      } else if (data.type === 'rich_text') {
        data.type = 'diy_editor'
      } else if (data.type === 'diy_wx_advert') {
        if (data.wx_qq_id && !data.wx_ad_id) {
          data.wx_ad_id = data.wx_qq_id
        }
      } else if (data.type === 'diy_qq_advert') {
        if (data.wx_qq_id && !data.qq_ad_id) {
          data.qq_ad_id = data.wx_qq_id
        }
      }
      if (data.is_title_show == 2) {
        data.is_title_show = 0
      }
      return data
    })
  })
  return arr
}
