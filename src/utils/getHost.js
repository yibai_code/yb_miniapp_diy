import { APP_BASEURL } from '@/dev.config'
export default function () {
  if (process.env.NODE_ENV === 'development') {
    return APP_BASEURL.replace(/\/addons\/\S+$/g, '')
  } else {
    return window.location.origin
  }
}
