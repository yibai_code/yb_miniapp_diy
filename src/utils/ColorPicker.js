import Pickr from '@simonwep/pickr'
import { assign } from 'lodash'

const colorpickerconfig = {
  useAsButton: true,
  position: 'bottom-middle',
  swatches: [
    'rgba(222, 68, 58,1)',
    'rgba(58, 130, 248,1)',
    'rgba(86, 187, 55,1)',
    'rgba(237, 93, 41,1)',
    'rgba(235, 52, 119,1)',
    'rgba(245, 180, 62,1)',
    'rgba(87, 190, 173,1)',
    'rgba(173, 152, 109,1)',
    'rgba(115, 96, 240,1)',
    'rgba(51, 51, 51,1)',
    'rgba(255, 255, 255,1)',
    'rgba(153, 153, 153,1)',
    'rgba(238, 238, 238, 1)',
    'rgba(255, 255, 255, 0)',
  ],
  defaultRepresentation: 'RGBA',
  components: {
    preview: true,
    opacity: true,
    hue: true,
    interaction: {
      hex: false,
      rgba: false,
      hsva: false,
      input: true,
      clear: false,
      cancel: true,
      save: true,
    },
  },
  strings: {
    save: '确定',
    cancel: '取消',
  },
  i18n: {
    'btn:save': '确定',
    'btn:cancel': '取消',
    'btn:clear': '清除',
  },
}

export function colorPick(event, defaultVal = '#fff') {
  return new Promise((resolve) => {
    let config = {}
    assign(config, colorpickerconfig, {
      el: event.currentTarget,
      default: defaultVal,
    })
    const pickr = new Pickr(config)
    pickr
      .on('hide', () => {
        setTimeout(function () {
          pickr.destroyAndRemove()
        }, 500)
      })
      .on('save', (...args) => {
        let color, alpha
        if (!args[0]) {
          color = 'rgba(0,0,0,0)'
          alpha = 0
        } else {
          let rgba = args[0].toRGBA()
          rgba[0] = parseInt(rgba[0])
          rgba[1] = parseInt(rgba[1])
          rgba[2] = parseInt(rgba[2])
          color = rgba.toString()
          alpha = args[0].a
        }
        resolve({ color, alpha })
        pickr.hide()
      })
      .on('cancel', () => {
        pickr.hide()
      })
    pickr.show()
  })
}
