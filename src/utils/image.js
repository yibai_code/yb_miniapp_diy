export function imageSize(src) {
  return new Promise((resolve, reject) => {
    let img = new Image()
    img.src = src
    // 判断是否有缓存
    if (img.complete) {
      resolve({ width: img.width, height: img.height })
    } else {
      // 加载完成执行
      img.onload = function () {
        resolve({ width: img.width, height: img.height })
      }
      img.onerror = function () {
        reject(new Error(`图像src加载失败: ${src}`))
      }
    }
  })
}
