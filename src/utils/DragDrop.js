import { StyleCenter, DataCenter } from '@/global'
import Vue from 'vue'
import store from '@/store'
import { makeId } from '@/common/unit'
import { Wegts } from '@/config/WegtConfig'
import { domGet, remove } from '@/components/VueDragDropAlign/dom'
import { contextMenuHide } from '@/utils/ContextMenu'

export function wegtAdd(vm, index, isSub = false) {
  return new Promise((resolve) => {
    if (!vm) {
      resolve(true)
      return
    }
    console.log('vm: ', vm)
    let wegtData = vm.wegtData
    let type = wegtData.type
    let styleIndex = wegtData.styleIndex
    let id = wegtData.id
    if (type === 'text' || type === 'image' || type === 'button') {
      if (!isSub) {
        let bid = makeId()
        initFromWegtTmpl(Wegts['block'], { id: bid }, index)
          .then(() => {
            return initFromWegtTmpl(Wegts[type], { id: id }, index)
          })
          .then(() => {
            handle()
          })
      } else {
        initFromWegtTmpl(Wegts[type], { id: id }, index).then(() => {
          handle()
        })
      }
    } else {
      initFromWegtTmpl(
        Wegts[type],
        { id: id, styleIndex: styleIndex },
        index
      ).then(() => {
        handle()
      })
    }

    function handle() {
      let editIndex = store.getters['diy/editIndex']
      if (editIndex >= index) {
        store.dispatch('diy/setEditIndex', editIndex + 1)
      }
      vm.$el.remove()
      vm.$destroy()
      DataCenter.WegtHover && DataCenter.WegtHover.remove()
      let dom = domGet('.copyhover')
      if (dom) {
        dom.remove()
      }
      window.dragInitClick_x = 0
      window.dragInitClick_y = 0
      resolve(true)
    }
  })
}

export function initFromWegtTmpl(WegtTmpl, data, index) {
  return new Promise((resolve) => {
    WegtTmpl.then((res) => {
      let Wegt = Vue.extend(res.default)
      let tmpl = new Wegt({
        data: data,
      })
      tmpl.init(index)
      resolve(true)
    })
  })
}

export function authAddWegt(vm, data) {
  let wegtData = vm.wegtData
  let type = wegtData.type
  let id = wegtData.id
  let left = vm.left
  let top = vm.top
  Wegts[type].then((res) => {
    let Wegt = Vue.extend(res.default)
    let tmpl = new Wegt({
      data: { id: id },
    })
    tmpl.initStyle()
    StyleCenter.css[id].left = { value: left }
    StyleCenter.css[id].top = { value: top }
    StyleCenter.make(id)
  })
  let item = {
    id: vm.wegtData.id,
    type: type,
    link: {
      type: 'none',
      type_name: '无链接',
      id: '',
      url: '',
      title: '无链接',
    },
  }
  if (type === 'text') {
    item.value = '请输入内容...'
  }
  Vue.set(data, item.id, item)
}

/**
 * 清理无效VM
 * @param vm
 */
export function cleanInvalidVm(vm) {
  remove('.copyhover')
  DataCenter.WegtHover = null
  DataCenter.WegtHoverVM = null
  vm && vm.$el && vm.$el.remove()
  vm && vm.$destroy && vm.$destroy()
  DataCenter.DragInit = false
  contextMenuHide()
}
