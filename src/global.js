import Vue from 'vue'
import Style from './common/Style'
import DTC from './common/DataCenter'
export const EventBus = new Vue()
export const DataCenter = new DTC()
export const StyleCenter = new Style()

window.StyleCenter = StyleCenter
