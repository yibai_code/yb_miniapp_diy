import { StyleCenter } from '@/global'
import DeepProxy from '@/common/DeepProxy'

const state = {
  // 所有页面
  allPage: [],
  // 当前编辑组件在列表中的索引
  editIndex: -1,
  // 当前编辑组件的数据
  editData: {},
  // 右侧编辑框的数据
  settingPanle: {},
  loginAuthShow: false,
  phoneAuthShow: false,
  // 操作历史
  history: {
    index: -1, // 当前操作序号 根据此序号 操作回退或重做
    list: [], // 当前操作记录 记录style和nodes
  },
  // 页面跳转历史
  page_jump_history: [],
  // 当前生效链接 底部导航点击 页面功能点击跳转等
  current_link: {},
  // 当前编辑样式标识 css | common
  styleFlag: 'css',
  appChanged: false,
  pageIndex: -1,
  systemPage: [],
}
const getters = {
  allPage: (state) => {
    return state.allPage
  },
  allData: function (state) {
    if (state.pageIndex >= 0 && state.pageIndex < state.allPage.length) {
      return state.allPage[state.pageIndex].data.all_data
    }
    return []
  },
  tabbar: function (state) {
    if (state.allPage.length === 0) {
      return {}
    }
    return state.allPage[0].data.tabbar
  },
  page: function (state) {
    if (state.pageIndex >= 0 && state.pageIndex < state.allPage.length) {
      return state.allPage[state.pageIndex].data.page
    }
    return {}
  },
  common: function (state) {
    if (state.allPage.length === 0) {
      return {}
    }
    return state.allPage[0].data.common
  },
  page_jump_history: (state) => state.page_jump_history,
  current_link: (state) => state.current_link,
  editIndex: (state) => state.editIndex,
  editData: (state) => state.editData,
  settingPanle: (state) => state.settingPanle,
  globleColor: function (state) {
    if (state.allPage.length === 0) {
      return {}
    }
    return state.allPage[0].data.common.globle
  },
  backgroundColor: function (state) {
    if (state.allPage.length === 0) {
      return {}
    }
    return state.allPage[0].data.common.background
  },
  loginAuth: function (state) {
    if (state.allPage.length === 0) {
      return {}
    }
    return state.allPage[0].data.common.login_auth.sub
  },
  phoneAuth: function (state) {
    if (state.allPage.length === 0) {
      return {}
    }
    return state.allPage[0].data.common.phone_auth.sub
  },
  loginAuthShow: (state) => state.loginAuthShow,
  phoneAuthShow: (state) => state.phoneAuthShow,
  floatBtn: function (state) {
    if (state.allPage.length === 0) {
      return null
    }
    return state.allPage[0].data.common.float
  },
  styleFlag: (state) => state.styleFlag,
  appChanged: (state) => state.appChanged,
  history: (state) => state.history,
  pageIndex: (state) => state.pageIndex,
  systemPage: (state) =>
    state.systemPage.map((item) => {
      let find = state.allPage.find((page) => page.tmpl_type === item.tmpl_type)
      if (find) {
        return Object.assign({}, item, find)
      } else {
        return item
      }
    }),
}
const mutations = {
  SET_PAGE_INDEX(state, index) {
    state.pageIndex = index
  },
  SET_APP_CHANGED(state, obj) {
    state.appChanged = obj
  },
  SET_LoginAuthShow(state) {
    state.loginAuthShow = !state.loginAuthShow
    if (state.loginAuthShow) {
      state.phoneAuthShow = false
    }
    if (state.loginAuthShow || state.phoneAuthShow) {
      state.styleFlag = 'common'
    } else {
      state.styleFlag = 'css'
    }
    state.editIndex = null
    state.editData = {}
    state.settingPanle = {}
  },
  SET_PhoneAuthShow(state) {
    state.phoneAuthShow = !state.phoneAuthShow
    if (state.phoneAuthShow) {
      state.loginAuthShow = false
    }
    if (state.loginAuthShow || state.phoneAuthShow) {
      state.styleFlag = 'common'
    } else {
      state.styleFlag = 'css'
    }
    state.editIndex = null
    state.editData = {}
    state.settingPanle = {}
  },
  PAGE_HISTORY_CLEAN(state) {
    state.page_jump_history.splice(0)
  },
  PAGE_HISTORY_ADD(stata, link) {
    state.page_jump_history.push(link)
  },
  PAGE_HISTORY_BACK(stata) {
    state.page_jump_history.pop()
    state.current_link =
      state.page_jump_history[state.page_jump_history.length - 1]
  },
  SET_PAGE_JUMP_HISTORY(state, list) {
    state.page_jump_history = list
  },
  SET_CURRENT_LINK(state, link) {
    state.current_link = link
  },
  SET_ALL_PAGE(state, obj) {
    state.allPage = obj
  },
  SET_ALL_DATA(state, obj) {
    state.allPage[state.pageIndex].data.all_data = obj
  },
  ADD_ALL_PAGE(state, obj) {
    state.allPage.push(obj)
  },
  DEL_ALL_PAGE(state, index) {
    state.allPage.splice(index, 1)
  },
  UPDATE_ALL_PAGE(state, { item, index }) {
    state.allPage.splice(index, 1, item)
  },
  ALL_DATA_ADD(state, { item, index }) {
    state.allPage[state.pageIndex].data.all_data.splice(index, 0, item)
  },
  ALL_DATA_DEL(state, index) {
    let data = state.allPage[state.pageIndex].data.all_data[index]
    if (data.id) {
      StyleCenter.remove(data.id)
    }
    if (data.sub) {
      for (let sid in data.sub) {
        StyleCenter.remove(sid)
      }
    }
    state.allPage[state.pageIndex].data.all_data.splice(index, 1)
  },
  ALL_DATA_UPDATE(state, { index, item }) {
    state.allPage[state.pageIndex].data.all_data.splice(index, 1, item)
  },
  SET_EDIT_INDEX(state, index) {
    state.editIndex = index
  },
  SET_EDIT_DATA(state, obj) {
    if (!obj) {
      return
    }
    if (state.loginAuthShow || state.phoneAuthShow) {
      state.styleFlag = 'common'
    } else {
      state.styleFlag = 'css'
    }
    if (state.history.list.length > 0) {
      let hasChanged = false
      let oid = state.editData.id
      let history = state.history.list[state.history.list.length - 1]
      let preStyle = history.style
      for (let status of ['css', 'common']) {
        for (let eid in preStyle[status]) {
          if (eid.indexOf(oid) >= 0) {
            let preStr = JSON.stringify(preStyle[status][eid])
            let currentStr = JSON.stringify(StyleCenter[status][eid])
            if (preStr !== currentStr) {
              hasChanged = true
              break
            }
          }
        }
        if (hasChanged) {
          break
        }
      }
      if (hasChanged) {
        this.commit('diy/HistoryAdd')
      }
    }
    state.editData = obj
    if (!obj.id) {
      return
    }
    let styles = StyleCenter.findAllById(obj.id)
    console.log('styles: ', styles)
    for (let id in styles) {
      let style = StyleCenter.css[id] ? StyleCenter.css : StyleCenter.common
      style[id] = new DeepProxy(styles[id], function () {
        let flag = arguments[0]
        switch (flag) {
          case 'set':
            state.appChanged = true
            break
          case 'delete':
            state.appChanged = true
            break
        }
      })
    }
  },
  SET_SETTING_PANLE(state, obj) {
    if (obj.styleFlag) {
      state.styleFlag = obj.styleFlag
    } else {
      state.styleFlag = 'css'
    }
    if (state.loginAuthShow || state.phoneAuthShow) {
      state.styleFlag = 'common'
    }
    delete obj.styleFlag
    state.settingPanle = obj
  },
  SET_SYSTEM_PAGE(state, data) {
    state.systemPage = data
  },
  HistoryClean(state) {
    state.history.list.splice(0)
    state.history.index = -1
  },
  HistoryAdd(state) {
    if (state.history.list.length === 100) {
      state.history.list.splice(0, 1)
      state.history.index -= 1
    }
    state.history.list.splice(
      state.history.index + 1,
      state.history.list.length - state.history.index
    )
    let obj = {
      style: {
        css: JSON.parse(JSON.stringify(StyleCenter.css)),
        common: JSON.parse(JSON.stringify(StyleCenter.common)),
      },
      page_jump_history: JSON.parse(JSON.stringify(state.page_jump_history)),
      current_link: JSON.parse(JSON.stringify(state.current_link)),
      allPage: JSON.parse(JSON.stringify(state.allPage)),
      pageIndex: state.pageIndex,
    }
    state.history.list.push(obj)
    state.history.index += 1
  },
  HistoryBack(state) {
    if (state.history.index > 0) {
      state.history.index -= 1
    }
  },
  HistoryGo(state) {
    if (state.history.index < state.history.list.length - 1) {
      state.history.index += 1
    }
  },
}
const actions = {
  setCurrentLink({ commit, getters }, link) {
    if (getters.systemPage.length) {
      let find = getters.systemPage.find(
        (item) => item.old_type.indexOf(link.type) > -1 && item.status
      )
      if (find) {
        link = {
          type: 'page',
          type_name: '万能页面',
          id: find.id,
          url: '',
          title: find.title,
          tmpl_type: find.tmpl_type,
          status: find.status,
          data_id: link.id || 0,
          data_title: link.title || find.title,
        }
      }
    }
    commit('SET_CURRENT_LINK', link)
  },
  setAllPage({ commit }, pages) {
    commit('SET_ALL_PAGE', pages)
  },
  wegtAdd({ commit }, { item, index }) {
    commit('ALL_DATA_ADD', { item, index })
  },
  wegtDel({ commit }, index) {
    commit('ALL_DATA_DEL', index)
  },
  wegtUpdate({ commit }, { item, index }) {
    commit('ALL_DATA_UPDATE', { item, index })
  },
  setEditIndex({ commit }, index) {
    commit('SET_EDIT_INDEX', index)
  },
  setEditData({ commit }, obj) {
    commit('SET_EDIT_DATA', obj)
  },
  setSettingPanle({ commit }, obj) {
    commit('SET_SETTING_PANLE', obj)
  },
}
export default { state, getters, mutations, actions }
