const state = {
  list: [],
}
const getters = {}

const mutations = {
  /*
   * @desc 更新模块配置数据
   * */
  updateModuleConfig(state, data) {
    let findIndex = state.list.find(
      (item) => item.type === data.type && data.id === item.id
    )
    if (findIndex === -1) {
      state.list.push(data)
    } else {
      state.list.splice(findIndex, 1, data)
    }
  },
}
const actions = {
  /*
   * @desc 获取模板配置
   * */
  getModuleConfig({ state }, { id = 0, type = '' }) {
    if (id) {
      let find = state.list.find((item) => item.id === id)
      return find || {}
    }
    if (type) {
      let find = state.list.find((item) => item.type === type)
      return find || {}
    }
  },
}

export default { state, getters, mutations, actions }
