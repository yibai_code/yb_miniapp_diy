const state = {
  userPopShow: false,
}
const getters = {
  userPopShow: (state) => state.userPopShow,
}
const mutations = {
  SET_USER_POP_SHOW(state, val) {
    state.userPopShow = val
  },
}
const actions = {}
export default { state, getters, mutations, actions }
