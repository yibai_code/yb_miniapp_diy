/**
 * @copyright chuzhixin 1204505056@qq.com
 * @description 导入所有 vuex 模块，自动加入namespaced:true，用于解决vuex命名冲突，请勿修改。
 */

import Vue from 'vue'
import Vuex from 'vuex'
import frameworkStore from '@yb_fromwork/admin-base/src/store/index'
import ImageUploaderStore from '@/components/CommonImageUpLoad/store'
import DragDropStore from '@/components/VueDragDropAlign/store'

Vue.use(Vuex)
const files = require.context('./modules', false, /\.js$/)
const modules = {}

files.keys().forEach((key) => {
  modules[key.replace(/(\.\/|\.js)/g, '')] = files(key).default
})
Object.keys(modules).forEach((key) => {
  modules[key]['namespaced'] = true
  frameworkStore.registerModule(key, modules[key])
})
frameworkStore.registerModule('imageUploader', ImageUploaderStore)
frameworkStore.registerModule('dragDrop', DragDropStore)

export default frameworkStore
