/**
 * 开发模式配置项 开发调试时设置成线上的对应值
 * @type {string}
 */

/**
 * 后台的PHPSESSID 主要用于前后台跨域数据同步s
 * @type {string}
 */
export const APP_PHPSESSID = 'r4fbi9ouhofsfdna44v66g4h44'
/**
 * 开发模式的请求接口
 * @type {string}
 */
export const APP_BASEURL =
  'https://dev6.sssvip.net/addons/yb_platform/core/index.php?s=/'
