import Vue from 'vue'
import { DataCenter } from '@/global'
import { makeId } from '@/common/unit'
import { EveBusKeys } from '@/config/EveBusKeys'
import { EventBus } from '@/global'
import { imageSize } from '@/utils/image'
import {
  addClass,
  css,
  domGet,
  domGets,
  offset,
  removeClass,
} from '@/components/VueDragDropAlign/dom'
import { extend } from 'lodash'
import store from '@/store'

export const DragMixins = {
  methods: {
    /**
     *从组件库拖动
     * @param e  拖送事件
     * @param image 占位图
     * @param type 组件类型 会根据此类型显示具体的样式 数据 在config.js里配置
     * @param index 组件样式序号
     */
    async dragInit(e, image, type, index) {
      if (e instanceof MouseEvent && e.isTrusted && e.which !== 1) {
        return
      }
      removeClass('.x-draggable', 'x-draggable')
      domGets('.nodeTool,.handleDot,.x-rotationdiv,.x-toolTip').forEach(
        (item) => {
          item.remove()
        }
      )
      EventBus.$emit(EveBusKeys.DragFromToolBar)
      e.stopPropagation && e.stopPropagation()
      e.preventDefault && e.preventDefault()

      let el = e.target
      let n = document.documentElement,
        o = { scrollLeft: n.scrollLeft, scrollTop: n.scrollTop }
      let s = document.createElement('img')
      s.setAttribute('src', image)
      let size = await imageSize(image)
      if (size.width > 350) {
        let bl = size.height / size.width
        size.width = 350
        size.height = bl * 350
      }
      let d = { width: size.width + 1, height: size.height + 1 }
      let eof = offset(el)
      let style = extend(
        {
          'z-index': 1000,
          left: eof.left - o.scrollLeft + 3,
          top: eof.top - o.scrollTop + 3,
          position: 'fixed',
        },
        d
      )
      console.log('style: ', style)
      let node = document.createElement('div')
      node.appendChild(s)
      addClass(node, 'dragingElementBody')
      css(node, style)

      let id = makeId()
      node.setAttribute('type', type)
      node.setAttribute('type-index', index)
      node.setAttribute('id', id)
      domGet('body').appendChild(node)
      DataCenter.DragInit = true
      let editIndex = store.getters['diy/editIndex']
      let allData = store.getters['diy/allData']
      if (typeof editIndex === 'number') {
        editIndex += 1
      } else {
        editIndex = allData.length
      }
      editIndex = editIndex > allData.length ? allData.length : editIndex
      DataCenter.WegtEditIndex = editIndex
      let WegtTmpl = require('@/wegts/ToolWegt').default
      let Wegt = Vue.extend(WegtTmpl)
      let tmpl = new Wegt({
        data: {
          id: id,
          x: style.left,
          y: style.top,
          w: style.width,
          h: style.height,
          src: image,
          wegtData: {
            id: id,
            type: type,
            styleIndex: index,
          },
        },
      }).$mount(node)
      tmpl.$nextTick(() => {
        css(`#${id}`, {
          width: style.width,
          height: style.height,
          left: style.left,
          top: style.top,
          'z-index': 500,
        })
        tmpl.dragInit(e)
      })
    },
  },
}
