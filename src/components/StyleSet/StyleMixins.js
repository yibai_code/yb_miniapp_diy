import { mapGetters } from 'vuex'
export const StyleMixins = {
  props: {
    eid: { type: String, default: '' },
    status: { type: String, default: 'css' },
  },
  computed: {
    ...mapGetters('diy', {
      editData: 'editData',
    }),
  },
  watch: {
    'editData.type': {
      handler() {
        this.reinit()
      },
      immediate: true,
    },
    'editData.type_index': {
      handler(val) {
        this.reinit()
      },
      immediate: true,
    },
    status: {
      handler() {
        this.reinit()
      },
      immediate: false,
      deep: true,
    },
    eid: {
      handler() {
        this.reinit()
      },
      immediate: true,
      deep: true,
    },
  },
}
