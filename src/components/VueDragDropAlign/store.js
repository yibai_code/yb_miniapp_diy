import Vue from 'vue'
const state = {
  select: {},
  selectedNodes: [],
  mutiDrag: false,
}
const getters = {
  select: (state) => state.select,
  selectedNodes: (state) => state.selectedNodes,
  mutiDrag: (state) => state.mutiDrag,
}
const mutations = {
  SET_SELECTED_NODES(state, val) {
    state.selectedNodes = val
  },
  ADD_SELECT(state, { uid, id }) {
    Vue.set(state.select, uid, id)
    state.mutiDrag = Object.keys(state.select).length > 1
  },
  DEL_SELECT(state, uid) {
    Vue.delete(state.select, uid)
    state.mutiDrag = Object.keys(state.select).length > 1
  },
  CLEAN_SELECT(state) {
    state.select = {}
    state.selectedNodes = []
    state.mutiDrag = false
  },
}
const actions = {}
export default { state, getters, mutations, actions, namespaced: true }
