import Vue from 'vue'
import store from '@/store'

const getSelectedNodes = () => {
  let arr = []
  for (let uid in DragDropDataCenter.select) {
    let vm = DragDropDataCenter.select[uid]
    let id = vm.extendData.id
    if (id) {
      arr.push(id)
    }
  }
  return arr
}

export const DragDropDataCenter = {
  select: {},
  active: -1,
  add: function (uid, vm, isActive = false) {
    this.select[uid] = vm
    store.commit('dragDrop/ADD_SELECT', { uid: uid, id: vm.extendData.id })
    store.commit('dragDrop/SET_SELECTED_NODES', getSelectedNodes())
    if (isActive) {
      this.active = uid
    }
  },
  del: function (uid) {
    delete this.select[uid]
    delete store.getters['dragDrop/select'][uid]
    store.commit('dragDrop/DEL_SELECT', uid)
    store.commit('dragDrop/SET_SELECTED_NODES', getSelectedNodes())
    if (uid === this.active) {
      this.active = -1
    }
  },
  clean: function () {
    this.active = -1
    this.select = {}
    store.commit('dragDrop/CLEAN_SELECT')
  },
}
