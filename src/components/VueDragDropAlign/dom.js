import { isFunction } from './fns'

export function matchesSelectorToParentElements(el, selector, baseNode) {
  let node = el

  const matchesSelectorFunc = [
    'matches',
    'webkitMatchesSelector',
    'mozMatchesSelector',
    'msMatchesSelector',
    'oMatchesSelector',
  ].find((func) => isFunction(node[func]))

  if (!isFunction(node[matchesSelectorFunc])) return false

  do {
    if (node[matchesSelectorFunc](selector)) return true
    if (node === baseNode) return false
    node = node.parentNode
  } while (node)

  return false
}

export function addEvent(el, event, handler) {
  if (!el) {
    return
  }
  if (el.attachEvent) {
    el.attachEvent('on' + event, handler)
  } else if (el.addEventListener) {
    el.addEventListener(event, handler, true)
  } else {
    el['on' + event] = handler
  }
}

export function removeEvent(el, event, handler) {
  if (!el) {
    return
  }
  if (el.detachEvent) {
    el.detachEvent('on' + event, handler)
  } else if (el.removeEventListener) {
    el.removeEventListener(event, handler, true)
  } else {
    el['on' + event] = null
  }
}

export function removeClass(el, arr) {
  if (!el) {
    return
  }
  if (typeof el === 'string') el = document.querySelectorAll(el)
  const els = el instanceof NodeList ? [...el] : el instanceof Node ? [el] : el
  let exclude = typeof arr === 'string' ? arr.split(' ') : arr
  els.forEach((el) => {
    if (el.classList) {
      el.classList.remove(...exclude)
    } else {
      el.className = el.className
        .split(' ')
        .filter((item) => {
          return !exclude.includes(item)
        })
        .join(' ')
    }
  })
}

export function hasClass(el, arr) {
  if (!el) {
    return
  }
  let include = typeof arr === 'string' ? arr.split(' ') : arr
  if (typeof el.className !== 'string') {
    return
  }
  let className = el.className.split(' ')
  return include.every((item) => {
    return className.includes(item)
  })
}

export function addClass(el, arr) {
  if (typeof el === 'string') el = document.querySelectorAll(el)
  const els = el instanceof NodeList ? [...el] : el instanceof Node ? [el] : el
  if (!els) {
    return
  }
  let add = typeof arr === 'string' ? arr.split(' ') : arr
  els.forEach((el) => {
    if (el.classList) {
      el.classList.add(...add)
    } else {
      let className = el.className.split(' ')
      let filted = add.filter((item) => {
        return !className.includes(item)
      })
      className.push(...filted)
      el.className = className.join(' ')
    }
  })
}

export function matchesFun() {
  let node = document.body
  const matchesSelectorFunc = [
    'matches',
    'webkitMatchesSelector',
    'mozMatchesSelector',
    'msMatchesSelector',
    'oMatchesSelector',
  ].find((func) => isFunction(node[func]))
  if (!isFunction(node[matchesSelectorFunc])) return false
  return matchesSelectorFunc
}

export function parents(el, selector = null) {
  if (typeof el === 'string') el = document.querySelector(el)
  let node = el
  let matchs = matchesFun()
  let list = []
  do {
    node = node.parentNode
    if (!node || node === document) {
      break
    }
    if (selector) {
      if (node[matchs] && node[matchs](selector)) {
        list.push(node)
      }
    } else {
      list.push(node)
    }
  } while (node)
  return list
}

export function domGet(selector) {
  if (!selector) {
    return false
  }
  return document.querySelector(selector)
}

export function domGets(selector) {
  if (!selector) {
    return false
  }
  let nodes = document.querySelectorAll(selector)
  return [...nodes]
}

export function hasSelector(el, selector) {
  if (typeof el === 'string') el = document.querySelector(el)
  let nodes = document.querySelectorAll(selector)
  return [...nodes].some((node) => {
    return node === el
  })
}

export function children(el, selector) {
  if (typeof el === 'string') el = document.querySelector(el)
  if (!el) {
    return []
  }
  let nodes = el.children
  nodes = [...nodes]
  if (!selector) {
    return nodes
  }
  let matchs = matchesFun()
  return nodes.filter((dom) => {
    return dom[matchs](selector)
  })
}

export function childrenZIndexs(el) {
  if (typeof el === 'string') el = document.querySelector(el)
  if (!el || !el.children) {
    return [-1]
  }
  let nodes = el.children
  nodes = [...nodes]
  return nodes
    .map((node) => {
      let z = parseInt(css(node, 'z-index'))
      z = isNaN(z) ? 0 : z
      return z
    })
    .sort()
}

export function find(el, selector) {
  if (typeof el === 'string') el = document.querySelector(el)
  if (!el) {
    return []
  }
  let list = children(el, selector)
  if (el && el.children) {
    for (let dom of el.children) {
      let sub = find(dom, selector)
      list.push(...sub)
    }
  }
  return list
}

export function remove(el) {
  if (typeof el === 'string') {
    ;[].forEach.call(document.querySelectorAll(el), (node) => {
      node.parentNode.removeChild(node)
    })
  } else if (el.parentNode) {
    el.parentNode.removeChild(el)
  } else if (el instanceof NodeList || el instanceof Array) {
    ;[].forEach.call(el, (node) => {
      node.parentNode.removeChild(node)
    })
  } else {
    throw new Error(
      'you can only pass Element, array of Elements or query string as argument'
    )
  }
}

/**
 * 获取或设置el的css
 * @param el
 * @param style string: 获取 | object: 设置
 * @returns {string}
 */
export function css(el, style) {
  if (typeof style === 'string') {
    return getComputedStyle(el, null).getPropertyValue(style)
  }
  const reUnit = /width|height|top|left|right|bottom|margin|padding/i
  if (typeof el === 'string') el = document.querySelectorAll(el)
  const els = el instanceof NodeList ? [...el] : el instanceof Node ? [el] : el
  els.forEach((node) => {
    for (let k in style) {
      if (reUnit.test(k) && typeof style[k] === 'number') {
        node.style[k] = `${style[k]}px`
        continue
      }
      node.style[k] = style[k]
    }
  })
}

export function offset(el, el1 = null) {
  if (typeof el === 'string') el = document.querySelector(el)
  let scrollX = window.scrollX || window.pageXOffset
  let scrollY = window.scrollY || window.pageYOffset
  let rect = el.getBoundingClientRect()
  if (!el1) {
    return {
      left: rect.left + scrollX,
      top: rect.top + scrollY,
      right: rect.right + scrollX,
      bottom: rect.bottom + scrollY,
      width: rect.width,
      height: rect.height,
    }
  } else {
    if (typeof el1 === 'string') el1 = document.querySelector(el1)
    let rect1 = el1.getBoundingClientRect()
    return {
      left: rect.left - rect1.left,
      top: rect.top - rect1.top,
      right: rect.right - rect1.right,
      bottom: rect.bottom - rect1.bottom,
      width: rect.width,
      height: rect.height,
    }
  }
}

/**
 * matrix 转 css transform skewY不能设置 否则结果不准确
 * @param matrix
 * @returns {{scaleX: number, rotate: number, scaleY: number, translateY: *, translateX: *, skewX: number}}
 */
export function matrixToTransform(matrix) {
  if (typeof matrix !== 'string' || matrix.indexOf('(') < 0) {
    return {
      translateX: 0,
      translateY: 0,
      rotate: 0,
      skewX: 0,
      scaleX: 0,
      scaleY: 0,
    }
  }
  let values = matrix
    .split('(')[1]
    .split(')')[0]
    .split(',')
    .map((item) => {
      return parseFloat(item.trim())
    })
  let a = values[0]
  let b = values[1]
  let c = values[2]
  let d = values[3]
  let e = values[4]
  let f = values[5]

  let degrees = 180 / Math.PI
  let scaleX, scaleY, skewX
  if ((scaleX = Math.sqrt(a * a + b * b))) (a /= scaleX), (b /= scaleX)
  if ((skewX = a * c + b * d)) (c -= a * skewX), (d -= b * skewX)
  if ((scaleY = Math.sqrt(c * c + d * d)))
    (c /= scaleY), (d /= scaleY), (skewX /= scaleY)
  if (a * d < b * c) (a = -a), (b = -b), (skewX = -skewX), (scaleX = -scaleX)

  return {
    translateX: e,
    translateY: f,
    rotate: parseFloat((Math.atan2(b, a) * degrees).toFixed(4)),
    skewX: parseFloat((Math.atan(skewX) * degrees).toFixed(4)),
    scaleX: parseFloat(scaleX.toFixed(4)),
    scaleY: parseFloat(scaleY.toFixed(4)),
  }
}

export function attr(el, key) {
  if (typeof el === 'string') el = document.querySelectorAll(el)
  return el.getAttribute(key)
}
