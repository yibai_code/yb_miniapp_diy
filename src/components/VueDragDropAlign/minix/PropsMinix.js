export const PropsMinix = {
  props: {
    dragDropDisable: {
      type: Boolean,
      default: false,
    },
    className: {
      type: String,
      default: 'vdr',
    },
    classNameDraggable: {
      type: String,
      default: 'draggable',
    },
    classNameResizable: {
      type: String,
      default: 'resizable',
    },
    classNameDragging: {
      type: String,
      default: 'dragging',
    },
    classNameResizing: {
      type: String,
      default: 'resizing',
    },
    classNameActive: {
      type: String,
      default: 'active',
    },
    classNameHandle: {
      type: String,
      default: 'handle',
    },
    classNameRotate: {
      type: String,
      default: 'rotate',
    },
    classNameContextMenu: {
      type: String,
      default: 'dragDropContextMenu',
    },
    disableUserSelect: {
      type: Boolean,
      default: true,
    },
    enableNativeDrag: {
      type: Boolean,
      default: false,
    },
    preventDeactivation: {
      type: Boolean,
      default: false,
    },
    align: {
      type: Boolean,
      default: true,
    },
    active: {
      type: Boolean,
      default: false,
    },
    draggable: {
      type: Boolean,
      default: true,
    },
    rotatable: {
      type: Boolean,
      default: true,
    },
    showTips: {
      type: Boolean,
      default: true,
    },
    dropable: {
      type: Boolean,
      default: false,
    },
    dropinable: {
      type: Boolean,
      default: false,
    },
    resizable: {
      type: Boolean,
      default: true,
    },
    contextmenuable: {
      type: Boolean,
      default: true,
    },
    lockAspectRatio: {
      type: Boolean,
      default: false,
    },
    mutleDragAble: {
      type: Boolean,
      default: false,
    },
    w: {
      type: [Number, String],
      default: 200,
      validator: (val) => {
        if (typeof val === 'number') {
          return val > 0
        }

        return val === 'auto'
      },
    },
    h: {
      type: [Number, String],
      default: 200,
      validator: (val) => {
        if (typeof val === 'number') {
          return val > 0
        }

        return val === 'auto'
      },
    },
    minWidth: {
      type: Number,
      default: 0,
      validator: (val) => val >= 0,
    },
    minHeight: {
      type: Number,
      default: 0,
      validator: (val) => val >= 0,
    },
    maxWidth: {
      type: Number,
      default: null,
      validator: (val) => val >= 0,
    },
    maxHeight: {
      type: Number,
      default: null,
      validator: (val) => val >= 0,
    },
    x: {
      type: Number,
      default: 0,
    },
    y: {
      type: Number,
      default: 0,
    },
    r: {
      type: Number,
      default: 0,
    },
    z: {
      type: [String, Number],
      default: 'auto',
      validator: (val) => (typeof val === 'string' ? val === 'auto' : val >= 0),
    },
    handles: {
      type: Array,
      default: () => ['tl', 'tm', 'tr', 'mr', 'br', 'bm', 'bl', 'ml'],
      validator: (val) => {
        const s = new Set(['tl', 'tm', 'tr', 'mr', 'br', 'bm', 'bl', 'ml'])
        return new Set(val.filter((h) => s.has(h))).size === val.length
      },
    },
    dragHandle: {
      type: String,
      default: null,
    },
    dragCancel: {
      type: String,
      default: null,
    },
    axis: {
      type: String,
      default: 'both',
      validator: (val) => ['x', 'y', 'both'].includes(val),
    },
    grid: {
      type: Array,
      default: () => [1, 1],
    },
    parent: {
      type: Boolean,
      default: false,
    },
    lock: {
      type: Boolean,
      default: false,
    },
    scale: {
      type: Number,
      default: 1,
      validator: (val) => val > 0,
    },
    dropAccess: {
      type: Array,
      default: function () {
        return []
      },
    },
    dropNotAccess: {
      type: Array,
      default: function () {
        return []
      },
    },
    extendData: {
      type: Object,
      default: function () {
        return {}
      },
    },
    tag: {
      type: String,
      default: 'div',
    },
  },
}
