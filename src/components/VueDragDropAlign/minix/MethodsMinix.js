import {
  addClass,
  addEvent,
  children,
  css,
  domGet,
  domGets,
  hasClass,
  hasSelector,
  matchesSelectorToParentElements,
  matrixToTransform,
  parents,
  removeClass,
  removeEvent,
} from '@/components/VueDragDropAlign/dom'
import {
  clientBoundsPoints,
  clientCenterPoint,
  domExactSize,
  polygonIsInPolygon,
  rectBoundsToRect,
  rotatePoint,
} from '@xpf0000/dom-points'
import { DragDropDataCenter } from '@/components/VueDragDropAlign'
import { EventBus } from '@/global'
import {
  computeHeight,
  computeWidth,
  restrictToBounds,
  snapToGrid,
} from '@/components/VueDragDropAlign/fns'

const events = {
  mouse: {
    start: 'mousedown',
    move: 'mousemove',
    stop: 'mouseup',
  },
  touch: {
    start: 'touchstart',
    move: 'touchmove',
    stop: 'touchend',
  },
}
let eventsFor = events.mouse

export const MethodsMinix = {
  methods: {
    getElmPosition() {
      let position = css(this.$el, 'position')
      if (position.is('absolute', 'fixed')) {
        let marginLeft = parseFloat(css(this.$el, 'margin-left')) ?? 0
        let marginTop = parseFloat(css(this.$el, 'margin-top')) ?? 0
        this.left = this.$el.offsetLeft - marginLeft
        this.top = this.$el.offsetTop - marginTop
      } else {
        this.left = 0
        this.top = 0
      }
      this.width = this.$el.offsetWidth || this.$el.clientWidth
      this.height = this.$el.offsetHeight || this.$el.clientHeight
      let matrix = css(this.$el, 'transform')
      this.rotate = matrixToTransform(matrix).rotate
      this.zIndex = css(this.$el, 'z-index')
    },
    //对齐线检测
    alignCheck(vm) {
      removeClass(this.$el, 'dragdrop-align-active')
      let vmLeft = vm.left
      let vmTop = vm.top
      let alignRange = vm.alignRange
      let checkpoints = [...vm.clientBoundsPoint, vm.clientCenterPoint]
      if (vm._uid === this._uid) {
        //获取中心点 具体以哪个点为基准对齐
        let clientCenter = clientCenterPoint(domGet('#contentblock'))
        for (let p of checkpoints) {
          let x = clientCenter.x - p.x
          if (Math.abs(x) <= alignRange) {
            let alignLeft = vmLeft + x
            if (!vm.alignLeft) {
              vm.alignLeft = alignLeft
            } else {
              vm.alignLeft =
                Math.abs(x) < Math.abs(vm.alignLeft - vmLeft)
                  ? alignLeft
                  : vm.alignLeft
            }
            if (Math.abs(x) < 1.0) {
              this.alignLineAdd(
                `width:1px;left:${clientCenter.x - 1}px;top:0;bottom:0;`,
                false
              )
            }
          }
          let y = clientCenter.y - p.y
          if (Math.abs(y) <= alignRange) {
            let alignTop = vmTop + y
            if (!vm.alignTop) {
              vm.alignTop = alignTop
            } else {
              vm.alignTop =
                Math.abs(y) < Math.abs(vm.alignTop - vmTop)
                  ? alignTop
                  : vm.alignTop
            }
            if (Math.abs(y) < 1.0) {
              this.alignLineAdd(
                `height:1px;left:0;right:0;top:${clientCenter.y - 1}px;`,
                false
              )
            }
          }
        }
      } else if (
        !DragDropDataCenter.select[this._uid] &&
        !vm.$el.contains(this.$el) &&
        children(vm.$el.parentNode).includes(this.$el)
      ) {
        let points = [...this.clientBoundsPoint, this.clientCenterPoint]
        let cx = vm.clientCenterPoint.x - this.clientCenterPoint.x
        let cy = vm.clientCenterPoint.y - this.clientCenterPoint.y
        if (Math.abs(cx) <= alignRange) {
          let alignLeft = vmLeft + cx
          if (!vm.alignLeft) {
            vm.alignLeft = alignLeft
          } else {
            vm.alignLeft =
              Math.abs(cx) < Math.abs(vm.alignLeft - vmLeft)
                ? alignLeft
                : vm.alignLeft
          }
          if (Math.abs(cx) < 1.0) {
            let ally = [...points, ...checkpoints].map((point) => {
              return point.y
            })
            let top = Math.min(...ally)
            let height = Math.max(...ally) - top
            this.alignLineAdd(
              `width:1px;height:${height}px;left:${
                vm.clientCenterPoint.x - 1
              }px;top:${top}px;`,
              'center-x'
            )
          }
        }
        if (Math.abs(cy) <= alignRange) {
          let alignTop = vmTop + cy
          if (!vm.alignTop) {
            vm.alignTop = alignTop
          } else {
            vm.alignTop =
              Math.abs(cy) < Math.abs(vm.alignTop - vmTop)
                ? alignTop
                : vm.alignTop
          }
          if (Math.abs(cy) < 1.0) {
            let allx = [...points, ...checkpoints].map((point) => {
              return point.x
            })
            let left = Math.min(...allx)
            let width = Math.max(...allx) - left
            this.alignLineAdd(
              `height:1px;width:${width}px;left:${left}px;top:${
                vm.clientCenterPoint.y - 1
              }px;`,
              'center-y'
            )
          }
        }
        let lines = { x: {}, y: {} }
        for (let cp of checkpoints) {
          for (let p of points) {
            let x = p.x - cp.x
            if (Math.abs(x) <= alignRange) {
              let alignLeft = vmLeft + x
              if (!vm.alignLeft) {
                vm.alignLeft = alignLeft
              } else {
                vm.alignLeft =
                  Math.abs(x) < Math.abs(vm.alignLeft - vmLeft)
                    ? alignLeft
                    : vm.alignLeft
              }
              if (Math.abs(x) < 1.0) {
                let xk = cp.x - 1
                if (!lines.x[xk]) {
                  lines.x[xk] = { y: [] }
                }
                lines.x[xk].y.push(p.y, cp.y)
              }
            }
            let y = p.y - cp.y
            if (Math.abs(y) <= alignRange) {
              let alignTop = vmTop + y
              if (!vm.alignTop) {
                vm.alignTop = alignTop
              } else {
                vm.alignTop =
                  Math.abs(y) < Math.abs(vm.alignTop - vmTop)
                    ? alignTop
                    : vm.alignTop
              }
              if (Math.abs(y) < 1.0) {
                let yk = cp.y - 1
                if (!lines.y[yk]) {
                  lines.y[yk] = { x: [] }
                }
                lines.y[yk].x.push(p.x, cp.x)
              }
            }
          }
        }

        for (let top in lines.y) {
          let xs = lines.y[top].x
          let left = Math.min(...xs)
          let width = Math.max(...xs) - left
          this.alignLineAdd(
            `height:1px;width:${width}px;left:${left}px;top:${top}px;`
          )
        }
        for (let left in lines.x) {
          let ys = lines.x[left].y
          let top = Math.min(...ys)
          let height = Math.max(...ys) - top
          this.alignLineAdd(
            `width:1px;height:${height}px;left:${left}px;top:${top}px;`
          )
        }
      }
    },
    alignInit() {
      this.alignLineRemove()
      this.alignLeft = null
      this.alignTop = null
      EventBus.$emit('alignCheck', this)
    },
    alignLineAdd(css, active = true) {
      let centerDom = document.createElement('div')
      centerDom.className = 'dragdrop-align-line'
      centerDom.style.cssText = css
      document.body.appendChild(centerDom)
      if (active) {
        addClass(this.$el, 'dragdrop-align-active')
      }
    },
    alignLineRemove() {
      let lines = document.querySelectorAll('.dragdrop-align-line')
      for (let line of lines) {
        line.remove()
      }
    },
    dropCheck() {
      if (this.dropNode) {
        removeClass(this.dropNode, 'dropHightLight')
      }
      this.dropNode = null
      if (this.parent && hasClass(this.$el.parentNode, 'dropable')) {
        return
      }
      let nnn = this.isOverlapped(this.$el)
      let n0 = nnn[0]
      let n1 = nnn[1]
      let n2 = nnn[2]
      let n3 = nnn[3]
      let nn = n0.filter((item, index) => {
        return (
          n0.indexOf(item) === index &&
          n1.includes(item) &&
          n2.includes(item) &&
          n3.includes(item)
        )
      })
      if (nn && nn.length > 0) {
        let plant = nn[0]
        if (
          plant !== this.$el.parentNode &&
          !hasClass(plant, 'dropable-stop')
        ) {
          addClass(plant, 'dropHightLight')
          this.dropNode = plant
        }
      }
    },
    mutlElementDown() {
      if (this.enabled) {
        this.getElmPosition()
        this.resetBoundsAndMouseState()
        this.reCheckParent()
        this.mouseClickPosition.left = this.left
        this.mouseClickPosition.right = this.right
        this.mouseClickPosition.top = this.top
        this.mouseClickPosition.bottom = this.bottom
        if (this.parent) {
          this.bounds = this.calcDragLimits()
        }
      }
    },
    handleDrag(deltaX, deltaY) {
      if (!this.lock && DragDropDataCenter.select[this._uid]) {
        const mouseClickPosition = this.mouseClickPosition
        const bounds = this.bounds
        let left = restrictToBounds(
          mouseClickPosition.left - deltaX,
          bounds.minLeft,
          bounds.maxLeft
        )
        let top = restrictToBounds(
          mouseClickPosition.top - deltaY,
          bounds.minTop,
          bounds.maxTop
        )
        let single = Object.keys(DragDropDataCenter.select).length === 1
        if (
          single &&
          this.align &&
          this.alignLeft &&
          Math.abs(left - this.alignLeft) < this.alignRange
        ) {
          left = this.alignLeft
        }
        if (
          single &&
          this.align &&
          this.alignTop &&
          Math.abs(top - this.alignTop) < this.alignRange
        ) {
          top = this.alignTop
        }
        const right = restrictToBounds(
          mouseClickPosition.right + deltaX,
          bounds.minRight,
          bounds.maxRight
        )
        const bottom = restrictToBounds(
          mouseClickPosition.bottom + deltaY,
          bounds.minBottom,
          bounds.maxBottom
        )
        this.showStyle = true
        this.left = left
        this.top = top
        this.right = right
        this.bottom = bottom
        if (DragDropDataCenter.active === this._uid) {
          this.$nextTick((_) => {
            if (this.dropable || this.align) {
              this.getBoundsPoint()
            }
            // 放置检测
            if (this.dropable) {
              this.dropCheck()
            }
            // 对齐检测 显示对齐线&停靠
            if (this.align) {
              this.alignInit()
            }
          })
        }
        this.moved = true
        EventBus.$emit('DragDrag', this, this.left, this.top)
      }
    },
    // 多选时 取消选择
    deSelectElement(vm, sigleDrag) {
      if (this.enabled && vm._uid !== this._uid) {
        if (sigleDrag || !this.mutleDragAble || !vm.mutleDragAble) {
          this.resizing = false
          this.enabled = false
          this.dragging = false
          this.rotating = false
          this.$emit('deactivated')
          this.$emit('update:active', false)
          this.resetBoundsAndMouseState()
          this.getBoundsPoint()
          DragDropDataCenter.del(this._uid)
          EventBus.$emit('DragDeSelect', this)
          this.showStyle = false
        }
      }
    },
    reSetEvent() {
      if (!this.lock && (this.draggable || this.rotatable || this.resizable)) {
        addEvent(document.documentElement, eventsFor.move, this.move)
      }
      addEvent(document.documentElement, eventsFor.stop, this.handleUp)
      removeEvent(document.documentElement, 'mousedown', this.deselect)
      removeEvent(
        document.documentElement,
        'touchend touchcancel',
        this.deselect
      )
      addEvent(document.documentElement, 'mousedown', this.deselect)
      addEvent(document.documentElement, 'touchend touchcancel', this.deselect)
    },
    // 重置
    resetBoundsAndMouseState() {
      this.mouseClickPosition = {
        mouseX: 0,
        mouseY: 0,
        x: 0,
        y: 0,
        w: 0,
        h: 0,
      }

      this.bounds = {
        minLeft: null,
        maxLeft: null,
        minRight: null,
        maxRight: null,
        minTop: null,
        maxTop: null,
        minBottom: null,
        maxBottom: null,
      }
    },
    reCheckParent() {
      const [parentWidth, parentHeight] = this.getParentSize()
      this.parentWidth = parentWidth
      this.parentHeight = parentHeight
      this.right = this.parentWidth - this.width - this.left
      this.bottom = this.parentHeight - this.height - this.top
    },
    getParentSize() {
      if (this.parent) {
        let parentNode = this.$el.parentNode
        let size = domExactSize(parentNode)
        const width = size.width
        const height = size.height
        return [width, height]
      }
      return [null, null]
    },
    elementTouchDown(e) {
      eventsFor = events.touch
      this.elementDown(e)
    },
    elementMouseDown(e) {
      eventsFor = events.mouse
      this.elementDown(e)
    },
    elementDoubleClick(e) {
      if (parents(e.target, '.dragdrop-static').length > 0) {
        return
      }
      e.stopPropagation && e.stopPropagation()
      e.preventDefault && e.preventDefault()
      EventBus.$emit('DragDoubleClick', this, e)
    },
    elementDown(e, t) {
      if (
        this.dragDropDisable ||
        (e instanceof MouseEvent && e.isTrusted && e.button !== 0)
      ) {
        return
      }
      if (parents(e.target, '.dragdrop-static').length > 0) {
        return
      }
      if (this.dragDropDisable) {
        addEvent(document.documentElement, eventsFor.stop, this.handleUp)
        EventBus.$emit('DragStart', this, e)
        return
      } else {
        this.showStyle = this.draggable || this.rotatable || this.resizable
      }
      const target = t || e.target || e.srcElement
      if (this.$el.contains(target)) {
        if (
          (this.dragHandle &&
            !matchesSelectorToParentElements(
              target,
              this.dragHandle,
              this.$el
            )) ||
          (this.dragCancel &&
            matchesSelectorToParentElements(target, this.dragCancel, this.$el))
        ) {
          this.dragging = false
          return
        }
        EventBus.$emit('contextMenuHide', this)
        if (!this.enabled) {
          this.enabled = true
          this.$emit('activated')
          this.$emit('update:active', true)
          if (!e.metaKey && !e.ctrlKey) {
            DragDropDataCenter.clean()
          }
          let sigleDrag = !e.metaKey && !e.ctrlKey
          EventBus.$emit('deSelectElement', this, sigleDrag)
          DragDropDataCenter.add(this._uid, this, true)
        } else {
          if (e.metaKey || e.ctrlKey) {
            DragDropDataCenter.del(this._uid)
            this.resizing = false
            this.enabled = false
            this.dragging = false
            this.$emit('deactivated')
            this.$emit('update:active', false)
            return
          } else {
            DragDropDataCenter.add(this._uid, this, true)
          }
        }
        // 容器内的子项目 阻止传递 否则会和拖动排序有冲突
        if (this.draggable) {
          e.stopPropagation && e.stopPropagation()
          e.preventDefault && e.preventDefault()
        }
        EventBus.$emit('mutlElementDown', this)
        if (this.draggable) {
          this.dragging = true
        }
        this.mouseClickPosition.mouseX = e.touches
          ? e.touches[0].pageX
          : e.pageX
        this.mouseClickPosition.mouseY = e.touches
          ? e.touches[0].pageY
          : e.pageY
        this.reSetEvent()
        EventBus.$emit('DragStart', this, e)
        if (this.align) {
          this.getBoundsPoint()
          this.alignInit()
        }
        if (this.dropable) {
          this.getDropInAndNot()
        }
      } else {
        console.log(this.$el, 'not contains', target)
      }
    },
    getDropInAndNot() {
      this.dropExclude = []
    },
    getBoundsPoint() {
      this.clientBoundsPoint = clientBoundsPoints(this.$el)
      this.clientCenterPoint = clientCenterPoint(this.$el)
    },
    calcDragLimits() {
      let rect = rectBoundsToRect(this.$el, this.$el.parentNode)
      let width = rect.width
      let height = rect.height
      let left = Math.max((width - this.width) * 0.5, 0)
      let top = Math.max((height - this.height) * 0.5, 0)
      left = left < 0.0000001 ? 0 : left
      top = top < 0.0000001 ? 0 : top
      let bounds = {}
      bounds.minLeft = Math.ceil(left / this.grid[0]) * this.grid[0]
      bounds.minRight = bounds.minLeft
      bounds.maxLeft =
        Math.floor((this.parentWidth - this.width - left) / this.grid[0]) *
        this.grid[0]
      bounds.maxRight = bounds.maxLeft
      bounds.minTop = Math.ceil(top / this.grid[1]) * this.grid[1]
      bounds.minBottom = bounds.minTop
      bounds.maxTop =
        Math.floor((this.parentHeight - this.height - top) / this.grid[1]) *
        this.grid[1]
      bounds.maxBottom = bounds.maxTop
      return bounds
    },
    calcResizeLimits() {
      let minW = this.minW
      let minH = this.minH
      let maxW = this.maxW
      let maxH = this.maxH
      const aspectFactor = this.aspectFactor
      const [gridX, gridY] = this.grid
      if (this.lockAspectRatio) {
        // 最小宽度 / 最小高度 大于宽高比 以最小宽度为准 重新计算最小高度
        if (minW / minH > aspectFactor) {
          minH = minW / aspectFactor
        } else {
          // 以最小高度为准 重新计算最小宽度
          minW = aspectFactor * minH
        }
        if (maxW && maxH) {
          maxW = Math.min(maxW, aspectFactor * maxH)
          maxH = Math.min(maxH, maxW / aspectFactor)
        } else if (maxW) {
          maxH = maxW / aspectFactor
        } else if (maxH) {
          maxW = aspectFactor * maxH
        }
        if (this.parent) {
          let maxPW = this.parentWidth
          let maxPH = this.parentHeight
          // 父元素 宽度 / 高度 > 宽高比 以父元素高度为准 计算宽度
          if (maxPW / maxPH > aspectFactor) {
            maxPW = aspectFactor * maxPH
          } else {
            // 以宽度为准 计算高度
            maxPH = maxPW / aspectFactor
          }
          minW = Math.min(minW, maxPW)
          maxW = Math.min(maxW, maxPW)
          minH = Math.min(minH, maxPH)
          maxH = Math.min(maxH, maxPH)
        }
      }
      minW = Math.ceil(minW / gridX) * gridX
      minH = Math.ceil(minH / gridY) * gridY
      maxW = Math.floor(maxW / gridX) * gridX
      maxH = Math.floor(maxH / gridY) * gridY
      let bounds = {
        minW: minW,
        minH: minH,
        maxW: maxW,
        maxH: maxH,
      }
      return bounds
    },
    checkIsDragDrop(target) {
      let is = false
      is = target.isDragDrop === true
      if (is) {
        return true
      }
      if (target.parentNode) {
        is = is || this.checkIsDragDrop(target.parentNode)
      }
      return is
    },
    deselect(e) {
      if (this.isToolWegt) {
        EventBus.$emit('DragClean', this)
        return
      }
      this.alignLineRemove()
      if (
        hasClass(e.target, 'text-editor-vdom') ||
        parents(e.target, '.text-editor-vdom').length > 0
      ) {
        return
      }
      const target = e.target || e.srcElement
      if (!this.checkIsDragDrop(target)) {
        if (
          hasClass(target, 'dropable') ||
          hasClass(target, this.className) ||
          hasClass(target, 'dragdeselect')
        ) {
          if (this.enabled && !this.preventDeactivation) {
            this.enabled = false
            this.$emit('deactivated')
            this.$emit('update:active', false)
          }
          removeEvent(document.documentElement, eventsFor.move, this.move)
          DragDropDataCenter.del(this._uid)
          EventBus.$emit('deSelectElement', this, true)
          EventBus.$emit('contextMenuHide', this)
          this.resetBoundsAndMouseState()
          EventBus.$emit('DragDeSelect', this)
        }
        return
      }
      if (!this.enabled) {
        this.resetBoundsAndMouseState()
      }
      removeEvent(document.documentElement, 'mousedown', this.deselect)
      removeEvent(
        document.documentElement,
        'touchend touchcancel',
        this.deselect
      )
    },
    handleTouchDown(handle, e) {
      eventsFor = events.touch
      this.handleDown(handle, e)
    },
    rotateTouchDown(e) {
      eventsFor = events.touch
      this.rotateDown(e)
    },
    rotateDown(e) {
      if (e instanceof MouseEvent && e.which !== 1) {
        return
      }
      this.showStyle = this.draggable || this.rotatable || this.resizable
      e.stopPropagation && e.stopPropagation()
      e.preventDefault && e.preventDefault()

      this.rotating = true
      this.reCheckParent()
      this.resetBoundsAndMouseState()
      this.mouseClickPosition.mouseX = e.touches ? e.touches[0].pageX : e.pageX
      this.mouseClickPosition.mouseY = e.touches ? e.touches[0].pageY : e.pageY
      this.mouseClickPosition.left = this.left
      this.mouseClickPosition.right = this.right
      this.mouseClickPosition.top = this.top
      this.mouseClickPosition.bottom = this.bottom
      if (this.align) {
        this.getBoundsPoint()
        this.alignInit()
      }
      this.initRotate()
      this.initParentBoundsPoints()
      this.reSetEvent()
      DragDropDataCenter.clean()
      DragDropDataCenter.add(this._uid, this, true)
      EventBus.$emit('deSelectElement', this, true)
    },
    initParentBoundsPoints() {
      let parentNode = this.$el.parentNode
      let size = domExactSize(parentNode)
      let pw = size.width
      let ph = size.height
      this.parentBoundsPoints = [
        {
          x: 0,
          y: 0,
        },
        {
          x: pw,
          y: 0,
        },
        {
          x: pw,
          y: ph,
        },
        {
          x: 0,
          y: ph,
        },
      ]
    },
    initRotate() {
      let scrollLeft = document.documentElement.scrollLeft
      let scrollTop = document.documentElement.scrollTop
      let bounds = this.$el.getBoundingClientRect()
      let width = bounds.width
      let height = bounds.height
      let left = bounds.left + scrollLeft
      let top = bounds.top + scrollTop
      let mouseY = this.mouseClickPosition.mouseY
      let mouseX = this.mouseClickPosition.mouseX

      let cx = left + width * 0.5 // 中心点
      let cy = top + height * 0.5
      let tan = height / width // 正弦
      let hd = Math.atan(tan) // 弧度
      let jd = (180 / Math.PI) * hd // 角度
      this.rotateInit = {}
      this.rotateInit.initjd = jd // 初始角度
      this.rotateInit.cx = cx // 中心点
      this.rotateInit.cy = cy

      let a = Math.abs(mouseY - this.rotateInit.cy)
      let b = Math.abs(mouseX - this.rotateInit.cx)
      tan = a / b // 正切
      hd = Math.atan(tan) // 弧度
      jd = (180 / Math.PI) * hd // 角度

      mouseX < this.rotateInit.cx &&
        mouseY <= this.rotateInit.cy &&
        (jd -= this.rotateInit.initjd)
      mouseX >= this.rotateInit.cx &&
        mouseY < this.rotateInit.cy &&
        (jd = 180 - jd - this.rotateInit.initjd)
      mouseX >= this.rotateInit.cx &&
        mouseY >= this.rotateInit.cy &&
        (jd = 180 + jd - this.rotateInit.initjd)
      mouseX < this.rotateInit.cx &&
        mouseY > this.rotateInit.cy &&
        (jd = -jd - this.rotateInit.initjd)

      this.rotateInit.mousejd = jd
      this.rotateInit.startjd = this.rotate

      this.centerPoint = {
        x: this.left + this.width * 0.5,
        y: this.top + this.height * 0.5,
      }
      this.originalPoints = [
        {
          x: this.left,
          y: this.top,
        },
        {
          x: this.left + this.width,
          y: this.top,
        },
        {
          x: this.left + this.width,
          y: this.top + this.height,
        },
        {
          x: this.left,
          y: this.top + this.height,
        },
      ]
    },
    isOverlapped(element) {
      let document = element.ownerDocument
      let { x, y, width, height } = element.getBoundingClientRect()
      x |= 0
      y |= 0
      width |= 0
      height |= 0
      let filter = (item) => {
        if (typeof item.className !== 'string') {
          return false
        }
        if (
          this.dropExclude &&
          this.dropExclude.length > 0 &&
          this.dropExclude.includes(item)
        ) {
          return false
        }
        let type = this.extendData.type
        if (item.DropAccess && item.DropAccess.length > 0) {
          let can = false
          if (type && item.DropAccess.includes(type)) {
            can = true
          }
          if (!can) {
            this.dropExclude.push(item)
            return false
          }
        }
        if (item.DropNotAccess && item.DropNotAccess.length > 0) {
          let can = true
          if (type && item.DropAccess.includes(type)) {
            can = false
          }
          if (!can) {
            this.dropExclude.push(item)
            return false
          }
        }
        let classNames = item.className.split(' ')
        return (
          (classNames.includes('dropable') ||
            classNames.includes('dropable-stop')) &&
          !element.contains(item)
        )
      }
      let xy = document.elementsFromPoint(x, y)
      return [
        xy.filter(filter),
        document.elementsFromPoint(x + width, y).filter(filter),
        document.elementsFromPoint(x, y + height).filter(filter),
        document.elementsFromPoint(x + width, y + height).filter(filter),
      ]
    },
    move(e) {
      if (e.isTrusted && e.which !== 1) {
        return
      }
      const axis = this.axis
      const grid = this.grid
      const mouseClickPosition = this.mouseClickPosition
      const tmpDeltaX =
        axis && axis !== 'y'
          ? mouseClickPosition.mouseX -
            (e.touches ? e.touches[0].pageX : e.pageX)
          : 0
      const tmpDeltaY =
        axis && axis !== 'x'
          ? mouseClickPosition.mouseY -
            (e.touches ? e.touches[0].pageY : e.pageY)
          : 0
      const [deltaX, deltaY] = snapToGrid(
        grid,
        tmpDeltaX,
        tmpDeltaY,
        this.scale
      )
      if (deltaX === 0 && deltaY === 0) {
        return
      }
      this.moved = true
      if (this.resizing) {
        this.handleResize(deltaX, deltaY)
      } else if (this.dragging) {
        EventBus.$emit('handleDrag', deltaX, deltaY)
      } else if (this.rotating) {
        this.handleRotate(
          e.touches ? e.touches[0].pageX : e.pageX,
          e.touches ? e.touches[0].pageY : e.pageY
        )
      }
    },
    moveHorizontally(val) {
      const [deltaX, _] = snapToGrid(this.grid, val, this.top, this.scale)

      const left = restrictToBounds(
        deltaX,
        this.bounds.minLeft,
        this.bounds.maxLeft
      )

      this.left = left
      this.right = this.parentWidth - this.width - left
    },
    moveVertically(val) {
      const [_, deltaY] = snapToGrid(this.grid, this.left, val, this.scale)

      const top = restrictToBounds(
        deltaY,
        this.bounds.minTop,
        this.bounds.maxTop
      )

      this.top = top
      this.bottom = this.parentHeight - this.height - top
    },
    handleDown(handle, e) {
      if (e instanceof MouseEvent && e.which !== 1) {
        return
      }
      this.showStyle = this.draggable || this.rotatable || this.resizable
      e.stopPropagation && e.stopPropagation()
      e.preventDefault && e.preventDefault()

      this.handle = handle
      this.resizing = true
      this.getElmPosition()
      this.reCheckParent()
      this.resetBoundsAndMouseState()
      this.mouseClickPosition.mouseX = e.touches ? e.touches[0].pageX : e.pageX
      this.mouseClickPosition.mouseY = e.touches ? e.touches[0].pageY : e.pageY
      this.mouseClickPosition.left = this.left
      this.mouseClickPosition.right = this.right
      this.mouseClickPosition.top = this.top
      this.mouseClickPosition.bottom = this.bottom

      this.bounds = this.calcResizeLimits()
      let center = {
        x: this.left + this.width * 0.5,
        y: this.top + this.height * 0.5,
      }
      let point = {}
      switch (this.handle) {
        case 'ml':
        case 'tm':
        case 'tl':
          point = { x: this.left + this.width, y: this.top + this.height }
          break
        case 'tr':
          point = { x: this.left, y: this.top + this.height }
          break
        case 'bl':
          point = { x: this.left + this.width, y: this.top }
          break
        case 'bm':
        case 'mr':
        case 'br':
          point = { x: this.left, y: this.top }
          break
      }
      this.lockedPoint = rotatePoint(center, point, this.rotate)
      this.initParentBoundsPoints()
      if (this.align) {
        this.getBoundsPoint()
        this.alignInit()
      }
      this.reSetEvent()
      DragDropDataCenter.clean()
      DragDropDataCenter.add(this._uid, this, true)
      EventBus.$emit('deSelectElement', this, true)
    },
    handleResize(deltaX, deltaY) {
      let left = this.left
      let top = this.top
      let right = this.right
      let bottom = this.bottom

      const mouseClickPosition = this.mouseClickPosition
      const aspectFactor = this.aspectFactor

      if (!this.widthTouched && deltaX) {
        this.widthTouched = true
      }

      if (!this.heightTouched && deltaY) {
        this.heightTouched = true
      }

      if (this.handle.includes('b')) {
        bottom = mouseClickPosition.bottom + deltaY
      } else if (this.handle.includes('t')) {
        top = mouseClickPosition.top - deltaY
      }
      if (this.handle.includes('r')) {
        right = mouseClickPosition.right + deltaX
      } else if (this.handle.includes('l')) {
        left = mouseClickPosition.left - deltaX
      }

      if (this.lockAspectRatio) {
        if (this.handle === 'tl') {
          let nt = this.top - (this.left - left) / aspectFactor
          let nl = this.left - (this.top - top) * aspectFactor
          if (Math.abs(this.top - nt) < Math.abs(this.left - nl)) {
            top = nt
          } else {
            left = nl
          }
        } else if (this.handle === 'tr') {
          let nt = this.top - (this.right - right) / aspectFactor
          let nr = this.right - (this.top - top) * aspectFactor
          if (Math.abs(this.top - nt) < Math.abs(this.right - nr)) {
            top = nt
          } else {
            right = nr
          }
        } else if (this.handle === 'br') {
          let nb = this.bottom - (this.right - right) / aspectFactor
          let nr = this.right - (this.bottom - bottom) * aspectFactor
          if (Math.abs(this.bottom - nb) < Math.abs(this.right - nr)) {
            bottom = nb
          } else {
            right = nr
          }
        } else if (this.handle === 'bl') {
          let nb = this.bottom - (this.left - left) / aspectFactor
          let nl = this.left - (this.bottom - bottom) * aspectFactor
          if (Math.abs(this.bottom - nb) < Math.abs(this.left - nl)) {
            bottom = nb
          } else {
            left = nl
          }
        } else if (this.handle === 'bm') {
          right = this.right - (this.bottom - bottom) * aspectFactor
        } else if (this.handle === 'tm') {
          left = this.left - (this.top - top) * aspectFactor
        } else if (this.handle === 'mr') {
          bottom = this.bottom - (this.right - right) / aspectFactor
        } else if (this.handle === 'ml') {
          top = this.top - (this.left - left) / aspectFactor
        }
      }

      const width = computeWidth(this.parentWidth, left, right)
      const height = computeHeight(this.parentHeight, top, bottom)

      if (!this.checkResizeWidthHeight(width, height)) {
        return
      }

      let center = {
        x: left + width * 0.5,
        y: top + height * 0.5,
      }
      let point = {}
      let pointRotated = {}
      let movex, movey
      switch (this.handle) {
        case 'ml':
        case 'tm':
        case 'tl':
          point = { x: left + width, y: top + height }
          break
        case 'tr':
          point = { x: left, y: top + height }
          break
        case 'bl':
          point = { x: left + width, y: top }
          break
        case 'bm':
        case 'mr':
        case 'br':
          point = { x: left, y: top }
          break
      }
      pointRotated = rotatePoint(center, point, this.rotate)
      movex = pointRotated.x - this.lockedPoint.x
      movey = pointRotated.y - this.lockedPoint.y
      left -= movex
      top -= movey
      right += movex
      bottom += movey

      let movedBoundsPoints = []
      let movedCenter = {
        x: left + width * 0.5,
        y: top + height * 0.5,
      }
      point = { x: left, y: top }
      pointRotated = rotatePoint(movedCenter, point, this.rotate)
      movedBoundsPoints.push(pointRotated)
      point = { x: left + width, y: top }
      pointRotated = rotatePoint(movedCenter, point, this.rotate)
      movedBoundsPoints.push(pointRotated)
      point = { x: left + width, y: top + height }
      pointRotated = rotatePoint(movedCenter, point, this.rotate)
      movedBoundsPoints.push(pointRotated)
      point = { x: left, y: top + height }
      pointRotated = rotatePoint(movedCenter, point, this.rotate)
      movedBoundsPoints.push(pointRotated)

      if (this.parent && !this.checkResizeBounds(movedBoundsPoints)) {
        return
      }
      this.left = left
      this.top = top
      this.right = right
      this.bottom = bottom
      this.width = width
      this.height = height
      if (this.align) {
        this.$nextTick((_) => {
          this.getBoundsPoint()
          this.alignInit()
        })
      }
      EventBus.$emit(
        'DragResize',
        this,
        this.left,
        this.top,
        this.width,
        this.height
      )
    },
    handleRotate(mouseX, mouseY) {
      let a = Math.abs(mouseY - this.rotateInit.cy)
      let b = Math.abs(mouseX - this.rotateInit.cx)
      let tan = a / b // 正切
      let hd = Math.atan(tan) // 弧度
      let jd = (180 / Math.PI) * hd // 角度
      mouseX < this.rotateInit.cx &&
        mouseY <= this.rotateInit.cy &&
        (jd -= this.rotateInit.initjd)
      mouseX >= this.rotateInit.cx &&
        mouseY < this.rotateInit.cy &&
        (jd = 180 - jd - this.rotateInit.initjd)
      mouseX >= this.rotateInit.cx &&
        mouseY >= this.rotateInit.cy &&
        (jd = 180 + jd - this.rotateInit.initjd)
      mouseX < this.rotateInit.cx &&
        mouseY > this.rotateInit.cy &&
        (jd = -jd - this.rotateInit.initjd)
      jd = jd === 360 ? 0 : jd
      let add = Math.abs(jd - this.rotateInit.mousejd)
      let z = this.rotateInit.startjd
      add = jd < this.rotateInit.mousejd ? add * -1 : add
      z += add
      z = z / 360.0 >= 1.0 ? z % 360 : z
      let boundsPoints = []
      for (let p of this.originalPoints) {
        boundsPoints.push(rotatePoint(this.centerPoint, p, z))
      }
      if (this.parent && !this.checkResizeBounds(boundsPoints)) {
        return
      }
      this.rotate = z
      if (this.align) {
        this.$nextTick((_) => {
          this.getBoundsPoint()
          this.alignInit()
        })
      }
      EventBus.$emit('DragRotate', this, this.rotate)
    },
    changeWidth(val) {
      const [newWidth, _] = snapToGrid(this.grid, val, 0, this.scale)

      let right = restrictToBounds(
        this.parentWidth - newWidth - this.left,
        this.bounds.minRight,
        this.bounds.maxRight
      )
      let bottom = this.bottom

      if (this.lockAspectRatio) {
        bottom = this.bottom - (this.right - right) / this.aspectFactor
      }

      const width = computeWidth(this.parentWidth, this.left, right)
      const height = computeHeight(this.parentHeight, this.top, bottom)

      this.right = right
      this.bottom = bottom
      this.width = width
      this.height = height
    },
    changeHeight(val) {
      const [_, newHeight] = snapToGrid(this.grid, 0, val, this.scale)

      let bottom = restrictToBounds(
        this.parentHeight - newHeight - this.top,
        this.bounds.minBottom,
        this.bounds.maxBottom
      )
      let right = this.right

      if (this.lockAspectRatio) {
        right = this.right - (this.bottom - bottom) * this.aspectFactor
      }

      const width = computeWidth(this.parentWidth, this.left, right)
      const height = computeHeight(this.parentHeight, this.top, bottom)

      this.right = right
      this.bottom = bottom
      this.width = width
      this.height = height
    },
    checkParentSize() {
      if (this.parent) {
        const [newParentWidth, newParentHeight] = this.getParentSize()
        this.parentWidth = newParentWidth
        this.parentHeight = newParentHeight
      }
    },
    checkResizeBounds(points) {
      return polygonIsInPolygon(points, this.parentBoundsPoints)
    },
    checkResizeWidthHeight(width, height) {
      let bounds = this.bounds
      if (width < bounds.minW || (bounds.maxW && width > bounds.maxW)) {
        return false
      }
      if (height < bounds.minH || (bounds.maxH && height > bounds.maxH)) {
        return false
      }
      return true
    },
    contextMenu(e) {
      if (hasClass(e.target, this.classNameContextMenu)) {
        if (e.target !== this.$el) {
          return
        }
      } else {
        let parentNodes = parents(e.target, `.${this.classNameContextMenu}`)
        if (parentNodes[0] !== this.$el) {
          return
        }
      }
      if (parents(e.target, '.dragdrop-static').length > 0) {
        return
      }
      e.stopPropagation && e.stopPropagation()
      e.preventDefault && e.preventDefault()
      if (this.isToolWegt) {
        return
      }
      this.showStyle = false

      if (!this.enabled) {
        this.enabled = true
        this.$emit('activated')
        this.$emit('update:active', true)
        if (!e.metaKey && !e.ctrlKey) {
          DragDropDataCenter.clean()
        }
        let sigleDrag = !e.metaKey && !e.ctrlKey
        EventBus.$emit('deSelectElement', this, sigleDrag)
      }
      DragDropDataCenter.add(this._uid, this, true)
      EventBus.$emit('mutlElementDown', this)
      if (this.draggable) {
        this.dragging = true
      }
      removeEvent(document.documentElement, 'mousedown', this.deselect)
      removeEvent(
        document.documentElement,
        'touchend touchcancel',
        this.deselect
      )
      addEvent(document.documentElement, 'mousedown', this.deselect)
      addEvent(document.documentElement, 'touchend touchcancel', this.deselect)
      EventBus.$emit('DragContextMenu', e, this)
    },
    handleUp(e) {
      this.alignLineRemove()
      removeEvent(document.documentElement, eventsFor.move, this.move)
      this.handle = null
      this.getElmPosition()
      if (!this.enabled) {
        this.resetBoundsAndMouseState()
      }
      this.getBoundsPoint()
      let dragging = this.dragging
      let moved = this.moved
      if (this.resizing) {
        this.resizing = false
        if (this.moved) {
          EventBus.$emit(
            'DragResizeEnd',
            this,
            this.left,
            this.top,
            this.width,
            this.height
          )
        }
      }
      if (this.dragging) {
        this.dragging = false
        if (this.moved) {
          EventBus.$emit('DragDragEnd', this, this.left, this.top)
        }
      }
      if (this.rotating) {
        this.rotating = false
        this.$emit('rotatestop', this.rotate)
        if (this.moved) {
          EventBus.$emit('DragRotateEnd', this, this.rotate)
        }
      }
      removeEvent(document.documentElement, 'mouseup', this.handleUp)
      // 多选时判断是否拖动 没拖动过 就是鼠标点击 把其余的选择取消掉
      if (
        !this.moved &&
        !e.metaKey &&
        !e.ctrlKey &&
        !hasClass(e.target, 'nodeTool') &&
        parents(e.target, '.nodeTool').length === 0
      ) {
        EventBus.$emit('deSelectElement', this, true)
      }
      this.moved = false
      if (this.dropNode) {
        removeClass(this.dropNode, 'dropHightLight')
        if (this.$el.parentNode !== this.dropNode) {
          this.moveToDropNode()
          EventBus.$emit('DragAddedToDrop', this, this.dropNode)
          this.dropNode = null
        } else {
          this.dropNode = null
        }
      }
      this.$nextTick(() => {
        for (let uid in DragDropDataCenter.select) {
          let vm = DragDropDataCenter.select[uid]
          EventBus.$emit('DragEnd', vm, moved)
        }
        if (!moved && ((dragging && this.enabled) || this.dragDropDisable)) {
          EventBus.$emit('DragClick', this, e)
        }
        EventBus.$emit('ResetBounds', this)
        removeClass('.dragdrop-align-active', 'dragdrop-align-active')
      })
    },
    moveToDropNode() {
      let { width, height } = domExactSize(this.dropNode)
      for (let id in DragDropDataCenter.select) {
        let vm = DragDropDataCenter.select[id]
        let rect = rectBoundsToRect(vm.$el, this.dropNode)
        let left = rect.left + (rect.width - vm.width) * 0.5
        let top = rect.top + (rect.height - vm.height) * 0.5
        if (vm.parent) {
          if (
            left < 0 ||
            top < 0 ||
            left + vm.width > width ||
            top + vm.height > height
          ) {
            continue
          }
        }
        vm.left = left
        vm.top = top
        vm.rotate = rect.deg
        this.dropNode.appendChild(vm.$el)
      }
    },
    ResetBounds(vm) {
      this.showStyle = false
      if (vm._uid !== this._uid && vm.$el && vm.$el.contains(this.$el)) {
        this.getBoundsPoint()
      }
    },
  },
}
