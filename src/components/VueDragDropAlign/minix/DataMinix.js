export const DataMinix = {
  data: function () {
    return {
      showStyle: false,
      alignRange: 5,
      left: this.x,
      top: this.y,
      right: null,
      bottom: null,
      width: null,
      height: null,
      rotate: this.r,
      widthTouched: false,
      heightTouched: false,

      aspectFactor: null,

      parentWidth: null,
      parentHeight: null,

      minW: this.minWidth,
      minH: this.minHeight,

      maxW: this.maxWidth,
      maxH: this.maxHeight,

      handle: null,
      enabled: this.active,
      resizing: false,
      dragging: false,
      rotating: false,
      zIndex: this.z,
      dropHightLight: false,
    }
  },
}
