const userSelectNone = {
  userSelect: 'none',
  MozUserSelect: 'none',
  WebkitUserSelect: 'none',
  MsUserSelect: 'none',
}

const userSelectAuto = {
  userSelect: 'auto',
  MozUserSelect: 'auto',
  WebkitUserSelect: 'auto',
  MsUserSelect: 'auto',
}

export const ComputedMinix = {
  computed: {
    style() {
      return this.showStyle
        ? {
            transform: `rotate(${this.rotate}deg)`,
            left: `${this.left}px`,
            top: `${this.top}px`,
            width: this.computedWidth,
            height: this.computedHeight,
            zIndex: this.zIndex,
            ...(this.dragging && this.disableUserSelect
              ? userSelectNone
              : userSelectAuto),
          }
        : null
    },
    toolTops() {
      if (this.dragging) {
        return `x:${this.left.toFixed(0)} y:${this.top.toFixed(0)}`
      } else if (this.resizing) {
        return `w:${this.width.toFixed(0)} h:${this.height.toFixed(0)}`
      } else if (this.rotating) {
        return `角度: ${this.rotate.toFixed(2)}`
      }
      return ''
    },
    actualHandles() {
      if (!this.resizable) return []
      return this.handles
    },
    computedWidth() {
      return this.width + 'px'
    },
    computedHeight() {
      return this.height + 'px'
    },
    resizingOnX() {
      return (
        Boolean(this.handle) &&
        (this.handle.includes('l') || this.handle.includes('r'))
      )
    },
    resizingOnY() {
      return (
        Boolean(this.handle) &&
        (this.handle.includes('t') || this.handle.includes('b'))
      )
    },
    isCornerHandle() {
      return (
        Boolean(this.handle) && ['tl', 'tr', 'br', 'bl'].includes(this.handle)
      )
    },
  },
}
