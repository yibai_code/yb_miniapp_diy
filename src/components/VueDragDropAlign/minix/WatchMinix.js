export const WatchMinix = {
  watch: {
    dropAccess: {
      handler() {
        if (this.$el) {
          this.$el.DropAccess = this.dropAccess
        }
      },
      immediate: true,
    },
    dropNotAccess: {
      handler() {
        if (this.$el) {
          this.$el.DropNotAccess = this.dropNotAccess
        }
      },
      immediate: true,
    },
    active: {
      handler(val) {
        this.enabled = val
        if (val) {
          removeEvent(document.documentElement, 'mousedown', this.deselect)
          removeEvent(
            document.documentElement,
            'touchend touchcancel',
            this.deselect
          )
          addEvent(document.documentElement, 'mousedown', this.deselect)
          addEvent(
            document.documentElement,
            'touchend touchcancel',
            this.deselect
          )
          this.$emit('activated')
        } else {
          this.$emit('deactivated')
        }
      },
      immediate: true,
      deep: true,
    },
    z(val) {
      if (val >= 0 || val === 'auto') {
        this.zIndex = val
      }
    },
    x(val) {
      if (this.resizing || this.dragging) {
        return
      }

      if (this.parent) {
        this.bounds = this.calcDragLimits()
      }

      this.moveHorizontally(val)
    },
    y(val) {
      if (this.resizing || this.dragging) {
        return
      }

      if (this.parent) {
        this.bounds = this.calcDragLimits()
      }

      this.moveVertically(val)
    },
    lockAspectRatio(val) {
      if (val) {
        this.aspectFactor = this.width / this.height
      } else {
        this.aspectFactor = undefined
      }
    },
    minWidth(val) {
      if (val > 0 && val <= this.width) {
        this.minW = val
      }
    },
    minHeight(val) {
      if (val > 0 && val <= this.height) {
        this.minH = val
      }
    },
    maxWidth(val) {
      this.maxW = val
    },
    maxHeight(val) {
      this.maxH = val
    },
    w(val) {
      if (this.resizing || this.dragging) {
        return
      }

      if (this.parent) {
        this.bounds = this.calcResizeLimits()
      }

      this.changeWidth(val)
    },
    h(val) {
      if (this.resizing || this.dragging) {
        return
      }

      if (this.parent) {
        this.bounds = this.calcResizeLimits()
      }

      this.changeHeight(val)
    },
  },
}
