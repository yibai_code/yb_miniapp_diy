import Vue from 'vue'
import App from './App'
import store from './store'
import './common/Prototypes'
import 'swiper/swiper-bundle.css'
import '@simonwep/pickr/dist/themes/classic.min.css' // 'classic' theme
import VueDragDropAlign from '@/components/VueDragDropAlign/vue-draggable-resizable'

import YBFromWork from '@yb_fromwork/admin-base'
import { APP_BASEURL, APP_PHPSESSID } from '@/dev.config'

import { allClass, listClass, lists, linkList } from '@/api/linkSet'

Vue.use(YBFromWork, {
  api: {
    album_all: 'album/Album/allAlbum',
    album_class: 'album/Albumclass/classTree',
    album_albumPictures: 'album/Album/albumPictures',
    album_addNetImage: 'album/Album/addNetImage',
    album_imageUpload: 'album/Album/imageUpload',
    // 相册管理相关接口
    album_del: 'album/Album/del',
    album_add: 'album/Album/add',
    album_edit: 'album/Album/edit',
    album_detail: 'album/Album/detail',
    album_images_del: 'album/Album/images_del',
    album_cover: 'album/Album/cover',
    album_images_move: 'album/Album/images_move',

    link_list: linkList,
    link_class_list: allClass,
    link_item: listClass,
    link_detail: lists,
  },
  apiClosure: 0,
  elementUI: {
    size: 'small',
  },
  routerBeforeResolve: null,
  dialogConfig: {
    size: 'dialog-common-size',
  },
  formConfig: {},
  APP_PHPSESSID: APP_PHPSESSID,
  APP_BASEURL: APP_BASEURL,
})

Vue.component('DragDrop', VueDragDropAlign)

Vue.config.productionTip = false
window.RootVM = new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app')
