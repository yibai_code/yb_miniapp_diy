import Vue from 'vue'
import { Wegts } from '@/config/WegtConfig'
export const TypeChangeMixins = {
  methods: {
    change_type(flag, index) {
      if (this.editData.type === flag) {
        if (index === this.editData.type_index) {
          return
        }
      }
      console.log(flag, 'flagflagflag')
      let VueWegt = Wegts[flag]
      if (VueWegt) {
        VueWegt.then((res) => {
          let Wegt = Vue.extend(res.default)
          let tmpl = new Wegt({
            data: {
              id: this.editData.id,
              styleIndex: index,
            },
          })
          tmpl.initStyle(index)
          this.$set(this.editData, 'type', flag)
          if (index !== this.editData.type_index) {
            this.$set(this.editData, 'type_index', index)
          }
        })
      }
    },
  },
}
