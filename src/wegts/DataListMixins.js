import Vue from 'vue'
import { isEqual } from 'lodash'
import { dataIdsList, dataItemInfo, dataItemsList } from '@/api/commonData'
import { children, domGet } from '@/components/VueDragDropAlign/dom'
import Swiper from 'swiper'

export const DataListMixins = {
  computed: {
    dataType() {
      if (this.wegtData.type.indexOf('diy_platform_product') > -1) {
        return 'diy_platform_product'
      } else if (this.wegtData.type.indexOf('article') >= 0) {
        return 'article'
      } else if (this.wegtData.type.indexOf('product') >= 0) {
        return 'product'
      } else if (this.wegtData.type.indexOf('services') >= 0) {
        let type = 'services'
        this.wegtData.type.indexOf('info') >= 0 ? (type = 'services_info') : ''
        return type
      } else if (this.wegtData.type.indexOf('recruit') >= 0) {
        let type = 'recruit'
        this.wegtData.type.indexOf('info') >= 0 ? (type = 'recruit_info') : ''
        return type
      } else if (this.wegtData.type.indexOf('images') >= 0) {
        let type = 'images'
        this.wegtData.type.indexOf('info') >= 0 ? (type = 'image_info') : ''
        return type
      } else if (this.wegtData.type.indexOf('map') >= 0) {
        return 'map'
      } else if (this.wegtData.type === 'member') {
        return 'member'
      } else if (this.wegtData.type.indexOf('vote') >= 0) {
        return 'vote'
      } else if (this.wegtData.type.indexOf('form') >= 0) {
        return 'form'
      } else if (this.wegtData.type.indexOf('shop_car') >= 0) {
        return 'shop_car'
      } else if (this.wegtData.type.indexOf('sales') >= 0) {
        let type = 'sales_list'
        this.wegtData.type.indexOf('list') >= 0
          ? (type = 'sales_list')
          : (type = 'sales_detail')
        return type
      } else if (this.wegtData.type.indexOf('sales1') >= 0) {
        let type = 'sales_list'
        this.wegtData.type.indexOf('list') >= 0
          ? (type = 'sales_list')
          : (type = 'sales_detail')
        return type
      } else if (this.wegtData.type.indexOf('fact_list') >= 0) {
        return 'fact_list'
      } else if (this.wegtData.type.indexOf('grass') >= 0) {
        return 'grass'
      } else if (this.wegtData.type.indexOf('page_agent_index') >= 0) {
        return 'page_agent_index'
      } else if (this.wegtData.type.indexOf('page_shareholder_index') >= 0) {
        return 'page_shareholder_index'
      } else if (this.wegtData.type.indexOf('page_more_shop') >= 0) {
        return 'store'
      } else if (this.wegtData.type.indexOf('page_goods_category') >= 0) {
        return this.wegtData.type
      } else if (this.wegtData.type.indexOf('video_list') >= 0) {
        return 'video_list'
      } else if (this.wegtData.type.indexOf('tencent_live_list') >= 0) {
        return 'tencent_live_list'
      } else if (this.wegtData.type.indexOf('live_list') >= 0) {
        return 'live_list'
      } else if (this.wegtData.type.indexOf('building_list') >= 0) {
        return 'building_list'
      } else if (this.wegtData.type.indexOf('page_house_type_list') >= 0) {
        return 'building_detail'
      } else if (this.wegtData.type.indexOf('house_type_detail') >= 0) {
        return 'house_type_detail'
      } else if (this.wegtData.type.indexOf('house_type_list') >= 0) {
        return 'building_list'
      } else if (this.wegtData.type.indexOf('decorate_list') >= 0) {
        return 'decorate_list'
      } else if (this.wegtData.type.indexOf('designer_list') >= 0) {
        return 'designer_list'
      } else if (this.wegtData.type.indexOf('building_detail') >= 0) {
        return 'building_detail'
      } else if (this.wegtData.type.indexOf('decorate_detail') >= 0) {
        return 'decorate_detail'
      } else if (this.wegtData.type.indexOf('designer_detail') >= 0) {
        return 'designer_detail'
      } else if (this.wegtData.type.indexOf('lecturer') >= 0) {
        return 'lecturer'
      } else if (this.wegtData.type.indexOf('audio') >= 0) {
        return 'audio'
      } else if (this.wegtData.type.indexOf('integral') >= 0) {
        return 'integral'
      } else if (this.wegtData.type.indexOf('coupon') >= 0) {
        return 'coupon'
      } else if (this.wegtData.type.indexOf('group') >= 0) {
        return 'group'
      } else if (this.wegtData.type.indexOf('goods') >= 0) {
        return 'goods'
      } else if (this.wegtData.type.indexOf('bargain') >= 0) {
        return 'bargain'
      } else if (this.wegtData.type.indexOf('seckill') >= 0) {
        return 'seckill'
      } else if (this.wegtData.type.indexOf('course') >= 0) {
        return 'knowledge_content'
      } else if (this.wegtData.type.indexOf('series') >= 0) {
        return 'knowledge_column'
      } else if (this.wegtData.type.indexOf('room_list') >= 0) {
        return 'hotel_room_list'
      } else if (this.wegtData.type.indexOf('hotel_room_detail') >= 0) {
        return 'hotel_room_detail'
      } else if (this.wegtData.type.indexOf('course_detail') >= 0) {
        return 'knowledge_content'
      } else if (this.wegtData.type.indexOf('series_detail') >= 0) {
        return 'knowledge_column'
      } else if (this.wegtData.type.indexOf('diy_wx_advert') >= 0) {
        return 'traffic'
      } else if (this.wegtData.type.indexOf('full_reduction') >= 0) {
        return 'full_reduction'
      } else if (this.wegtData.type.indexOf('shop_index') >= 0) {
        return 'shop_index'
      } else if (this.wegtData.type.indexOf('shop_list') >= 0) {
        return 'shop_list'
      } else if (this.wegtData.type.indexOf('album') >= 0) {
        return 'images_list'
      }
      return ''
    },
  },
  watch: {
    'wegtData.list': {
      handler(val) {
        if (!val) {
          return
        }
        if (this.wegtData.data_type !== 'auto') {
          let ids = this.wegtData.list.map((item) => {
            return item.id
          })
          if (!isEqual(ids, this.wegtData.ids)) {
            this.wegtData.ids = ids
          }
        }
        // 数据是否swiper
        if (this.needSwiper) {
          let id = this.wegtData.id
          let el = domGet(`#${id}`)
          console.log('el: ', el)
          // 从dom节点获取swiper对象
          let swiper = el.swiper
          console.log('swiper: ', swiper)
          this.$nextTick(function () {
            if (swiper) {
              swiper.update()
            } else {
              let swiper = children(el, '.swiper-container')[0]
              let mySwiper = new Swiper(swiper, {
                freeMode: true,
                setWrapperSize: true,
                slidesPerView: 'auto',
                spaceBetween: 11,
                slidesOffsetBefore: 11,
                slidesOffsetAfter: 11,
                touchStartPreventDefault: false,
              })
              el.swiper = mySwiper
            }
          })
        }
      },
      immediate: false,
      deep: true,
    },
    'wegtData.data_type': {
      handler() {
        if (this.wegtData.data_type === 'auto') this.getlist()
      },
      immediate: true,
      deep: true,
    },
    'wegtData.order': {
      handler() {
        if (this.wegtData.data_type === 'auto') this.getlist()
      },
      immediate: false,
      deep: true,
    },
    'wegtData.sort': {
      handler() {
        if (this.wegtData.data_type === 'auto') this.getlist()
      },
      immediate: false,
      deep: true,
    },
    'wegtData.class_ids': {
      handler() {
        if (this.wegtData.data_type === 'auto') this.getlist()
      },
      immediate: false,
      deep: true,
    },
    'wegtData.ids': {
      handler() {
        let ids = this.wegtData.list.map((item) => {
          return item.id
        })
        if (!isEqual(ids, this.wegtData.ids)) {
          this.getIdsList()
        }
      },
      immediate: false,
    },
    'wegtData.num': {
      handler() {
        if (this.wegtData.data_type === 'auto') this.getlist()
      },
      immediate: false,
      deep: true,
    },
    'wegtData.id': {
      handler(val) {
        if (val && this.wegtData.info) {
          this.getinfo()
        }
      },
      immediate: true,
    },
  },
  methods: {
    getlist() {
      let data = {
        type: this.dataType,
        num: this.wegtData.num,
        class_ids: this.wegtData.class_ids,
        sort: this.wegtData.sort,
        order: this.wegtData.order,
      }
      if (data.class_ids !== '') {
        dataItemsList(data).then((res) => {
          if (res.info && this.wegtData.data_type === 'auto') {
            this.wegtData.list.splice(0)
            let list = res.info.list
            if (list !== null) {
              this.wegtData.list.push(...list)
            }
            if (this.wegtData.bannerList) {
              let banner = res.info.banner_list
              this.wegtData.bannerList.splice(0)
              this.wegtData.bannerList.push(...banner)
            }
          }
        })
      }
    },
    getIdsList() {
      let data = {
        type: this.dataType,
        ids: this.wegtData.ids.join(),
      }
      dataIdsList(data).then((res) => {
        if (res.info) {
          this.wegtData.list.splice(0)
          let list = res.info.list
          this.wegtData.list.push(...list)
        }
      })
    },
    getinfo() {
      let data = {
        type: this.dataType,
        id: this.wegtData.id,
      }
      dataItemInfo(data).then((res) => {
        let info = res.info
        if (info) {
          for (let k in info) {
            Vue.set(this.wegtData.info, k, info[k])
          }
        }
      })
    },
  },
}
