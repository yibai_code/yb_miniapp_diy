import { StyleCenter } from '@/global'
import store from '@/store'
import { extend } from 'lodash'

/** 组件mixins
 * @author 徐鹏飞
 * @type {{methods: {init(*=): void}, props: {wegtData: {default(): {}, type: ObjectConstructor}, index: {default: number, type: NumberConstructor}}}}
 */
export const WegtMixins = {
  props: {
    // 组件数据
    wegtData: {
      type: Object,
      default() {
        return {}
      },
    },
    // 组件在页面中的顺序索引
    index: {
      type: Number,
      default: 0,
    },
    dragDropDisable: {
      type: Boolean,
      default: false,
    },
  },
  watch: {
    'wegtData.id'(value) {
      if (value) {
        this.id = value
      }
    },
  },
  mounted() {
    if (this.wegtData.id && !this.id) {
      this.id = this.wegtData.id
    }
  },
  methods: {
    /**
     * 组件初始化方法 组件添加到组件列表里之前 调用初始化方法 设置组件的初始样式和初始数据
     * @param index 组件在组件列表中的索引 [组件A,组件B,当前组件,.....]
     */
    init(index) {
      this.initStyle && this.initStyle()
      this.initStyleExtend && this.initStyleExtend()
      this.initData && this.initData(index)
    },
    /**
     * 组件样式初始化
     */
    initStyle() {
      if (this.css && this.css.length > this.styleIndex) {
        let color = store.getters['diy/globleColor'].color
        console.log('color: ', color)
        let alpha = color.colorAlpha() * 100
        let style = this.css[this.styleIndex]
        // 写入全局样式表
        style = JSON.stringify(style)
        style = style
          .replace(/#ID#/g, this.id)
          .replace(/#GlobleColor#/g, color)
          .replace(/"#GlobleColorAlpha#"/g, alpha)
        console.log('style: ', style)
        style = JSON.parse(style)

        for (let eid in style) {
          if (!StyleCenter.css[eid]) {
            StyleCenter.css[eid] = {}
          }
          extend(StyleCenter.css[eid], style[eid])
          StyleCenter.make(eid)
        }
      }
    },
  },
}
