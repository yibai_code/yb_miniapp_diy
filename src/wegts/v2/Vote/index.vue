<template>
  <DragDrop
    :id="wegtData.id"
    :drag-drop-disable="dragDropDisable"
    :type="wegtData.type"
    :draggable="false"
    :align="false"
    :rotatable="false"
    :resizable="false"
    class="block"
  >
    <div>
      <div
        v-for="(item, index) in wegtData.list"
        :key="index"
        class="votingList"
      >
        <div class="main-tit">
          <span class="onlineVoting">{{ item.title }}</span>
          <div class="result">
            <a url="../votingResult/index">查看结果</a>
            <img src="../../../assets/image/right-arrow.png" />
          </div>
        </div>
        <div class="cont">
          <div class="tit">
            <div v-if="item.status == 1" class="endTime">
              距离投票结束还有 {{ item.times }}
            </div>
            <div v-if="item.status == 0" class="endTime">
              距离投票开始还有 {{ item.times }}
            </div>
            <div v-if="item.status == 2" class="endTime">投票已结束</div>
          </div>
          <div class="item-warp">
            <div
              v-for="(v, index1) in item.option"
              :key="index1"
              class="item zzf-vote-item"
            >
              <div class="img">
                <img :src="v.imgurl" />
                <span :style="'background:' + globleColor.color">
                  {{ v.number }}
                </span>
              </div>
              <div class="option">{{ v.title }}</div>
              <div class="num">{{ v.sum }}票</div>
              <div class="btn" :style="'background:' + globleColor.color">
                投票
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </DragDrop>
</template>

<script>
  import { StyleCenter } from '@/global'
  import store from '@/store'
  import { SettingItems, StyleEdit } from '@/config/WegtStyles'
  import { WegtMixins } from '@/wegts/WegtMixins'
  import { mapGetters } from 'vuex'
  import DataSet from './data'
  import { DataListMixins } from '@/wegts/DataListMixins'

  /** 在线投票组件
   */
  export default {
    components: {},
    mixins: [WegtMixins, DataListMixins],
    data: function () {
      return {
        // 组件id
        id: '',
        // 组件样式索引 一个组件多个样式时使用
        styleIndex: 0,
        // 初始化样式
        css: [],

        // 组件标题
        title: '在线投票',
        // 组件右侧可设置栏目
        settingItem: [
          SettingItems.style,
          SettingItems.data,
          SettingItems.animo,
        ],
        // 组件样式可编辑选项
        styleItem: [StyleEdit.bg],
        // 组件数据设置
        dataSet: DataSet,
      }
    },
    computed: {
      ...mapGetters('diy', {
        globleColor: 'globleColor',
      }),
    },
    watch: {},
    mounted() {},
    methods: {
      /**
       * 初始化样式
       */
      initStyle() {
        StyleCenter.css[this.id] = {
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1)', alpha: 100 },
          },
        }
        StyleCenter.make(this.id)
      },
      /**
       * 初始化数据
       * @param index
       */
      initData(index) {
        let item = {
          id: this.id,
          type: 'vote',
        }
        item['num'] = 5
        item['showtitle'] = true
        item['order'] = 'create_time'
        item['sort'] = 'desc'
        item['class_ids'] = '0'
        item['data_type'] = ''
        item['ids'] = []
        item['list'] = []
        for (let i = 1; i < 2; i++) {
          let t = {
            title: '投票名称',
            times: '0天0时0分0秒',
            status: 1,
            option: [
              {
                title: '投票选项',
                imgurl: require(`@/assets/image/default_c.png`),
                sum: 0,
                number: i,
              },
              {
                title: '投票选项',
                imgurl: require(`@/assets/image/default_c.png`),
                sum: 0,
                number: i + 1,
              },
            ],
            link: {
              type: 'none',
              type_name: '无链接',
              id: '',
              url: '',
              title: '',
            },
          }
          item['list'].push(t)
        }
        store.commit('diy/ALL_DATA_ADD', { item: item, index: index })
      },
    },
  }
</script>

<style lang="scss" scoped>
  div {
    box-sizing: border-box;
  }

  .btn {
    display: inline-block;
    font-weight: 400;
    vertical-align: middle;
    cursor: pointer;
    user-select: none;
    border: 1px solid transparent;
    box-shadow: none;
  }

  .votingList .main-tit {
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    height: 50px;
    padding: 0 15px;
    border-bottom: 1px solid #f0f0f0;
  }

  .votingList .main-tit .onlineVoting {
    font-size: 15px;
    color: #353535;

    /* width:80px; */
    text-align: center;
  }

  .votingList .main-tit .result {
    position: relative;
    display: flex;
    padding-right: 12px;
    font-size: 12px;
    color: #9b9b9b;
  }

  .votingList .main-tit .result img {
    position: absolute;
    top: 0;
    right: 0;
    display: block;
    width: 8px;
    height: 10px;
    margin-top: 3px;
    content: '';
  }

  .votingList .cont {
    width: 100%;
    padding: 15px;
    text-align: center;
  }

  .votingList .cont .name {
    margin-bottom: 5px;
    font-size: 14px;
    color: #333;
  }

  .votingList .cont .endTime {
    padding-left: 5px;
    font-size: 12px;
    color: #666;
  }

  .votingList .cont .endTime::before {
    display: inline-block;
    width: 11px;
    height: 11px;
    margin-right: 5px;
    vertical-align: middle;
    content: '';
    background: url('../../../assets/image/clock.png') no-repeat;
    background-size: cover;
  }

  .votingList .cont .item-warp {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    margin-top: 15px;
  }

  .votingList .cont .item {
    width: 165px;
    height: 220px;
    margin-bottom: 15px;
    text-align: center;
    background-color: #fff;
    border-radius: 4px;
    box-shadow: 0 2px 7px 0 rgba(51, 51, 51, 0.2);
  }

  .votingList .cont .img {
    position: relative;
    width: 100%;
    height: 104px;
  }

  .votingList .cont .img img {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 104px;
  }

  .votingList .cont .img span {
    position: absolute;
    top: 0;
    left: 15px;
    display: block;
    width: 29px;
    height: 25px;
    font-size: 14px;
    line-height: 25px;
    color: #fff;
    text-align: center;
    background: #57bead;
    border-radius: 0 0 3px 3px;
  }

  .votingList .cont .option {
    width: 100%;
    height: 20px;
    margin: 10px 0 5px 0;
    font-size: 14px;
    color: #333;
  }

  .votingList .cont .num {
    height: 16px;
    margin-top: 5px;
    font-size: 12px;
    color: #666;
  }

  .votingList .cont .btn {
    width: 84%;
    height: 35px;
    margin: 15px 10px;
    font-size: 14px;
    line-height: 26px;
    color: #fff;
    text-align: center;
    background: #57bead;
    border-radius: 30px;
  }
</style>
