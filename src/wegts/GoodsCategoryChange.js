import { dataClassesList, dataItemsList } from '@/api/commonData'
import Vue from 'vue'
import store from '@/store'
import { SettingItems } from '@/config/WegtStyles'
import TypeSet from './v2/PageGoodsCategoryLevel1Style3/type'
export const GoodsCategoryChange = {
  filters: {
    formatterPriceYuan(value) {
      return value
        ? (String(value).match(/^\S+\./) && String(value).match(/^\S+\./)[0]) ||
            value
        : '0'
    },
    formatterPriceFen(value) {
      return value
        ? String(value).match(/\./)
          ? String(value).replace(/^\S+\./, '')
          : '.00'
        : '.00'
    },
  },
  methods: {
    doEdit() {
      store.dispatch('diy/setEditIndex', -55)
      store.dispatch('diy/setEditData', this.currentLink)
      store.dispatch('diy/setSettingPanle', {
        id: 'goodsCategory',
        title: '',
        settingItem: [SettingItems.type],
        typeSet: TypeSet,
      })
    },
    changeClass() {
      let type = this.currentLink.type
      dataClassesList({
        type: 'goods_list',
      }).then((res) => {
        let classesInfo = recursion(1, 3, 0, res.info.list) || []
        function recursion(startLevel, stopLevel, id, source) {
          return source
            .filter((item) => item.level === startLevel && item.pid === id)
            .map((item) => {
              return {
                children:
                  startLevel === stopLevel
                    ? []
                    : recursion(startLevel + 1, stopLevel, item.id, source),
                id: item.id,
                sort: item.sort,
                title: item.name,
                image: item.pic || '',
              }
            })
        }
        if (
          type === 'page_goods_category_level1_style1' ||
          type === 'page_goods_category_level1_style2' ||
          type === 'page_goods_category_level1_style3' ||
          type === 'goods_class'
        ) {
          this.testList = JSON.parse(JSON.stringify(classesInfo)).map(
            (item) => {
              item.children = []
              return item
            }
          )
          this.changeGoods(0, this.testList[0])
        } else if (
          type === 'page_goods_category_level2_style1' ||
          type === 'page_goods_category_level2_style2' ||
          type === 'page_goods_category_level2_style3'
        ) {
          let list = JSON.parse(JSON.stringify(classesInfo)).map((item) => {
            item.children.forEach((second) => {
              second.children = []
            })
            return item
          })
          this.testList = JSON.parse(JSON.stringify(list))
          if (type === 'page_goods_category_level2_style2') {
            if (
              this.testList[0] &&
              this.testList[0].children &&
              this.testList[0].children[0]
            ) {
              this.changeGoods(0, this.testList[0].children[0])
            }
          } else if (type === 'page_goods_category_level2_style1') {
            if (this.testList[0].children && this.testList[0].children[0]) {
              this.changeGoods(0, this.testList[0])
            }
          }
        } else {
          this.testList = JSON.parse(JSON.stringify(classesInfo))
        }
      })
    },
    changeGoods(index, item) {
      let type = this.currentLink.type
      if (type === 'page_goods_category_level2_style1') {
        dataItemsList({
          type: 'goods_class_list',
          class_id: item.id,
          style_type: 1,
          page: 1,
        }).then((res) => {
          item.children = res.info.list.map((item) => {
            return {
              title: item.cate_name,
              image: item.pic,
              children: item.goods_list.map((second) => {
                return {
                  image: second.pic,
                  title: second.goods_name,
                  price: second.price,
                }
              }),
            }
          })
        })
      } else {
        dataItemsList({
          type: 'goods_class_list',
          class_id: item.id,
          style_type: 0,
          page: 1,
        }).then((res) => {
          item.children = res.info.list.map((item) => {
            return {
              image: item.pic,
              title: item.goods_name,
              price: item.price,
            }
          })
        })
      }
    },
    goGoodDetail() {},
  },
}
