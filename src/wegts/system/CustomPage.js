import PageBase from '@/wegts/system/PageBase'

export default class CustomPage extends PageBase {
  constructor() {
    super(JSON.parse(JSON.stringify(defaultPageData)))
  }
}

const defaultPageData = {
  id: 0,
  title: '自定义',
  data: {
    all_data: [],
    page: {
      name: '自定义',
      nv_color: '#ffffff',
      bg_color: '#f4f4f4',
      text_color: 'black',
      bg_img: '',
    },
    styles: {
      css: {
        mainblock: {
          background: {
            value: 'rgba(242, 242, 242,1.00)',
            alpha: 100,
          },
        },
        pagenav: {
          background: {
            value: 'rgba(255, 255, 255,1.00)',
            alpha: 100,
          },
        },
      },
    },
  },
}
