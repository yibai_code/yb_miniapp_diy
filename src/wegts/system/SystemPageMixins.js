import SystemGoodClassify from '@/wegts/system/SystemGoodClassify'
import SystemMember from '@/wegts/system/SystemMember'
import { pageAdd, allPage, pageCopy, pageDel, pageTitleChange } from '@/api/app'
import { oldDataFix } from '@/utils/OldDataFix'
import { mapGetters } from 'vuex'
let systemPageMap = [
  {
    title: '个人中心',
    tmpl_type: 1,
    old_type: ['member'],
    Controller: SystemMember,
  },
  {
    title: '商品分类',
    tmpl_type: 2,
    old_type: [
      'page_goods_category_level1_style1',
      'page_goods_category_level1_style2',
      'page_goods_category_level1_style3',
      'page_goods_category_level2_style1',
      'page_goods_category_level2_style2',
      'page_goods_category_level2_style3',
      'page_goods_category_level3_style3',
      'goods_class',
    ],
    Controller: SystemGoodClassify,
  },
]
export default {
  data() {
    return {}
  },
  computed: {
    ...mapGetters('diy', {
      systemPage: 'systemPage',
      globleColor: 'globleColor',
    }),
  },
  /*
   * @desc 初始化系统页面
   * */
  methods: {
    initSystemPage() {
      this.$store.commit('diy/SET_SYSTEM_PAGE', systemPageMap)
      setTimeout(() => {
        let needCreateSystemPage = this.systemPage.filter((item) => {
          let findIndex = this.allPage.findIndex(
            (child) => child.tmpl_type === item.tmpl_type
          )
          return findIndex === -1
        })
        if (needCreateSystemPage.length) {
          // 需要初始化
          this.$alert(
            '点击左侧页面按钮可以进行商品分类和个人中心diy设置',
            '提示',
            {
              confirmButtonText: '知道了',
            }
          )
          Promise.all(
            needCreateSystemPage.map((item) => {
              let systemPage = new item.Controller()
              systemPage.updateGlobalColor(this.globleColor.color)
              return pageAdd({
                ...systemPage.data,
                data: JSON.stringify(systemPage.data.data),
              })
            })
          )
            .then((res) => {
              return allPage()
            })
            .then((res) => {
              let arr = res.info
              if (!arr) {
                return
              }
              oldDataFix(arr)
              this.$store.commit('diy/SET_ALL_PAGE', arr)
            })
        }
      }, 30)
    },
  },
}
