export default class PageBase {
  constructor(data) {
    this.data = data
    this.globalColor = 'rgba(194, 30, 48, 1)'
  }
  updateId(id) {
    this.data.id = id
    return this
  }
  updateGlobalColor(color) {
    this.globalColor = color
    this.data = JSON.parse(
      JSON.stringify(this.data).replace(/GLOBAL_COLOR/g, this.globalColor)
    )
    return this
  }
}
