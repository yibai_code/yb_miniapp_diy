import PageBase from '@/wegts/system/PageBase'

let isRestaurant = 0
let isMerchant = 0
/* IFTRUE_isRestaurant
  isRestaurant = 1
FITRUE_isRestaurant */
/* IFTRUE_isMerchant
  isMerchant = 1
FITRUE_isMerchant */

export default class SystemMember extends PageBase {
  constructor(props) {
    super(
      isRestaurant
        ? restaurantDefaultData
        : isMerchant
        ? merchantDefaultPageData
        : defaultPageData
    )
  }
}

const defaultPageData = {
  id: 0,
  title: '个人中心',
  tmpl_type: 1,
  status: 1,
  data: {
    styles: {
      css: {
        n162753892766179: {
          bg: {
            type: 'color',
            color: { color: 'GLOBAL_COLOR', alpha: 100 },
            gradient: { begin: 'transparent', end: 'transparent' },
            image: {
              path: '',
              px: 0,
              py: 0,
              repeat: { repeat: 'no-repeat', size: 'cover' },
            },
          },
        },
        'n162753892766179 .mine-info-name': {
          font: {
            size: 15,
            color: 'rgba(250, 250, 250, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162753892766179 .mine-assets-title': {
          font: {
            size: 14,
            color: 'rgba(250, 250, 250, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162753892766179 .mine-assets-name': {
          font: {
            size: 11,
            color: 'rgba(250, 250, 250, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n16275389435643: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n16275389435643 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n16275389435643 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n16275389435643 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n16275389435643 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n16275389435643 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n16275390208640: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n16275390208640 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n16275390208640 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n16275390208640 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n16275390208640 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n16275390208640 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821375680199: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821375680199 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821375680199 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821375680199 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821375680199 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821375680199 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821382398025: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821382398025 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821382398025 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821382398025 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821382398025 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821382398025 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821388720064: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821388720064 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821388720064 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821388720064 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821388720064 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821388720064 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821389039836: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821389039836 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821389039836 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821389039836 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821389039836 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821389039836 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821390011928: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821390011928 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821390011928 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821390011928 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821390011928 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821390011928 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821580968366: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821580968366 .mine-list-wrapper': {
          'padding-top': { value: 0 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821580968366 .primary-list': {
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
      },
    },
    all_data: [
      /* 会员信息组件 */
      {
        id: 'n162753892766179',
        type: 'diy_member',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        ids: [],
        show_member_card: true,
        info_list: [
          {
            link: { type: 'page_integral_mall', title: '积分商城' },
            title: '积分',
            desc: 0,
            hidden: false,
          },
          {
            link: { type: 'coupon', params: 'private' },
            title: '优惠券',
            desc: 0,
            hidden: false,
          },
          {
            link: { type: 'balance', title: '我的余额' },
            title: '余额',
            desc: '0.00',
            hidden: false,
          },
        ],
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA39pVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo3Yzc3MTA4ZS1hMTkxLTk3NGUtYjhmOC0yNTk2OTg0NTM4M2QiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzU1M0IwNkVERTZDMTFFOThENzhFRjQ2QThGNzhDNjIiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzU1M0IwNkRERTZDMTFFOThENzhFRjQ2QThGNzhDNjIiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOmU3MjVhZGZiLWE1ZjgtNDY0Mi1iMDhiLTQxZmNkNzU4NjE5OSIgc3RSZWY6ZG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOmYwMDdkZWNmLTU3OTQtZTE0OC04YjAyLWNjZmM0NDE3MTc5NCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PoZYZm8AAAFfSURBVHjatNU/KIRxHMfxx+OOMiokMhiwoCw2g4FNynLKIAMl5X8oRQaSRSZHIpLEYFGyWEw3yCCDQQarUeGc8/7V50q58Dy+961XN/2+n9/9/j1+Op32csn3gtUSTtGB/L8MCBrQiDJMIIYC64BVPKEIg+hG4U8DIgEDzvU7ihIMKGAXLxYBmZAkxlCOPu3HdrYQ3wtXF1jGoybZi/5sexI2wNUlFvGgf9CDIUStAlwlFHKvXjEtXdQqwNUVFnCnfl2YzCyXRYCrG8zjVj07MYuKPHedDatKS1YH1zjhezmuiGGvGsyhFh84Q9wqoB4zCnHNT7DiLqRFQBOmUa3mR3qzkhZL1IwpbW4Kh1jDu8UetOhSVar5Pta/Nv9PQCvG9W1wDXfkzeIUtWMYpVrnLezh1eKYtmnmxXqaN3GQbeZhA0bU/FnrfZw5LVYB12jAhj7+qd8GWL9F3+pTgAEA/F6SKM1lmlEAAAAASUVORK5CYII=',
      },
      /* 会员信息组件 */
      /* 我的订单组件 */
      {
        id: 'n162821580968366',
        type: 'diy_order',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        ids: [],
        show_mark: true,
        title: '我的订单',
        right: '全部订单',
        list: [
          {
            count: '99',
            status: '待付款',
            title: '待付款',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAABHhJREFUaEPdmd1vFGUUxp9ndrdYRW1s9MaQGjWkRRKMNERLSK1C6e7ShSCY+JHANn5Hbr3xVv0DgAuLSCEWihAUSD9FtApSMaWapgUiGk24MDZqxQrt7nwcM9sP2zKzO7Mzs1t9k81ezHnPeX7nnHfmnXeIIg4ZbnkYiiRAxgGsAAQA+2FgN5clDzuRRidGftlIf3MEd0RqoWMjyEYAFVl8N7My+Uqu2IEDyE8tZZiQKMAEmPm/M5eomesiz7KqqS2bfSAAcvFgBZTURlBphEgtgIhj0bMNRS6iMrmcpNlblsMXAAGIy++vBJQEAPO3Ii/BlpNkEyubTvgOIFd2LoJx2xOQjGizn+/1T/ScMvSxsqnGFwC5dKAcihGDMAExGkAuDkb0PK8ij7Oq6QurWDlbSIb3PohwKAExW0NWAwwXRPScIOxi5faYKwC5vP85UDZDsKzwgi0iCqKsSv48/0rOCiwI8VlE/L8A5NxjpeqisudJrhNgqQC3AgjTvE0Wd9wQ4ApFOiKpP1tZ0zc+LWdGmNq/vk4U5QDAJcXVmiu6XKVhbItU93xuWmYA1AuxBqGcBJjfEzNXTN+vi0phIrKys5tjA9G7bwEuCVjue5wAHRLy+wRQxdSF6Fsk3wwwVmCuReRtpgeiQwAfCixKoI5l2ARI/3d6f342RGV6IGa7VQ00eT45LzyAz+liun8BVMADFNPfLAAAD+3E9Pk8ATxkzYPem6ZS7csTwEqFFyiKQPgxYbRqSnhIGzfGnIBS/cpHACcRLWwIjAqNrZGa7tNuXVA9G5PMeVKxBqFBjLrImu6zpgT5tuG+tGZU65BfSqtP9ZEwskmj+qWHCvgATsiecG3Xy5lN5UD9OwK8ASA0JXowomEDV31y1Q6Caq8HAD/WgcHVkSc7zqUG6rcQOHqzS+kteeRUnT3AZ/MBCvvuEub121nX+/f4meg+IZI0w0+fY5lvUoRcx2hZ+aPn/7KCoHo6XtQ1EA5NAlzrajwE4JlM+uYCYDEnyrju02uWANqpuHUn+9DfTu4LIYU1XNve98eRTS+JsDkzZ1YVqMjgXU+dsD3po9ZjA+Akup2NC3hC3g01dL4qR7aGRsZwWARbpiFIGSG19fckj39nuwa0rgAAXC1u0UKQNYx1fW1O+3XP02t1wSrAGFH18WMVr3WMZr2Nap1TAC6y5qU4ln0M/GaItjmyoeeMW9/U2rNUoJBQhA4DbaKgNZzSBqGkZo5OslfgZAAtVEBwascDAHC1Bpw3jdUTivpHBQJwrtPiYWw/mfqxHAAFbAdXjFO6qB91WYEFBkT9w7gOQHFFn6+x//AG9bbYDwAfyFeT53meoORH6oeiOwFlh60QTwE849k7yOgydnHiYPz+EmBIBKWOw/kA5XnTTowzjeUZP/JB7AURvucYwIuhD/CTG1Z5kds6984kQvbHkgLuxuRXGZvhOW/Wft1B3SDxOre3t0yCzBqyr34JWLJDIHEIlhbnk6oFo4gG4nuSHZD0Ljb9+44cUEq99Ji7uf8AL0WbPN0YVg8AAAAASUVORK5CYII=',
            link: { type: 'mine_order', tab: 1 },
            show: true,
          },
          {
            count: '99',
            status: '待发货',
            title: '待发货',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAAz9JREFUaEPtmclPFEEUxr/Xs6EsigoBNWpiDJFEDS6AGyogsuNh8Kwx8S/w5ImTF028m7igIsoMqwsEQojxYiIHTyRoTDxwkd1hBoaZ6S7TbI6gVPV0DcMkM9f63qv3e19VdXcNIc5/tFx/fbcnh1mUBhDKCdgeCVemVUFxqoMb2uPxY1plXN1qAQOmwdBDqtbgqkgb1scXAJy9cyVEWifAkg1nDQuINkDYVD7GLHXusi39VD/gzYKKITCkmylej91AAL31U7Agl5x93nsE3DZb/IYDAGDAfXL2znwlokNxCcDYN6rv84YAWOIRAICqAxg/Dv5Du6F7YKmGBEC4GQkHItiIiSUUqyW0fPLQ1e6pAxE498+Q0rSthdkOazMv3w9/sOrjzNwQTycyvvIyJyLmaVyfgxcVUgZ4OlVT867l27/wdCLjCYDwLsXEgdoe7wMRq0Q0Bx3WvedSbU6etn860DQSUMd4OpFxqnkv71Uiy6agPJ3/QdM14cdkyPwbjJ6Bqt95mflUi73KtiuoEADonPBjQgKAPidVvpXngA5QuYPvQMe4RICKN3IBqnbyAdrHxABEVgZd6ZIHsNuhoFoAoFUHCIqUx9/GVNYpF6BmF98B96hEgMsdcgFqM/4A6D0eD2jIsCt/tdL1049xWQ6ULgHIOIr2OBTUZS4C/App+DAZxMi8hv1JCorS7Ui1Lj74W3SAgKQlVNwuz4EUC6Eu047vsyoGPSGEn5Q2Agq22bAvSUHb6Dz8Kn99iyjoUps8gOUJ5fR2KRsnGV1olQ8g0jlZGipyxznAeZfPkOOGxLLavE4eOttiDGADajI0BZ15HecAp1cByHgeGGqhSTEVvIrAgU20ESi/OQIAk10zGr7eqqBTL40DbCIDQCeajAMY7WA09XT8RWwBzLpJec/XAphNGs2Or85Nx57F1gGzsHS00TsFUET/C4tMHl032TQdaZx1MTDuZdSaYqNbmUhvQERuOvzYd1xR6BPAbEJRRkVRA6Wgxljhwjde7lPvDWj0EASr0fpk6A0zMoRIYbeGrqc8WbmdznnkO6kQ7jCwEoDSZBQmPwfzEKhfY7g7fDN5UM8v9XpdfsH8jHEP8BtEtHpbfArplwAAAABJRU5ErkJggg==',
            link: { type: 'mine_order', tab: 2 },
            show: true,
          },
          {
            count: '99',
            status: '待收货',
            title: '待收货',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAABZtJREFUaEPtmW1MlWUYx///+zmAUtpm2WqkHA4nxdBNkyPyIgICqw+t1szlTGvNWh/asprZUtO12lofdGu1Vi2z5Rp9qbY2NzlTScSEAwoqCCHnHKRhJYq6JvLyPFd7zuEgL+cNd+TA1r2dnQ/3dV/X9bv/93U9b8Q0H5zm+YPFna0pFsOyT4AiAgl3DYg0YEibAG85rekHYxWHZR3tFQBKY+Uwkh8BepVo6Yes1kuRbKOZNwH6ACRGYxwrGxF51mm1/xwLfyaAxMLRhHwYsr4izV4+oTUhjP8HuKNdjKUCpd7JP0KUGB6hUk8cABBDgJK7BRCmNYgh6w/bY1TEJe7JVyCmAGvaJx8AIl2i1I1QDYCGYQDoEKW+OGKz/RquUXDNBffkXwcm0roErx9+1PZ5SNjiqQ4ADECh5IjNdiwYBIvbprgC/qz/6tMty6sz5neNhWDRH9MCACCq5c+0wsoiDo6EmDhAfCtmz9GFtrdHARS2xlWBVogchVK+nERkgIKHQawNdt7FNDFk7bFF6T8F5lnYEkcAsrxyYdr6sckWtrQfALghROe5ZkA9fizD6jHnufp8HAHA8t8WjQfIa7k8y4IbpyFMDwFR12305jVlZvZPSQAz6dWtFx2iD1STDPqYa0A+qFqUvosFzfFTQMDyqsfGKxDY9fzm9m0K/DioCsStAZVgZ0FTHAEYHgC7d6uC5zY6ARaHOEofcVWTx98Y49Mey6sWh1bATCunoTXFkpDUCJH7x0EQZ7nq3BDARO5PYmcbEcAMlXfWvU6RPwYJqzP/bFwBLoFojGo/hKWAaONuJfLPxBUgqtzDGTFvugGMqVXmNU5zBXIbpjvA6ekLYJ4m5kxjAN/NXM6pqa9AuGssV9ZPCYBbIDohkiTkPAqi/vAyAiDUGt8j3AEDRrnoerMpm6JlCTWug2ADIJY7beYCaYSBXYO0HqzP4oDpZ9mpS3OT0P+yGPIegNmRfDO7zhtaIZG/STx9MiutptDjmUFdz9QsFqOnu/tcfVbWwMo6T7aAvwB4KFKgsfMC/KAj9SUz8ae6upL79VuLdV0f6Om+7vOdU99pN0R3ArCGvZCtqPPqFPif6UYMIfoMQU5dVmpDQatnu6K8Q3KWKS4pl4Xau0fTrfsctZ6lVDxJQVIkiNs7Ja7kf725lYWFeulF904SWyG4d2j9P2LINmeaff/yus4lmuh1YT7ACB0u7ykCy8YD8FNXVuobeY3uT6hhK32J+39+CPOfr1UuSPvSUevZS3BLJIDAvEGW1TlSnWUX2/dA8GbQdcLNFVbbN45a71cEXglmI8Q5Omq9uQTM13dzRhoZg0a2JOpXErWEVlK0QNJ+CPHDKFzv6U9OmdnblyFKzJ2K5q78quvg/rlrNm60axacN0sqaHLAtauXe1Ioc/MoYn7HGz2IaxDtGV/lLjzeMmu2NjNbIAnUxKe0oa4cpjzwqgI+CyQ8CkINqaDw5PEml9PxiCN/bAxDGbpSmvme0zdEJ6kN9rtWpLtKOtq3KGBvONUMoNTbb1Td121Z6rfzF7rS2GcZVO7fc+f1hm1XWTUduwjZHQgyfHzUkAJ+NTadWGr7PtrjE7Ar8174UMDtPp+hBuX5ivn2YM8BwyvCAiyv8byowP1B/Q/XBFfVZKUenyhAcZt7M5V8PbKmfMmMyIiK2Yfm2WrD+Q4LkHmic06yGvTC7D7Bh8d107MARUWjXvdFA5PbeOHBxBnKTeIeP8RwXQUaRZtzvi0D5gfycCJFCuY46X5BqL7j+GLrBY0nXCuCvzWO5NecL2h2bwL5LSGKgZoyAyncVKKVOe2p1ZH8RHXJdpzoWAOL7BCRHIKGAEc4KDtceWkNkQJEms9vbi+BqPcpspIKg1RwGonYWWWznYm0dsyJi8Z86tlEpcDUS/t2Rv8BveNIIo/OtMcAAAAASUVORK5CYII=',
            link: { type: 'mine_order', tab: 3 },
            show: true,
          },
          {
            count: '99',
            status: '待评价',
            title: '待评价',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAA4FJREFUaEPtmUtoFVccxr9v0pjEZykhiFQ3Ci4EEYqrdllFCnXZWkmixhjNQ81DxRpj02qrUqQ+ru8HKiEqrkRURCVWcCOWFtwoFqx268bc1yS5ma/cXAytuZdz5t4xmQs9MKvz/3//7/c/Z2bOMESRDxa5f4wAuJe3LiX0I4BFAD4IOVQKwh+OtKN05YE7dHs7ltDhLQAlITf+jj0NO+QyDlzqeARwcXGZz7gl9IgDvR1DUOi3Tfb+Eim6Pe16b91/f8qjlulezAUwDtUD6Bzd823F4TQHLN1zrUUOcHazHYBdVACbwp8Ek6ftAAg8AXVWnvPGXwl/0XS8GQDrJCw0Zkpg8uQmY29J/FZW2v8Z15x3jaIBBLzoXl0+c9b0BxCM7ycmj280AkCqqWiK9ATgzVoicWLjNxR6TQlMHm0xAhBcXt585LpJLMh5N9L8hcgbJk0mI2YAED0VzZEak1iQ88kjLRcA1Jo0mTjcbFyBzMFDh5whRJJMvDaJFjJfocmVXqnTBKnNRoeJg012ADZqExDDxC8+AMYV1a4YEwca7SL9dDd4xZzVmfh5wziW89MFu1jG968vcoC9wQCQuC/gCYFhu97lilKVJ3xJcJqNDuM/NRS+AtLaKZ2nz9kUtIlJ7GuYIw8PIXxsimd8z7oCAfh4ys5TxjOLyci78/HdDY2gjmXN+5djxn/IAORNQV2Z2nVmhV+DpvjY7rqlkHPbFMdYd33e3kfEpb9fsn/egu6rg6Zifubj39fvkdBpymHsu7WFAWTOGZcGXafxo/2nCv5WEMDYrvqvCKXPQmVmgK66AABGygwIeEUankLmapUA0pfVYKwzMACrgkEHMfrtGnNPgq4aoB6j21fbAdhFBWjNTorRbZYAdnqFPJCtK4wGpu/46JZVIe2tHQ+j7bVFDtBaGxc02Y43fFHs31x9DeDyCbeW5z7gm6bqeSzBQwBVEw6Rh4GRf2TxlhWzhlnSDvETEJMAeFm0RCiVtYaYypYAyCM45vsg3WwC2bWoFMQx60HA86gxZULxl1LP781FSp+D9O3Hd0Ieq2yVoqd3FwLcCuBrAKVWSZmVDNfQn7/OxlCqDUQ9AONnZegA3rZTL/o+xKAaIW0CMDN7m9P3WMiHnt8sg1dWA2ELgPn/sSv9HnqA0RWRiGd96fdVK6BPIf0Fj9VFA5Bro/wPMNG30D+7IDIikOZzUwAAAABJRU5ErkJggg==',
            link: { type: 'mine_order', tab: 4 },
            show: true,
          },
          {
            count: '99',
            status: '退款/售后',
            title: '退款/售后',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAABxFJREFUaEPtWW1wVNUZfp5zdzeYzw4onbEdLExhRpfqj1JAaeuunXGo0+kAM8USO0BQitBSYxRBw2YvCYnFsaRNIUAgKYgfdJiOWoma+iOxOm3R6ThMWfxAHMsoOhMKJCSbsNl73869+5GQZDd3d0McZjh/spP7fjzPed9zznveQ1zlg1c5flwj8FVH8FoE0kUg+ILcqAELASwQ4lYCNwGYDEAB6IbgMyiEIDgKE21VpQxlGtFxj4Cui+LNWEwT60D44mCd4goJsFcUmvWl7HGiNK4EthySHwN4msAtTpynkTkrQPVxhcbDS2mkszUuBILPSrFyoZHAfTkCH67+rgn8Qv85P0plN2cCNc/JTFPhCIFZ4ww+Ya5bgNLgMraOZj8nAtUH5Ta45A0IbrhC4GNmiSjAFVXL+PxwP1kT0J/rn0F4/gHg61cUfMJ4jMSSYClfGeovKwL6HslngXkUwOwJAT/o5KIoNVcv5QeDvLJAoD9r7KTIuixUc1BJzvV78k01V/czGsuuFOOt92VW1MRd8OCIfyY/S4gFnpF5GgwrdazDKN04TcpfRGjtTFPHQk6gF4BHAPdYsjBlg77S/XRKAkfflylhA6dIlAjweVEfps+ZwwFLQX9moB1iH1BphxD1W5a7K/SW/hnQXO2ATEupQDbD0H5t0lisKCMW6nA9Ic/392nTt61h16gRaA/JIwRshiCMSW5Mnj+T3YEDA7drAmv2xxwCdtLU5uur+EnwQHgaxSLBGSPAgDu3rNDWgxT9wEA1BIExjccENuor3U+NSqAjJO8A+F7c0Gs+L++xZ39/pAWCMocOLLHTENOvr5r0SXBPeBo9FgkMkiB26Cs9623bzZHlorGFIppD+6f0Ms+3RxBoOyZT81z4Mrk+iOW+W3hQbxeXfBrthMjXHDqIiVFO0/D8SH+AH1sk4E6S2LFl1RDwCtbkOAUfTw58dwSB9hOyiIIX4yBF9eEbP5zDLwL7wgsUtbczAp8UljMRA/4nfznpI4uEuLQV1ffn1Vifq/ZFVlBJc6bgY6bV4yMIdByXAIjquO9PfV5Oj4W4v0KEv8uOgKUlZyJmjETCRlVzZDVFdjvY0VK5fWkEgTdDsk+A++Mar/u8tCpMVDX1N5FYnT0BQCBfKKq79QfyjutN/T8V2pEeaztO6VKAD0emUEheJLAornXQ5+Vy63egqb+VgL2YnQwhDAr+SuK/Q+WjUfOF2rX57+hN4TuE6meJb6a9ttRiQkqc2LdjSly0CVgLt8uFC0u9jHSE5DXEblEQYI/fywdtArv7/k7iB06NA9hVvea6jE7rqr2X7oVpHsrAB9hxXLaDeBjAGU1wj0HoGIxAi89LO52qdoffAvD9y42nLaW+FDIoEj09VEdE/ad2bf7nlTv6bqLLuDnxTRmqBAqPgExs3454sCMkESSOb+IsBacEmBePQKvfy5/YEdjV1wqI4xRK7d0sq1lbuD/QGC4nUe8IZWqhHnackDYI7k4hc9LnpX1R2byzN+NFLHYWopuxv/Ehv6pZV/j85l29aymoS/6bLIDI2HXQoIL160O+/YEUDRhoI3D7cBIkTLfghju8PBfY0VMBZrKNyps9l4wl9RUl55zMsq6Lx7i+dxvIcifytgwR20bTkRDBMv9sHgr8sXsBqDI4yPiG5s5fpK9h2Akgq5thXB+uAeQJJ/LxFH8iuQpTkRDgZb+Xi3RdXMaU3k4BMiklLhE4PxSQCSmvXV/0580NvatJSRyYEEEBiCKn4C05k7y8lGgPSaECjghw5xBDUVMw567ZPLb5Dz0toGRSzI3AY0LK6n5TvL+yoaucUFkvYhIf16wvmjlaMVcwyYXWoSQS50FlQ3g+xfhnJrM0XNY0pazu4eL9lfVd5VTZExBwY+1DhaOX023H5DISBB6808s99m5U390BXhahzPiYWLW1ovhPNgEy2whcGLiu6FspLzQWoldPSl7BJdxrCs75v8MjCZSP/75rnhI6uVKOToxsqC0veqhye3cTsqytKHh0a0WxXVhm1ZWo3N61E0BGZULyFLBLGFhdheQpnFkI8Z67u3iuro9xqU9nVNfP5EcK84+SnPC2iimc++Sjxbm1VSxym+ovzNAM+348MY0tIAoxl9RumJx7YysRnY2/PX+b0vg3QuJtk6wy0kkG2a3Fug0l49daTC7qus5Z1FyvgFeuuWuYUrpt0+Txb+4mSAQb/lcc6eO4t9dF+K6m1H1bHys5mSpM4xrzTU91LaRpWP0kr5O8SAPqLGBWn/z3lMbDhyfggWMoEKso68/rWgwY6wj6AHF85xUwJKbsvThgNDfqUyf+iWn4jD4W7LxRedRCZT3yAbeCtB/5BKIIWA8XVs/1BIF/mQpt2zZN+eof+XJJnWx0x3UNZAMgV51rBHKdwVz1r/oI/B85LZk3Ruc6YQAAAABJRU5ErkJggg==',
            link: { type: 'mine_order', tab: 5 },
            show: true,
          },
        ],
      },
      /* 我的订单组件 */
      /* 列表导航组件 */
      {
        id: 'n16275389435643',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '我的收藏',
            icon: require('@/assets/v2_image/icon_mine_36_2.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'collection',
              type_name: '我的收藏',
              id: 0,
              url: '',
              title: '我的收藏',
            },
          },
          {
            content: '电子凭证',
            icon: require('@/assets/v2_image/icon_mine_36_15.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_electronic',
              type_name: '电子凭证',
              id: 0,
              url: '',
              title: '电子凭证',
              target: '_self',
            },
          },
          {
            content: '我的优惠券',
            icon: require('@/assets/v2_image/icon_mine_36_3.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_coupon',
              type_name: '我的优惠券',
              id: 0,
              url: '',
              title: '我的优惠券',
            },
          },
          {
            content: '我的储值',
            icon: require('@/assets/v2_image/icon_mine_36_16.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'balance',
              type_name: '我的储值',
              id: 0,
              url: '',
              title: '我的储值',
            },
          },
        ],
      },
      {
        id: 'n16275390208640',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '积分商城',
            icon: require('@/assets/v2_image/icon_mine_36_8.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'integral_mall',
              type_name: '积分商城',
              id: 0,
              url: '',
              title: '积分商城',
            },
          },
          {
            content: '佣金中心',
            icon: require('@/assets/v2_image/icon_mine_36_9.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'page_distribution',
              type_name: '佣金中心',
              id: 0,
              url: '',
              title: '佣金中心',
            },
          },
          {
            content: '股东中心',
            icon: require('@/assets/v2_image/icon_mine_36_26.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'page_shareholder_index',
              type_name: '股东中心',
              id: 0,
              url: '',
              title: '股东中心',
            },
          },
          {
            content: '队长中心',
            icon: require('@/assets/v2_image/icon_mine_36_27.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'page_captain_center',
              type_name: '队长中心',
              id: 0,
              url: '',
              title: '队长中心',
              target: '_self',
            },
          },
        ],
      },
      {
        id: 'n162821375680199',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '团长中心',
            icon: require('@/assets/v2_image/icon_mine_36_28.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'page_community_index',
              type_name: '团长中心',
              id: 0,
              url: '',
              title: '团长中心',
            },
          },
          {
            content: '我的拼团',
            icon: require('@/assets/v2_image/icon_mine_36_12.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_order_group',
              type_name: '我的拼团',
              id: 0,
              url: '',
              title: '我的拼团',
            },
          },
          {
            content: '我的砍价',
            icon: require('@/assets/v2_image/icon_mine_36_13.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_order_bargain',
              type_name: '我的砍价',
              id: 0,
              url: '',
              title: '我的砍价',
            },
          },
          {
            content: '我的秒杀',
            icon: require('@/assets/v2_image/icon_mine_36_10.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_order_seckill',
              type_name: '我的秒杀',
              id: 0,
              url: '',
              title: '我的秒杀',
              target: '_self',
            },
          },
        ],
      },
      {
        id: 'n162821382398025',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '我的换购',
            icon: require('@/assets/v2_image/icon_mine_36_14.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_order_integral',
              type_name: '我的换购',
              id: 0,
              url: '',
              title: '我的换购',
            },
          },
          {
            content: '我的兑换码',
            icon: require('@/assets/v2_image/icon_mine_36_19.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_goods_code',
              type_name: '我的兑换码',
              id: 0,
              url: '',
              title: '我的兑换码',
            },
          },
          {
            content: '我的表单',
            icon: require('@/assets/v2_image/icon_mine_36_18.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_form',
              type_name: '我的表单',
              id: '',
              url: '',
              title: '我的表单',
            },
          },
          {
            content: '我的预约',
            icon: require('@/assets/v2_image/icon_mine_36_11.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'services_order_list',
              type_name: '我的预约',
              id: 0,
              url: '',
              title: '我的预约',
              target: '_self',
            },
          },
        ],
      },
      {
        id: 'n162821388720064',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '我的课程',
            icon: require('@/assets/v2_image/icon_mine_36_17.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_course',
              type_name: '我的课程',
              id: 0,
              url: '',
              title: '我的课程',
            },
          },
          {
            content: '我的评价',
            icon: require('@/assets/v2_image/icon_mine_36_4.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_evaluate',
              type_name: '我的评价',
              id: 0,
              url: '',
              title: '我的评价',
            },
          },
          {
            content: '我的爆料',
            icon: require('@/assets/v2_image/icon_mine_36_20.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_fact_list',
              type_name: '我的爆料',
              id: '',
              url: '',
              title: '我的爆料',
            },
          },
          {
            content: '种草收藏',
            icon: require('@/assets/v2_image/icon_mine_36_21.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_grass_collection',
              type_name: '种草收藏',
              id: '',
              url: '',
              title: '种草收藏',
            },
          },
        ],
      },
      {
        id: 'n162821389039836',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '种草评论',
            icon: require('@/assets/v2_image/icon_mine_36_22.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_grass_comment',
              type_name: '种草评论',
              id: '',
              url: '',
              title: '种草评论',
            },
          },
          {
            content: '曝光评论',
            icon: require('@/assets/v2_image/icon_mine_36_23.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_fact_comment',
              type_name: '曝光评论',
              id: '',
              url: '',
              title: '曝光评论',
            },
          },
          {
            content: '我的关注',
            icon: require('@/assets/v2_image/icon_mine_36_24.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_grass_follow',
              type_name: '我的关注',
              id: '',
              url: '',
              title: '我的关注',
            },
          },
          {
            content: '代理中心',
            icon: require('@/assets/v2_image/icon_mine_36_25.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'page_agent_index',
              type_name: '代理中心',
              id: 0,
              url: '',
              title: '代理中心',
              target: '_self',
            },
          },
        ],
      },
      {
        id: 'n162821390011928',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '收货地址',
            icon: require('@/assets/v2_image/icon_mine_36_5.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'address',
              type_name: '收货地址',
              id: 0,
              url: '',
              title: '收货地址',
            },
          },
          {
            content: '联系客服',
            icon: require('@/assets/v2_image/icon_mine_36_6.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'chat',
              type_name: '联系客服',
              id: 0,
              url: '',
              title: '联系客服',
            },
          },
          {
            content: '关于我们',
            icon: require('@/assets/v2_image/icon_mine_36_7.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'about',
              type_name: '关于我们',
              id: 0,
              url: '',
              title: '关于我们',
            },
          },
        ],
      },
      /* 列表导航组件 */
    ],
    page: {
      name: '个人中心',
      nv_color: '#ffffff',
      bg_color: '#f4f4f4',
      text_color: 'black',
      bg_img: '',
    },
  },
  issystem: 0,
  page_flag: 'page',
}
const restaurantDefaultData = {
  id: 0,
  title: '个人中心',
  tmpl_type: 1,
  status: 1,
  data: {
    styles: {
      css: {
        n162753892766179: {
          bg: {
            type: 'color',
            color: { color: 'GLOBAL_COLOR', alpha: 100 },
            gradient: { begin: 'transparent', end: 'transparent' },
            image: {
              path: '',
              px: 0,
              py: 0,
              repeat: { repeat: 'no-repeat', size: 'cover' },
            },
          },
        },
        'n162753892766179 .mine-info-name': {
          font: {
            size: 15,
            color: 'rgba(250, 250, 250, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162753892766179 .mine-assets-title': {
          font: {
            size: 14,
            color: 'rgba(250, 250, 250, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162753892766179 .mine-assets-name': {
          font: {
            size: 11,
            color: 'rgba(250, 250, 250, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n16275389435643: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n16275389435643 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n16275389435643 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n16275389435643 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n16275389435643 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n16275389435643 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n16275390208640: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n16275390208640 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n16275390208640 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n16275390208640 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n16275390208640 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n16275390208640 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821375680199: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821375680199 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821375680199 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821375680199 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821375680199 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821375680199 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821382398025: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821382398025 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821382398025 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821382398025 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821382398025 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821382398025 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821388720064: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821388720064 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821388720064 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821388720064 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821388720064 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821388720064 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821389039836: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821389039836 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821389039836 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821389039836 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821389039836 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821389039836 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821390011928: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821390011928 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821390011928 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821390011928 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821390011928 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821390011928 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821580968366: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821580968366 .mine-list-wrapper': {
          'padding-top': { value: 0 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821580968366 .primary-list': {
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
      },
    },
    all_data: [
      /* 会员信息组件 */
      {
        id: 'n162753892766179',
        type: 'diy_member',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        ids: [],
        show_member_card: true,
        info_list: [
          {
            link: { type: 'page_integral_mall', title: '积分商城' },
            title: '积分',
            desc: 0,
            hidden: false,
          },
          {
            link: { type: 'coupon', params: 'private' },
            title: '优惠券',
            desc: 0,
            hidden: false,
          },
          {
            link: { type: 'balance', title: '我的余额' },
            title: '余额',
            desc: '0.00',
            hidden: false,
          },
        ],
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA39pVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo3Yzc3MTA4ZS1hMTkxLTk3NGUtYjhmOC0yNTk2OTg0NTM4M2QiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzU1M0IwNkVERTZDMTFFOThENzhFRjQ2QThGNzhDNjIiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzU1M0IwNkRERTZDMTFFOThENzhFRjQ2QThGNzhDNjIiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOmU3MjVhZGZiLWE1ZjgtNDY0Mi1iMDhiLTQxZmNkNzU4NjE5OSIgc3RSZWY6ZG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOmYwMDdkZWNmLTU3OTQtZTE0OC04YjAyLWNjZmM0NDE3MTc5NCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PoZYZm8AAAFfSURBVHjatNU/KIRxHMfxx+OOMiokMhiwoCw2g4FNynLKIAMl5X8oRQaSRSZHIpLEYFGyWEw3yCCDQQarUeGc8/7V50q58Dy+961XN/2+n9/9/j1+Op32csn3gtUSTtGB/L8MCBrQiDJMIIYC64BVPKEIg+hG4U8DIgEDzvU7ihIMKGAXLxYBmZAkxlCOPu3HdrYQ3wtXF1jGoybZi/5sexI2wNUlFvGgf9CDIUStAlwlFHKvXjEtXdQqwNUVFnCnfl2YzCyXRYCrG8zjVj07MYuKPHedDatKS1YH1zjhezmuiGGvGsyhFh84Q9wqoB4zCnHNT7DiLqRFQBOmUa3mR3qzkhZL1IwpbW4Kh1jDu8UetOhSVar5Pta/Nv9PQCvG9W1wDXfkzeIUtWMYpVrnLezh1eKYtmnmxXqaN3GQbeZhA0bU/FnrfZw5LVYB12jAhj7+qd8GWL9F3+pTgAEA/F6SKM1lmlEAAAAASUVORK5CYII=',
      },
      /* 会员信息组件 */
      /* 我的订单组件 */
      {
        id: 'n162821580968366',
        type: 'diy_order',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        ids: [],
        show_mark: true,
        title: '我的订单',
        right: '全部订单',
        list: [
          {
            count: '99',
            status: '待付款',
            title: '待付款',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAABHhJREFUaEPdmd1vFGUUxp9ndrdYRW1s9MaQGjWkRRKMNERLSK1C6e7ShSCY+JHANn5Hbr3xVv0DgAuLSCEWihAUSD9FtApSMaWapgUiGk24MDZqxQrt7nwcM9sP2zKzO7Mzs1t9k81ezHnPeX7nnHfmnXeIIg4ZbnkYiiRAxgGsAAQA+2FgN5clDzuRRidGftlIf3MEd0RqoWMjyEYAFVl8N7My+Uqu2IEDyE8tZZiQKMAEmPm/M5eomesiz7KqqS2bfSAAcvFgBZTURlBphEgtgIhj0bMNRS6iMrmcpNlblsMXAAGIy++vBJQEAPO3Ii/BlpNkEyubTvgOIFd2LoJx2xOQjGizn+/1T/ScMvSxsqnGFwC5dKAcihGDMAExGkAuDkb0PK8ij7Oq6QurWDlbSIb3PohwKAExW0NWAwwXRPScIOxi5faYKwC5vP85UDZDsKzwgi0iCqKsSv48/0rOCiwI8VlE/L8A5NxjpeqisudJrhNgqQC3AgjTvE0Wd9wQ4ApFOiKpP1tZ0zc+LWdGmNq/vk4U5QDAJcXVmiu6XKVhbItU93xuWmYA1AuxBqGcBJjfEzNXTN+vi0phIrKys5tjA9G7bwEuCVjue5wAHRLy+wRQxdSF6Fsk3wwwVmCuReRtpgeiQwAfCixKoI5l2ARI/3d6f342RGV6IGa7VQ00eT45LzyAz+liun8BVMADFNPfLAAAD+3E9Pk8ATxkzYPem6ZS7csTwEqFFyiKQPgxYbRqSnhIGzfGnIBS/cpHACcRLWwIjAqNrZGa7tNuXVA9G5PMeVKxBqFBjLrImu6zpgT5tuG+tGZU65BfSqtP9ZEwskmj+qWHCvgATsiecG3Xy5lN5UD9OwK8ASA0JXowomEDV31y1Q6Caq8HAD/WgcHVkSc7zqUG6rcQOHqzS+kteeRUnT3AZ/MBCvvuEub121nX+/f4meg+IZI0w0+fY5lvUoRcx2hZ+aPn/7KCoHo6XtQ1EA5NAlzrajwE4JlM+uYCYDEnyrju02uWANqpuHUn+9DfTu4LIYU1XNve98eRTS+JsDkzZ1YVqMjgXU+dsD3po9ZjA+Akup2NC3hC3g01dL4qR7aGRsZwWARbpiFIGSG19fckj39nuwa0rgAAXC1u0UKQNYx1fW1O+3XP02t1wSrAGFH18WMVr3WMZr2Nap1TAC6y5qU4ln0M/GaItjmyoeeMW9/U2rNUoJBQhA4DbaKgNZzSBqGkZo5OslfgZAAtVEBwascDAHC1Bpw3jdUTivpHBQJwrtPiYWw/mfqxHAAFbAdXjFO6qB91WYEFBkT9w7gOQHFFn6+x//AG9bbYDwAfyFeT53meoORH6oeiOwFlh60QTwE849k7yOgydnHiYPz+EmBIBKWOw/kA5XnTTowzjeUZP/JB7AURvucYwIuhD/CTG1Z5kds6984kQvbHkgLuxuRXGZvhOW/Wft1B3SDxOre3t0yCzBqyr34JWLJDIHEIlhbnk6oFo4gG4nuSHZD0Ljb9+44cUEq99Ji7uf8AL0WbPN0YVg8AAAAASUVORK5CYII=',
            link: { type: 'mine_order', tab: 1 },
            show: true,
          },
          {
            count: '99',
            status: '待发货',
            title: '待发货',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAAz9JREFUaEPtmclPFEEUxr/Xs6EsigoBNWpiDJFEDS6AGyogsuNh8Kwx8S/w5ImTF028m7igIsoMqwsEQojxYiIHTyRoTDxwkd1hBoaZ6S7TbI6gVPV0DcMkM9f63qv3e19VdXcNIc5/tFx/fbcnh1mUBhDKCdgeCVemVUFxqoMb2uPxY1plXN1qAQOmwdBDqtbgqkgb1scXAJy9cyVEWifAkg1nDQuINkDYVD7GLHXusi39VD/gzYKKITCkmylej91AAL31U7Agl5x93nsE3DZb/IYDAGDAfXL2znwlokNxCcDYN6rv84YAWOIRAICqAxg/Dv5Du6F7YKmGBEC4GQkHItiIiSUUqyW0fPLQ1e6pAxE498+Q0rSthdkOazMv3w9/sOrjzNwQTycyvvIyJyLmaVyfgxcVUgZ4OlVT867l27/wdCLjCYDwLsXEgdoe7wMRq0Q0Bx3WvedSbU6etn860DQSUMd4OpFxqnkv71Uiy6agPJ3/QdM14cdkyPwbjJ6Bqt95mflUi73KtiuoEADonPBjQgKAPidVvpXngA5QuYPvQMe4RICKN3IBqnbyAdrHxABEVgZd6ZIHsNuhoFoAoFUHCIqUx9/GVNYpF6BmF98B96hEgMsdcgFqM/4A6D0eD2jIsCt/tdL1049xWQ6ULgHIOIr2OBTUZS4C/App+DAZxMi8hv1JCorS7Ui1Lj74W3SAgKQlVNwuz4EUC6Eu047vsyoGPSGEn5Q2Agq22bAvSUHb6Dz8Kn99iyjoUps8gOUJ5fR2KRsnGV1olQ8g0jlZGipyxznAeZfPkOOGxLLavE4eOttiDGADajI0BZ15HecAp1cByHgeGGqhSTEVvIrAgU20ESi/OQIAk10zGr7eqqBTL40DbCIDQCeajAMY7WA09XT8RWwBzLpJec/XAphNGs2Or85Nx57F1gGzsHS00TsFUET/C4tMHl032TQdaZx1MTDuZdSaYqNbmUhvQERuOvzYd1xR6BPAbEJRRkVRA6Wgxljhwjde7lPvDWj0EASr0fpk6A0zMoRIYbeGrqc8WbmdznnkO6kQ7jCwEoDSZBQmPwfzEKhfY7g7fDN5UM8v9XpdfsH8jHEP8BtEtHpbfArplwAAAABJRU5ErkJggg==',
            link: { type: 'mine_order', tab: 2 },
            show: true,
          },
          {
            count: '99',
            status: '待收货',
            title: '待收货',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAABZtJREFUaEPtmW1MlWUYx///+zmAUtpm2WqkHA4nxdBNkyPyIgICqw+t1szlTGvNWh/asprZUtO12lofdGu1Vi2z5Rp9qbY2NzlTScSEAwoqCCHnHKRhJYq6JvLyPFd7zuEgL+cNd+TA1r2dnQ/3dV/X9bv/93U9b8Q0H5zm+YPFna0pFsOyT4AiAgl3DYg0YEibAG85rekHYxWHZR3tFQBKY+Uwkh8BepVo6Yes1kuRbKOZNwH6ACRGYxwrGxF51mm1/xwLfyaAxMLRhHwYsr4izV4+oTUhjP8HuKNdjKUCpd7JP0KUGB6hUk8cABBDgJK7BRCmNYgh6w/bY1TEJe7JVyCmAGvaJx8AIl2i1I1QDYCGYQDoEKW+OGKz/RquUXDNBffkXwcm0roErx9+1PZ5SNjiqQ4ADECh5IjNdiwYBIvbprgC/qz/6tMty6sz5neNhWDRH9MCACCq5c+0wsoiDo6EmDhAfCtmz9GFtrdHARS2xlWBVogchVK+nERkgIKHQawNdt7FNDFk7bFF6T8F5lnYEkcAsrxyYdr6sckWtrQfALghROe5ZkA9fizD6jHnufp8HAHA8t8WjQfIa7k8y4IbpyFMDwFR12305jVlZvZPSQAz6dWtFx2iD1STDPqYa0A+qFqUvosFzfFTQMDyqsfGKxDY9fzm9m0K/DioCsStAZVgZ0FTHAEYHgC7d6uC5zY6ARaHOEofcVWTx98Y49Mey6sWh1bATCunoTXFkpDUCJH7x0EQZ7nq3BDARO5PYmcbEcAMlXfWvU6RPwYJqzP/bFwBLoFojGo/hKWAaONuJfLPxBUgqtzDGTFvugGMqVXmNU5zBXIbpjvA6ekLYJ4m5kxjAN/NXM6pqa9AuGssV9ZPCYBbIDohkiTkPAqi/vAyAiDUGt8j3AEDRrnoerMpm6JlCTWug2ADIJY7beYCaYSBXYO0HqzP4oDpZ9mpS3OT0P+yGPIegNmRfDO7zhtaIZG/STx9MiutptDjmUFdz9QsFqOnu/tcfVbWwMo6T7aAvwB4KFKgsfMC/KAj9SUz8ae6upL79VuLdV0f6Om+7vOdU99pN0R3ArCGvZCtqPPqFPif6UYMIfoMQU5dVmpDQatnu6K8Q3KWKS4pl4Xau0fTrfsctZ6lVDxJQVIkiNs7Ja7kf725lYWFeulF904SWyG4d2j9P2LINmeaff/yus4lmuh1YT7ACB0u7ykCy8YD8FNXVuobeY3uT6hhK32J+39+CPOfr1UuSPvSUevZS3BLJIDAvEGW1TlSnWUX2/dA8GbQdcLNFVbbN45a71cEXglmI8Q5Omq9uQTM13dzRhoZg0a2JOpXErWEVlK0QNJ+CPHDKFzv6U9OmdnblyFKzJ2K5q78quvg/rlrNm60axacN0sqaHLAtauXe1Ioc/MoYn7HGz2IaxDtGV/lLjzeMmu2NjNbIAnUxKe0oa4cpjzwqgI+CyQ8CkINqaDw5PEml9PxiCN/bAxDGbpSmvme0zdEJ6kN9rtWpLtKOtq3KGBvONUMoNTbb1Td121Z6rfzF7rS2GcZVO7fc+f1hm1XWTUduwjZHQgyfHzUkAJ+NTadWGr7PtrjE7Ar8174UMDtPp+hBuX5ivn2YM8BwyvCAiyv8byowP1B/Q/XBFfVZKUenyhAcZt7M5V8PbKmfMmMyIiK2Yfm2WrD+Q4LkHmic06yGvTC7D7Bh8d107MARUWjXvdFA5PbeOHBxBnKTeIeP8RwXQUaRZtzvi0D5gfycCJFCuY46X5BqL7j+GLrBY0nXCuCvzWO5NecL2h2bwL5LSGKgZoyAyncVKKVOe2p1ZH8RHXJdpzoWAOL7BCRHIKGAEc4KDtceWkNkQJEms9vbi+BqPcpspIKg1RwGonYWWWznYm0dsyJi8Z86tlEpcDUS/t2Rv8BveNIIo/OtMcAAAAASUVORK5CYII=',
            link: { type: 'mine_order', tab: 3 },
            show: true,
          },
          {
            count: '99',
            status: '待评价',
            title: '待评价',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAA4FJREFUaEPtmUtoFVccxr9v0pjEZykhiFQ3Ci4EEYqrdllFCnXZWkmixhjNQ81DxRpj02qrUqQ+ru8HKiEqrkRURCVWcCOWFtwoFqx268bc1yS5ma/cXAytuZdz5t4xmQs9MKvz/3//7/c/Z2bOMESRDxa5f4wAuJe3LiX0I4BFAD4IOVQKwh+OtKN05YE7dHs7ltDhLQAlITf+jj0NO+QyDlzqeARwcXGZz7gl9IgDvR1DUOi3Tfb+Eim6Pe16b91/f8qjlulezAUwDtUD6Bzd823F4TQHLN1zrUUOcHazHYBdVACbwp8Ek6ftAAg8AXVWnvPGXwl/0XS8GQDrJCw0Zkpg8uQmY29J/FZW2v8Z15x3jaIBBLzoXl0+c9b0BxCM7ycmj280AkCqqWiK9ATgzVoicWLjNxR6TQlMHm0xAhBcXt585LpJLMh5N9L8hcgbJk0mI2YAED0VzZEak1iQ88kjLRcA1Jo0mTjcbFyBzMFDh5whRJJMvDaJFjJfocmVXqnTBKnNRoeJg012ADZqExDDxC8+AMYV1a4YEwca7SL9dDd4xZzVmfh5wziW89MFu1jG968vcoC9wQCQuC/gCYFhu97lilKVJ3xJcJqNDuM/NRS+AtLaKZ2nz9kUtIlJ7GuYIw8PIXxsimd8z7oCAfh4ys5TxjOLyci78/HdDY2gjmXN+5djxn/IAORNQV2Z2nVmhV+DpvjY7rqlkHPbFMdYd33e3kfEpb9fsn/egu6rg6Zifubj39fvkdBpymHsu7WFAWTOGZcGXafxo/2nCv5WEMDYrvqvCKXPQmVmgK66AABGygwIeEUankLmapUA0pfVYKwzMACrgkEHMfrtGnNPgq4aoB6j21fbAdhFBWjNTorRbZYAdnqFPJCtK4wGpu/46JZVIe2tHQ+j7bVFDtBaGxc02Y43fFHs31x9DeDyCbeW5z7gm6bqeSzBQwBVEw6Rh4GRf2TxlhWzhlnSDvETEJMAeFm0RCiVtYaYypYAyCM45vsg3WwC2bWoFMQx60HA86gxZULxl1LP781FSp+D9O3Hd0Ieq2yVoqd3FwLcCuBrAKVWSZmVDNfQn7/OxlCqDUQ9AONnZegA3rZTL/o+xKAaIW0CMDN7m9P3WMiHnt8sg1dWA2ELgPn/sSv9HnqA0RWRiGd96fdVK6BPIf0Fj9VFA5Bro/wPMNG30D+7IDIikOZzUwAAAABJRU5ErkJggg==',
            link: { type: 'mine_order', tab: 4 },
            show: true,
          },
          {
            count: '99',
            status: '退款/售后',
            title: '退款/售后',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAABxFJREFUaEPtWW1wVNUZfp5zdzeYzw4onbEdLExhRpfqj1JAaeuunXGo0+kAM8USO0BQitBSYxRBw2YvCYnFsaRNIUAgKYgfdJiOWoma+iOxOm3R6ThMWfxAHMsoOhMKJCSbsNl73869+5GQZDd3d0McZjh/spP7fjzPed9zznveQ1zlg1c5flwj8FVH8FoE0kUg+ILcqAELASwQ4lYCNwGYDEAB6IbgMyiEIDgKE21VpQxlGtFxj4Cui+LNWEwT60D44mCd4goJsFcUmvWl7HGiNK4EthySHwN4msAtTpynkTkrQPVxhcbDS2mkszUuBILPSrFyoZHAfTkCH67+rgn8Qv85P0plN2cCNc/JTFPhCIFZ4ww+Ya5bgNLgMraOZj8nAtUH5Ta45A0IbrhC4GNmiSjAFVXL+PxwP1kT0J/rn0F4/gHg61cUfMJ4jMSSYClfGeovKwL6HslngXkUwOwJAT/o5KIoNVcv5QeDvLJAoD9r7KTIuixUc1BJzvV78k01V/czGsuuFOOt92VW1MRd8OCIfyY/S4gFnpF5GgwrdazDKN04TcpfRGjtTFPHQk6gF4BHAPdYsjBlg77S/XRKAkfflylhA6dIlAjweVEfps+ZwwFLQX9moB1iH1BphxD1W5a7K/SW/hnQXO2ATEupQDbD0H5t0lisKCMW6nA9Ic/392nTt61h16gRaA/JIwRshiCMSW5Mnj+T3YEDA7drAmv2xxwCdtLU5uur+EnwQHgaxSLBGSPAgDu3rNDWgxT9wEA1BIExjccENuor3U+NSqAjJO8A+F7c0Gs+L++xZ39/pAWCMocOLLHTENOvr5r0SXBPeBo9FgkMkiB26Cs9623bzZHlorGFIppD+6f0Ms+3RxBoOyZT81z4Mrk+iOW+W3hQbxeXfBrthMjXHDqIiVFO0/D8SH+AH1sk4E6S2LFl1RDwCtbkOAUfTw58dwSB9hOyiIIX4yBF9eEbP5zDLwL7wgsUtbczAp8UljMRA/4nfznpI4uEuLQV1ffn1Vifq/ZFVlBJc6bgY6bV4yMIdByXAIjquO9PfV5Oj4W4v0KEv8uOgKUlZyJmjETCRlVzZDVFdjvY0VK5fWkEgTdDsk+A++Mar/u8tCpMVDX1N5FYnT0BQCBfKKq79QfyjutN/T8V2pEeaztO6VKAD0emUEheJLAornXQ5+Vy63egqb+VgL2YnQwhDAr+SuK/Q+WjUfOF2rX57+hN4TuE6meJb6a9ttRiQkqc2LdjSly0CVgLt8uFC0u9jHSE5DXEblEQYI/fywdtArv7/k7iB06NA9hVvea6jE7rqr2X7oVpHsrAB9hxXLaDeBjAGU1wj0HoGIxAi89LO52qdoffAvD9y42nLaW+FDIoEj09VEdE/ad2bf7nlTv6bqLLuDnxTRmqBAqPgExs3454sCMkESSOb+IsBacEmBePQKvfy5/YEdjV1wqI4xRK7d0sq1lbuD/QGC4nUe8IZWqhHnackDYI7k4hc9LnpX1R2byzN+NFLHYWopuxv/Ehv6pZV/j85l29aymoS/6bLIDI2HXQoIL160O+/YEUDRhoI3D7cBIkTLfghju8PBfY0VMBZrKNyps9l4wl9RUl55zMsq6Lx7i+dxvIcifytgwR20bTkRDBMv9sHgr8sXsBqDI4yPiG5s5fpK9h2Akgq5thXB+uAeQJJ/LxFH8iuQpTkRDgZb+Xi3RdXMaU3k4BMiklLhE4PxSQCSmvXV/0580NvatJSRyYEEEBiCKn4C05k7y8lGgPSaECjghw5xBDUVMw567ZPLb5Dz0toGRSzI3AY0LK6n5TvL+yoaucUFkvYhIf16wvmjlaMVcwyYXWoSQS50FlQ3g+xfhnJrM0XNY0pazu4eL9lfVd5VTZExBwY+1DhaOX023H5DISBB6808s99m5U390BXhahzPiYWLW1ovhPNgEy2whcGLiu6FspLzQWoldPSl7BJdxrCs75v8MjCZSP/75rnhI6uVKOToxsqC0veqhye3cTsqytKHh0a0WxXVhm1ZWo3N61E0BGZULyFLBLGFhdheQpnFkI8Z67u3iuro9xqU9nVNfP5EcK84+SnPC2iimc++Sjxbm1VSxym+ovzNAM+348MY0tIAoxl9RumJx7YysRnY2/PX+b0vg3QuJtk6wy0kkG2a3Fug0l49daTC7qus5Z1FyvgFeuuWuYUrpt0+Txb+4mSAQb/lcc6eO4t9dF+K6m1H1bHys5mSpM4xrzTU91LaRpWP0kr5O8SAPqLGBWn/z3lMbDhyfggWMoEKso68/rWgwY6wj6AHF85xUwJKbsvThgNDfqUyf+iWn4jD4W7LxRedRCZT3yAbeCtB/5BKIIWA8XVs/1BIF/mQpt2zZN+eof+XJJnWx0x3UNZAMgV51rBHKdwVz1r/oI/B85LZk3Ruc6YQAAAABJRU5ErkJggg==',
            link: { type: 'mine_order', tab: 5 },
            show: true,
          },
        ],
      },
      /* 我的订单组件 */
      /* 列表导航组件 */
      {
        id: 'n16275389435643',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '我的收藏',
            icon: require('@/assets/v2_image/icon_mine_36_2.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'collection',
              type_name: '我的收藏',
              id: 0,
              url: '',
              title: '我的收藏',
            },
          },
          {
            content: '电子凭证',
            icon: require('@/assets/v2_image/icon_mine_36_15.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_electronic',
              type_name: '电子凭证',
              id: 0,
              url: '',
              title: '电子凭证',
              target: '_self',
            },
          },
          {
            content: '我的优惠券',
            icon: require('@/assets/v2_image/icon_mine_36_3.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_coupon',
              type_name: '我的优惠券',
              id: 0,
              url: '',
              title: '我的优惠券',
            },
          },
        ],
      },
      {
        id: 'n16275390208640',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '我的储值',
            icon: require('@/assets/v2_image/icon_mine_36_16.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'balance',
              type_name: '我的储值',
              id: 0,
              url: '',
              title: '我的储值',
            },
          },
          {
            content: '积分商城',
            icon: require('@/assets/v2_image/icon_mine_36_8.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'integral_mall',
              type_name: '积分商城',
              id: 0,
              url: '',
              title: '积分商城',
            },
          },
          {
            content: '佣金中心',
            icon: require('@/assets/v2_image/icon_mine_36_9.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'page_distribution',
              type_name: '佣金中心',
              id: 0,
              url: '',
              title: '佣金中心',
            },
          },
        ],
      },
      {
        id: 'n162821375680199',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '我的拼团',
            icon: require('@/assets/v2_image/icon_mine_36_12.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_order_group',
              type_name: '我的拼团',
              id: 0,
              url: '',
              title: '我的拼团',
            },
          },
          {
            content: '我的砍价',
            icon: require('@/assets/v2_image/icon_mine_36_13.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_order_bargain',
              type_name: '我的砍价',
              id: 0,
              url: '',
              title: '我的砍价',
            },
          },
          {
            content: '我的秒杀',
            icon: require('@/assets/v2_image/icon_mine_36_10.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_order_seckill',
              type_name: '我的秒杀',
              id: 0,
              url: '',
              title: '我的秒杀',
              target: '_self',
            },
          },
        ],
      },
      {
        id: 'n162821382398025',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '我的换购',
            icon: require('@/assets/v2_image/icon_mine_36_14.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_order_integral',
              type_name: '我的换购',
              id: 0,
              url: '',
              title: '我的换购',
            },
          },
          {
            content: '我的兑换码',
            icon: require('@/assets/v2_image/icon_mine_36_19.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_goods_code',
              type_name: '我的兑换码',
              id: 0,
              url: '',
              title: '我的兑换码',
            },
          },
          {
            content: '我的表单',
            icon: require('@/assets/v2_image/icon_mine_36_18.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_form',
              type_name: '我的表单',
              id: '',
              url: '',
              title: '我的表单',
            },
          },
        ],
      },
      {
        id: 'n162821390011928',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '收货地址',
            icon: require('@/assets/v2_image/icon_mine_36_5.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'address',
              type_name: '收货地址',
              id: 0,
              url: '',
              title: '收货地址',
            },
          },
          {
            content: '联系客服',
            icon: require('@/assets/v2_image/icon_mine_36_6.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'chat',
              type_name: '联系客服',
              id: 0,
              url: '',
              title: '联系客服',
            },
          },
          {
            content: '关于我们',
            icon: require('@/assets/v2_image/icon_mine_36_7.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'about',
              type_name: '关于我们',
              id: 0,
              url: '',
              title: '关于我们',
            },
          },
        ],
      },
      /* 列表导航组件 */
    ],
    page: {
      name: '个人中心',
      nv_color: '#ffffff',
      bg_color: '#f4f4f4',
      text_color: 'black',
      bg_img: '',
    },
  },
  issystem: 0,
  page_flag: 'page',
}
const merchantDefaultPageData = {
  id: 0,
  title: '个人中心',
  tmpl_type: 1,
  status: 1,
  data: {
    styles: {
      css: {
        n162753892766179: {
          bg: {
            type: 'color',
            color: { color: 'GLOBAL_COLOR', alpha: 100 },
            gradient: { begin: 'transparent', end: 'transparent' },
            image: {
              path: '',
              px: 0,
              py: 0,
              repeat: { repeat: 'no-repeat', size: 'cover' },
            },
          },
        },
        'n162753892766179 .mine-info-name': {
          font: {
            size: 15,
            color: 'rgba(250, 250, 250, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162753892766179 .mine-assets-title': {
          font: {
            size: 14,
            color: 'rgba(250, 250, 250, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162753892766179 .mine-assets-name': {
          font: {
            size: 11,
            color: 'rgba(250, 250, 250, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n16275389435643: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n16275389435643 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n16275389435643 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n16275389435643 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n16275389435643 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n16275389435643 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n16275390208640: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n16275390208640 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n16275390208640 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n16275390208640 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n16275390208640 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n16275390208640 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821375680199: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821375680199 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821375680199 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821375680199 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821375680199 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821375680199 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821382398025: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821382398025 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821382398025 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821382398025 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821382398025 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821382398025 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821388720064: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821388720064 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821388720064 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821388720064 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821388720064 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821388720064 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821389039836: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821389039836 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821389039836 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821389039836 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821389039836 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821389039836 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821390011928: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821390011928 .listmenu': {
          'padding-top': { value: 10 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821390011928 .container': {
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
        'n162821390011928 .menu-item': {
          bd: {
            t: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            r: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            b: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            l: { style: 'solid', color: 'rgba(241, 241, 241, 1.0)', width: 1 },
            now: 'all',
          },
        },
        'n162821390011928 .a_title': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        'n162821390011928 .tit': {
          font: {
            size: 12,
            color: 'rgba(53, 53, 53, 1.0)',
            isB: false,
            isI: false,
            isU: false,
          },
        },
        n162821580968366: {
          bg: {
            type: 'color',
            color: { color: 'rgba(244, 244, 244, 1.0)', alpha: 100 },
          },
        },
        'n162821580968366 .mine-list-wrapper': {
          'padding-top': { value: 0 },
          'padding-bottom': { value: 0 },
          'padding-left': { value: 0 },
          'padding-right': { value: 0 },
        },
        'n162821580968366 .primary-list': {
          bg: {
            type: 'color',
            color: { color: 'rgba(255, 255, 255, 1.0)', alpha: 100 },
          },
          br: { now: 'all', tl: 0, tr: 0, bl: 0, br: 0, radius: 0 },
          background: { value: 'rgba(255, 255, 255,1.00)' },
        },
      },
    },
    all_data: [
      /* 会员信息组件 */
      {
        id: 'n162753892766179',
        type: 'diy_member',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        ids: [],
        show_member_card: true,
        info_list: [
          {
            link: { type: 'page_integral_mall', title: '积分商城' },
            title: '积分',
            desc: 0,
            hidden: false,
          },
          {
            link: { type: 'coupon', params: 'private' },
            title: '优惠券',
            desc: 0,
            hidden: false,
          },
          {
            link: { type: 'balance', title: '我的余额' },
            title: '余额',
            desc: '0.00',
            hidden: false,
          },
        ],
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA39pVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo3Yzc3MTA4ZS1hMTkxLTk3NGUtYjhmOC0yNTk2OTg0NTM4M2QiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzU1M0IwNkVERTZDMTFFOThENzhFRjQ2QThGNzhDNjIiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzU1M0IwNkRERTZDMTFFOThENzhFRjQ2QThGNzhDNjIiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOmU3MjVhZGZiLWE1ZjgtNDY0Mi1iMDhiLTQxZmNkNzU4NjE5OSIgc3RSZWY6ZG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOmYwMDdkZWNmLTU3OTQtZTE0OC04YjAyLWNjZmM0NDE3MTc5NCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PoZYZm8AAAFfSURBVHjatNU/KIRxHMfxx+OOMiokMhiwoCw2g4FNynLKIAMl5X8oRQaSRSZHIpLEYFGyWEw3yCCDQQarUeGc8/7V50q58Dy+961XN/2+n9/9/j1+Op32csn3gtUSTtGB/L8MCBrQiDJMIIYC64BVPKEIg+hG4U8DIgEDzvU7ihIMKGAXLxYBmZAkxlCOPu3HdrYQ3wtXF1jGoybZi/5sexI2wNUlFvGgf9CDIUStAlwlFHKvXjEtXdQqwNUVFnCnfl2YzCyXRYCrG8zjVj07MYuKPHedDatKS1YH1zjhezmuiGGvGsyhFh84Q9wqoB4zCnHNT7DiLqRFQBOmUa3mR3qzkhZL1IwpbW4Kh1jDu8UetOhSVar5Pta/Nv9PQCvG9W1wDXfkzeIUtWMYpVrnLezh1eKYtmnmxXqaN3GQbeZhA0bU/FnrfZw5LVYB12jAhj7+qd8GWL9F3+pTgAEA/F6SKM1lmlEAAAAASUVORK5CYII=',
      },
      /* 会员信息组件 */
      /* 我的订单组件 */
      {
        id: 'n162821580968366',
        type: 'diy_order',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        ids: [],
        show_mark: true,
        title: '我的订单',
        right: '全部订单',
        list: [
          {
            count: '99',
            status: '待付款',
            title: '待付款',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAABHhJREFUaEPdmd1vFGUUxp9ndrdYRW1s9MaQGjWkRRKMNERLSK1C6e7ShSCY+JHANn5Hbr3xVv0DgAuLSCEWihAUSD9FtApSMaWapgUiGk24MDZqxQrt7nwcM9sP2zKzO7Mzs1t9k81ezHnPeX7nnHfmnXeIIg4ZbnkYiiRAxgGsAAQA+2FgN5clDzuRRidGftlIf3MEd0RqoWMjyEYAFVl8N7My+Uqu2IEDyE8tZZiQKMAEmPm/M5eomesiz7KqqS2bfSAAcvFgBZTURlBphEgtgIhj0bMNRS6iMrmcpNlblsMXAAGIy++vBJQEAPO3Ii/BlpNkEyubTvgOIFd2LoJx2xOQjGizn+/1T/ScMvSxsqnGFwC5dKAcihGDMAExGkAuDkb0PK8ij7Oq6QurWDlbSIb3PohwKAExW0NWAwwXRPScIOxi5faYKwC5vP85UDZDsKzwgi0iCqKsSv48/0rOCiwI8VlE/L8A5NxjpeqisudJrhNgqQC3AgjTvE0Wd9wQ4ApFOiKpP1tZ0zc+LWdGmNq/vk4U5QDAJcXVmiu6XKVhbItU93xuWmYA1AuxBqGcBJjfEzNXTN+vi0phIrKys5tjA9G7bwEuCVjue5wAHRLy+wRQxdSF6Fsk3wwwVmCuReRtpgeiQwAfCixKoI5l2ARI/3d6f342RGV6IGa7VQ00eT45LzyAz+liun8BVMADFNPfLAAAD+3E9Pk8ATxkzYPem6ZS7csTwEqFFyiKQPgxYbRqSnhIGzfGnIBS/cpHACcRLWwIjAqNrZGa7tNuXVA9G5PMeVKxBqFBjLrImu6zpgT5tuG+tGZU65BfSqtP9ZEwskmj+qWHCvgATsiecG3Xy5lN5UD9OwK8ASA0JXowomEDV31y1Q6Caq8HAD/WgcHVkSc7zqUG6rcQOHqzS+kteeRUnT3AZ/MBCvvuEub121nX+/f4meg+IZI0w0+fY5lvUoRcx2hZ+aPn/7KCoHo6XtQ1EA5NAlzrajwE4JlM+uYCYDEnyrju02uWANqpuHUn+9DfTu4LIYU1XNve98eRTS+JsDkzZ1YVqMjgXU+dsD3po9ZjA+Akup2NC3hC3g01dL4qR7aGRsZwWARbpiFIGSG19fckj39nuwa0rgAAXC1u0UKQNYx1fW1O+3XP02t1wSrAGFH18WMVr3WMZr2Nap1TAC6y5qU4ln0M/GaItjmyoeeMW9/U2rNUoJBQhA4DbaKgNZzSBqGkZo5OslfgZAAtVEBwascDAHC1Bpw3jdUTivpHBQJwrtPiYWw/mfqxHAAFbAdXjFO6qB91WYEFBkT9w7gOQHFFn6+x//AG9bbYDwAfyFeT53meoORH6oeiOwFlh60QTwE849k7yOgydnHiYPz+EmBIBKWOw/kA5XnTTowzjeUZP/JB7AURvucYwIuhD/CTG1Z5kds6984kQvbHkgLuxuRXGZvhOW/Wft1B3SDxOre3t0yCzBqyr34JWLJDIHEIlhbnk6oFo4gG4nuSHZD0Ljb9+44cUEq99Ji7uf8AL0WbPN0YVg8AAAAASUVORK5CYII=',
            link: { type: 'mine_order', tab: 1 },
            show: true,
          },
          {
            count: '99',
            status: '待发货',
            title: '待发货',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAAz9JREFUaEPtmclPFEEUxr/Xs6EsigoBNWpiDJFEDS6AGyogsuNh8Kwx8S/w5ImTF028m7igIsoMqwsEQojxYiIHTyRoTDxwkd1hBoaZ6S7TbI6gVPV0DcMkM9f63qv3e19VdXcNIc5/tFx/fbcnh1mUBhDKCdgeCVemVUFxqoMb2uPxY1plXN1qAQOmwdBDqtbgqkgb1scXAJy9cyVEWifAkg1nDQuINkDYVD7GLHXusi39VD/gzYKKITCkmylej91AAL31U7Agl5x93nsE3DZb/IYDAGDAfXL2znwlokNxCcDYN6rv84YAWOIRAICqAxg/Dv5Du6F7YKmGBEC4GQkHItiIiSUUqyW0fPLQ1e6pAxE498+Q0rSthdkOazMv3w9/sOrjzNwQTycyvvIyJyLmaVyfgxcVUgZ4OlVT867l27/wdCLjCYDwLsXEgdoe7wMRq0Q0Bx3WvedSbU6etn860DQSUMd4OpFxqnkv71Uiy6agPJ3/QdM14cdkyPwbjJ6Bqt95mflUi73KtiuoEADonPBjQgKAPidVvpXngA5QuYPvQMe4RICKN3IBqnbyAdrHxABEVgZd6ZIHsNuhoFoAoFUHCIqUx9/GVNYpF6BmF98B96hEgMsdcgFqM/4A6D0eD2jIsCt/tdL1049xWQ6ULgHIOIr2OBTUZS4C/App+DAZxMi8hv1JCorS7Ui1Lj74W3SAgKQlVNwuz4EUC6Eu047vsyoGPSGEn5Q2Agq22bAvSUHb6Dz8Kn99iyjoUps8gOUJ5fR2KRsnGV1olQ8g0jlZGipyxznAeZfPkOOGxLLavE4eOttiDGADajI0BZ15HecAp1cByHgeGGqhSTEVvIrAgU20ESi/OQIAk10zGr7eqqBTL40DbCIDQCeajAMY7WA09XT8RWwBzLpJec/XAphNGs2Or85Nx57F1gGzsHS00TsFUET/C4tMHl032TQdaZx1MTDuZdSaYqNbmUhvQERuOvzYd1xR6BPAbEJRRkVRA6Wgxljhwjde7lPvDWj0EASr0fpk6A0zMoRIYbeGrqc8WbmdznnkO6kQ7jCwEoDSZBQmPwfzEKhfY7g7fDN5UM8v9XpdfsH8jHEP8BtEtHpbfArplwAAAABJRU5ErkJggg==',
            link: { type: 'mine_order', tab: 2 },
            show: true,
          },
          {
            count: '99',
            status: '待收货',
            title: '待收货',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAABZtJREFUaEPtmW1MlWUYx///+zmAUtpm2WqkHA4nxdBNkyPyIgICqw+t1szlTGvNWh/asprZUtO12lofdGu1Vi2z5Rp9qbY2NzlTScSEAwoqCCHnHKRhJYq6JvLyPFd7zuEgL+cNd+TA1r2dnQ/3dV/X9bv/93U9b8Q0H5zm+YPFna0pFsOyT4AiAgl3DYg0YEibAG85rekHYxWHZR3tFQBKY+Uwkh8BepVo6Yes1kuRbKOZNwH6ACRGYxwrGxF51mm1/xwLfyaAxMLRhHwYsr4izV4+oTUhjP8HuKNdjKUCpd7JP0KUGB6hUk8cABBDgJK7BRCmNYgh6w/bY1TEJe7JVyCmAGvaJx8AIl2i1I1QDYCGYQDoEKW+OGKz/RquUXDNBffkXwcm0roErx9+1PZ5SNjiqQ4ADECh5IjNdiwYBIvbprgC/qz/6tMty6sz5neNhWDRH9MCACCq5c+0wsoiDo6EmDhAfCtmz9GFtrdHARS2xlWBVogchVK+nERkgIKHQawNdt7FNDFk7bFF6T8F5lnYEkcAsrxyYdr6sckWtrQfALghROe5ZkA9fizD6jHnufp8HAHA8t8WjQfIa7k8y4IbpyFMDwFR12305jVlZvZPSQAz6dWtFx2iD1STDPqYa0A+qFqUvosFzfFTQMDyqsfGKxDY9fzm9m0K/DioCsStAZVgZ0FTHAEYHgC7d6uC5zY6ARaHOEofcVWTx98Y49Mey6sWh1bATCunoTXFkpDUCJH7x0EQZ7nq3BDARO5PYmcbEcAMlXfWvU6RPwYJqzP/bFwBLoFojGo/hKWAaONuJfLPxBUgqtzDGTFvugGMqVXmNU5zBXIbpjvA6ekLYJ4m5kxjAN/NXM6pqa9AuGssV9ZPCYBbIDohkiTkPAqi/vAyAiDUGt8j3AEDRrnoerMpm6JlCTWug2ADIJY7beYCaYSBXYO0HqzP4oDpZ9mpS3OT0P+yGPIegNmRfDO7zhtaIZG/STx9MiutptDjmUFdz9QsFqOnu/tcfVbWwMo6T7aAvwB4KFKgsfMC/KAj9SUz8ae6upL79VuLdV0f6Om+7vOdU99pN0R3ArCGvZCtqPPqFPif6UYMIfoMQU5dVmpDQatnu6K8Q3KWKS4pl4Xau0fTrfsctZ6lVDxJQVIkiNs7Ja7kf725lYWFeulF904SWyG4d2j9P2LINmeaff/yus4lmuh1YT7ACB0u7ykCy8YD8FNXVuobeY3uT6hhK32J+39+CPOfr1UuSPvSUevZS3BLJIDAvEGW1TlSnWUX2/dA8GbQdcLNFVbbN45a71cEXglmI8Q5Omq9uQTM13dzRhoZg0a2JOpXErWEVlK0QNJ+CPHDKFzv6U9OmdnblyFKzJ2K5q78quvg/rlrNm60axacN0sqaHLAtauXe1Ioc/MoYn7HGz2IaxDtGV/lLjzeMmu2NjNbIAnUxKe0oa4cpjzwqgI+CyQ8CkINqaDw5PEml9PxiCN/bAxDGbpSmvme0zdEJ6kN9rtWpLtKOtq3KGBvONUMoNTbb1Td121Z6rfzF7rS2GcZVO7fc+f1hm1XWTUduwjZHQgyfHzUkAJ+NTadWGr7PtrjE7Ar8174UMDtPp+hBuX5ivn2YM8BwyvCAiyv8byowP1B/Q/XBFfVZKUenyhAcZt7M5V8PbKmfMmMyIiK2Yfm2WrD+Q4LkHmic06yGvTC7D7Bh8d107MARUWjXvdFA5PbeOHBxBnKTeIeP8RwXQUaRZtzvi0D5gfycCJFCuY46X5BqL7j+GLrBY0nXCuCvzWO5NecL2h2bwL5LSGKgZoyAyncVKKVOe2p1ZH8RHXJdpzoWAOL7BCRHIKGAEc4KDtceWkNkQJEms9vbi+BqPcpspIKg1RwGonYWWWznYm0dsyJi8Z86tlEpcDUS/t2Rv8BveNIIo/OtMcAAAAASUVORK5CYII=',
            link: { type: 'mine_order', tab: 3 },
            show: true,
          },
          {
            count: '99',
            status: '待评价',
            title: '待评价',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAA4FJREFUaEPtmUtoFVccxr9v0pjEZykhiFQ3Ci4EEYqrdllFCnXZWkmixhjNQ81DxRpj02qrUqQ+ru8HKiEqrkRURCVWcCOWFtwoFqx268bc1yS5ma/cXAytuZdz5t4xmQs9MKvz/3//7/c/Z2bOMESRDxa5f4wAuJe3LiX0I4BFAD4IOVQKwh+OtKN05YE7dHs7ltDhLQAlITf+jj0NO+QyDlzqeARwcXGZz7gl9IgDvR1DUOi3Tfb+Eim6Pe16b91/f8qjlulezAUwDtUD6Bzd823F4TQHLN1zrUUOcHazHYBdVACbwp8Ek6ftAAg8AXVWnvPGXwl/0XS8GQDrJCw0Zkpg8uQmY29J/FZW2v8Z15x3jaIBBLzoXl0+c9b0BxCM7ycmj280AkCqqWiK9ATgzVoicWLjNxR6TQlMHm0xAhBcXt585LpJLMh5N9L8hcgbJk0mI2YAED0VzZEak1iQ88kjLRcA1Jo0mTjcbFyBzMFDh5whRJJMvDaJFjJfocmVXqnTBKnNRoeJg012ADZqExDDxC8+AMYV1a4YEwca7SL9dDd4xZzVmfh5wziW89MFu1jG968vcoC9wQCQuC/gCYFhu97lilKVJ3xJcJqNDuM/NRS+AtLaKZ2nz9kUtIlJ7GuYIw8PIXxsimd8z7oCAfh4ys5TxjOLyci78/HdDY2gjmXN+5djxn/IAORNQV2Z2nVmhV+DpvjY7rqlkHPbFMdYd33e3kfEpb9fsn/egu6rg6Zifubj39fvkdBpymHsu7WFAWTOGZcGXafxo/2nCv5WEMDYrvqvCKXPQmVmgK66AABGygwIeEUankLmapUA0pfVYKwzMACrgkEHMfrtGnNPgq4aoB6j21fbAdhFBWjNTorRbZYAdnqFPJCtK4wGpu/46JZVIe2tHQ+j7bVFDtBaGxc02Y43fFHs31x9DeDyCbeW5z7gm6bqeSzBQwBVEw6Rh4GRf2TxlhWzhlnSDvETEJMAeFm0RCiVtYaYypYAyCM45vsg3WwC2bWoFMQx60HA86gxZULxl1LP781FSp+D9O3Hd0Ieq2yVoqd3FwLcCuBrAKVWSZmVDNfQn7/OxlCqDUQ9AONnZegA3rZTL/o+xKAaIW0CMDN7m9P3WMiHnt8sg1dWA2ELgPn/sSv9HnqA0RWRiGd96fdVK6BPIf0Fj9VFA5Bro/wPMNG30D+7IDIikOZzUwAAAABJRU5ErkJggg==',
            link: { type: 'mine_order', tab: 4 },
            show: true,
          },
          {
            count: '99',
            status: '退款/售后',
            title: '退款/售后',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAABxFJREFUaEPtWW1wVNUZfp5zdzeYzw4onbEdLExhRpfqj1JAaeuunXGo0+kAM8USO0BQitBSYxRBw2YvCYnFsaRNIUAgKYgfdJiOWoma+iOxOm3R6ThMWfxAHMsoOhMKJCSbsNl73869+5GQZDd3d0McZjh/spP7fjzPed9zznveQ1zlg1c5flwj8FVH8FoE0kUg+ILcqAELASwQ4lYCNwGYDEAB6IbgMyiEIDgKE21VpQxlGtFxj4Cui+LNWEwT60D44mCd4goJsFcUmvWl7HGiNK4EthySHwN4msAtTpynkTkrQPVxhcbDS2mkszUuBILPSrFyoZHAfTkCH67+rgn8Qv85P0plN2cCNc/JTFPhCIFZ4ww+Ya5bgNLgMraOZj8nAtUH5Ta45A0IbrhC4GNmiSjAFVXL+PxwP1kT0J/rn0F4/gHg61cUfMJ4jMSSYClfGeovKwL6HslngXkUwOwJAT/o5KIoNVcv5QeDvLJAoD9r7KTIuixUc1BJzvV78k01V/czGsuuFOOt92VW1MRd8OCIfyY/S4gFnpF5GgwrdazDKN04TcpfRGjtTFPHQk6gF4BHAPdYsjBlg77S/XRKAkfflylhA6dIlAjweVEfps+ZwwFLQX9moB1iH1BphxD1W5a7K/SW/hnQXO2ATEupQDbD0H5t0lisKCMW6nA9Ic/392nTt61h16gRaA/JIwRshiCMSW5Mnj+T3YEDA7drAmv2xxwCdtLU5uur+EnwQHgaxSLBGSPAgDu3rNDWgxT9wEA1BIExjccENuor3U+NSqAjJO8A+F7c0Gs+L++xZ39/pAWCMocOLLHTENOvr5r0SXBPeBo9FgkMkiB26Cs9623bzZHlorGFIppD+6f0Ms+3RxBoOyZT81z4Mrk+iOW+W3hQbxeXfBrthMjXHDqIiVFO0/D8SH+AH1sk4E6S2LFl1RDwCtbkOAUfTw58dwSB9hOyiIIX4yBF9eEbP5zDLwL7wgsUtbczAp8UljMRA/4nfznpI4uEuLQV1ffn1Vifq/ZFVlBJc6bgY6bV4yMIdByXAIjquO9PfV5Oj4W4v0KEv8uOgKUlZyJmjETCRlVzZDVFdjvY0VK5fWkEgTdDsk+A++Mar/u8tCpMVDX1N5FYnT0BQCBfKKq79QfyjutN/T8V2pEeaztO6VKAD0emUEheJLAornXQ5+Vy63egqb+VgL2YnQwhDAr+SuK/Q+WjUfOF2rX57+hN4TuE6meJb6a9ttRiQkqc2LdjSly0CVgLt8uFC0u9jHSE5DXEblEQYI/fywdtArv7/k7iB06NA9hVvea6jE7rqr2X7oVpHsrAB9hxXLaDeBjAGU1wj0HoGIxAi89LO52qdoffAvD9y42nLaW+FDIoEj09VEdE/ad2bf7nlTv6bqLLuDnxTRmqBAqPgExs3454sCMkESSOb+IsBacEmBePQKvfy5/YEdjV1wqI4xRK7d0sq1lbuD/QGC4nUe8IZWqhHnackDYI7k4hc9LnpX1R2byzN+NFLHYWopuxv/Ehv6pZV/j85l29aymoS/6bLIDI2HXQoIL160O+/YEUDRhoI3D7cBIkTLfghju8PBfY0VMBZrKNyps9l4wl9RUl55zMsq6Lx7i+dxvIcifytgwR20bTkRDBMv9sHgr8sXsBqDI4yPiG5s5fpK9h2Akgq5thXB+uAeQJJ/LxFH8iuQpTkRDgZb+Xi3RdXMaU3k4BMiklLhE4PxSQCSmvXV/0580NvatJSRyYEEEBiCKn4C05k7y8lGgPSaECjghw5xBDUVMw567ZPLb5Dz0toGRSzI3AY0LK6n5TvL+yoaucUFkvYhIf16wvmjlaMVcwyYXWoSQS50FlQ3g+xfhnJrM0XNY0pazu4eL9lfVd5VTZExBwY+1DhaOX023H5DISBB6808s99m5U390BXhahzPiYWLW1ovhPNgEy2whcGLiu6FspLzQWoldPSl7BJdxrCs75v8MjCZSP/75rnhI6uVKOToxsqC0veqhye3cTsqytKHh0a0WxXVhm1ZWo3N61E0BGZULyFLBLGFhdheQpnFkI8Z67u3iuro9xqU9nVNfP5EcK84+SnPC2iimc++Sjxbm1VSxym+ovzNAM+348MY0tIAoxl9RumJx7YysRnY2/PX+b0vg3QuJtk6wy0kkG2a3Fug0l49daTC7qus5Z1FyvgFeuuWuYUrpt0+Txb+4mSAQb/lcc6eO4t9dF+K6m1H1bHys5mSpM4xrzTU91LaRpWP0kr5O8SAPqLGBWn/z3lMbDhyfggWMoEKso68/rWgwY6wj6AHF85xUwJKbsvThgNDfqUyf+iWn4jD4W7LxRedRCZT3yAbeCtB/5BKIIWA8XVs/1BIF/mQpt2zZN+eof+XJJnWx0x3UNZAMgV51rBHKdwVz1r/oI/B85LZk3Ruc6YQAAAABJRU5ErkJggg==',
            link: { type: 'mine_order', tab: 5 },
            show: true,
          },
        ],
      },
      /* 我的订单组件 */
      /* 列表导航组件 */
      {
        id: 'n16275389435643',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '我的收藏',
            icon: require('@/assets/v2_image/icon_mine_36_2.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'collection',
              type_name: '我的收藏',
              id: 0,
              url: '',
              title: '我的收藏',
            },
          },
          {
            content: '电子凭证',
            icon: require('@/assets/v2_image/icon_mine_36_15.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_electronic',
              type_name: '电子凭证',
              id: 0,
              url: '',
              title: '电子凭证',
              target: '_self',
            },
          },
          {
            content: '我的优惠券',
            icon: require('@/assets/v2_image/icon_mine_36_3.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_coupon',
              type_name: '我的优惠券',
              id: 0,
              url: '',
              title: '我的优惠券',
            },
          },
          {
            content: '我的储值',
            icon: require('@/assets/v2_image/icon_mine_36_16.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'balance',
              type_name: '我的储值',
              id: 0,
              url: '',
              title: '我的储值',
            },
          },
        ],
      },
      {
        id: 'n16275390208640',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '积分商城',
            icon: require('@/assets/v2_image/icon_mine_36_8.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'integral_mall',
              type_name: '积分商城',
              id: 0,
              url: '',
              title: '积分商城',
            },
          },
          {
            content: '佣金中心',
            icon: require('@/assets/v2_image/icon_mine_36_9.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'page_distribution',
              type_name: '佣金中心',
              id: 0,
              url: '',
              title: '佣金中心',
            },
          },
          {
            content: '队长中心',
            icon: require('@/assets/v2_image/icon_mine_36_27.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'page_captain_center',
              type_name: '队长中心',
              id: 0,
              url: '',
              title: '队长中心',
              target: '_self',
            },
          },
        ],
      },
      {
        id: 'n162821375680199',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '我的拼团',
            icon: require('@/assets/v2_image/icon_mine_36_12.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_order_group',
              type_name: '我的拼团',
              id: 0,
              url: '',
              title: '我的拼团',
            },
          },
          {
            content: '我的砍价',
            icon: require('@/assets/v2_image/icon_mine_36_13.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_order_bargain',
              type_name: '我的砍价',
              id: 0,
              url: '',
              title: '我的砍价',
            },
          },
          {
            content: '我的秒杀',
            icon: require('@/assets/v2_image/icon_mine_36_10.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_order_seckill',
              type_name: '我的秒杀',
              id: 0,
              url: '',
              title: '我的秒杀',
              target: '_self',
            },
          },
        ],
      },
      {
        id: 'n162821382398025',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '我的表单',
            icon: require('@/assets/v2_image/icon_mine_36_18.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_form',
              type_name: '我的表单',
              id: '',
              url: '',
              title: '我的表单',
            },
          },
          {
            content: '我的预约',
            icon: require('@/assets/v2_image/icon_mine_36_11.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'services_order_list',
              type_name: '我的预约',
              id: 0,
              url: '',
              title: '我的预约',
              target: '_self',
            },
          },
        ],
      },
      {
        id: 'n162821388720064',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '我的评价',
            icon: require('@/assets/v2_image/icon_mine_36_4.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'mine_evaluate',
              type_name: '我的评价',
              id: 0,
              url: '',
              title: '我的评价',
            },
          },
        ],
      },
      {
        id: 'n162821390011928',
        type: 'diy_list',
        num: 5,
        order: 'create_time',
        sort: 'desc',
        data_type: '',
        show_more: true,
        more_text: '',
        right_icon:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAxElEQVRYR+3WMQ4CIRCF4YFLbWVvaeUhhkPoIZi5hI2Vq57OkGg/8B7ZxCw14f8CyYQkG6+0cV92QPgGzOxYSnmynywEqLUuOee3iLxU9cxEhABmdk0pXb7hGxMRArTwLEQYMAvRBZiB6AawEUMAJmIYwEJAAAYCBqAICgBB/AcAmZLwDSDx9nQQAI1DAEZ8GMCKDwGY8W4AO94FmBEPA2bFwwB3P4jIKiIP5n8wDGgb3f2kqnfmj7gLwA7/zoMmIQO1Az5cVnYhCEUwBAAAAABJRU5ErkJggg==',
        ids: [],
        list: [
          {
            content: '收货地址',
            icon: require('@/assets/v2_image/icon_mine_36_5.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'address',
              type_name: '收货地址',
              id: 0,
              url: '',
              title: '收货地址',
            },
          },
          {
            content: '联系客服',
            icon: require('@/assets/v2_image/icon_mine_36_6.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'chat',
              type_name: '联系客服',
              id: 0,
              url: '',
              title: '联系客服',
            },
          },
          {
            content: '关于我们',
            icon: require('@/assets/v2_image/icon_mine_36_7.png'),
            show_icon: true,
            hidden: true,
            link: {
              type: 'about',
              type_name: '关于我们',
              id: 0,
              url: '',
              title: '关于我们',
            },
          },
        ],
      },
      /* 列表导航组件 */
    ],
    page: {
      name: '个人中心',
      nv_color: '#ffffff',
      bg_color: '#f4f4f4',
      text_color: 'black',
      bg_img: '',
    },
  },
  issystem: 0,
  page_flag: 'page',
}
