import PageBase from '@/wegts/system/PageBase'
export default class SystemGoodClassify extends PageBase {
  constructor(props) {
    super(defaultPageData)
  }
}

const defaultPageData = {
  id: 0,
  title: '商品分类',
  tmpl_type: 2,
  status: 1,
  data: {
    styles: {
      css: {
        n162753114259639: {
          height: {
            value: 554,
          },
          bg: {
            type: 'color',
            color: {
              color: 'rgba(255, 255, 255,1.00)',
              alpha: '100',
            },
          },
          cache_height: {
            value: 554,
          },
        },
        n162753143443731: {
          bg: {
            type: 'color',
            color: {
              color: 'rgba(255, 255, 255,1.0)',
              alpha: '100',
            },
          },
        },
        'n162753143443731 .search_box': {
          bg: {
            type: 'color',
            color: {
              color: 'rgba(238, 238, 238,1.0)',
              alpha: '100',
            },
          },
          font: {
            family: 'Arial',
            size: '14',
            color: '#888',
            isB: false,
            isI: false,
            isU: false,
          },
          br: {
            now: 'all',
            tl: 16,
            tr: 16,
            bl: 16,
            br: 16,
            radius: 16,
          },
        },
      },
    },
    all_data: [
      {
        id: 'n162753143443731',
        type: 'diy_search',
        search_tip: '请输入商品名称',
        search_keyword: '',
        search_type: ['goods'],
      },
      {
        id: 'n162753114259639',
        type: 'diy_good_class',
        level: 1,
        style: 1,
        auto_height: 1,
        list: [],
        classifyList: [],
        num: 20,
        sort: '',
        order: '',
      },
    ],
    page: {
      name: '商品分类',
      nv_color: '#ffffff',
      bg_color: '#f4f4f4',
      text_color: 'black',
      bg_img: '',
    },
  },
  issystem: 0,
  page_flag: 'page',
}
