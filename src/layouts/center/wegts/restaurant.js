/* 公用链接页面 */
import PageIndex from '@/wegts/common/PageIndex'
import PageArticles from '@/wegts/common/PageArticles'
import PageArticlesClass from '@/wegts/common/PageArticlesClass'
import PageArticlesInfo from '@/wegts/common/PageArticlesInfo'
import PageImageClass from '@/wegts/common/PageImageClass'
import PageImageInfo from '@/wegts/common/PageImageInfo'
import PagePhone from '@/wegts/common/PagePhone'
import PageMap from '@/wegts/common/PageMap'

/* v2 独有链接页面 */
import PageMember from '@/wegts/v2/PageMember'
import PageShopCar from '@/wegts/v2/PageShopCar'
import PageSalesList from '@/wegts/v2/PageSalesList'
import PageSalesDetail from '@/wegts/v2/PageSalesDetail'
import PageFactList from '@/wegts/v2/PageFactList'
import PageFactDetail from '@/wegts/v2/PageFactDetail'
import PageFactCreate from '@/wegts/v2/PageFactCreate'
import PageGrassList from '@/wegts/v2/PageGrassList'
import PageGrassDetail from '@/wegts/v2/PageGrassDetail'
import PageAgentIndex from '@/wegts/v2/PageAgentIndex'
import PageShareHolderIndex from '@/wegts/v2/PageShareHolderIndex'
import PageMemberShip from '@/wegts/v2/PageMemberShip'
import PageCaptainCenter from '@/wegts/v2/PageCaptainCenter'
import PageGoodsCategoryLevel1Style1 from '@/wegts/v2/PageGoodsCategoryLevel1Style1'
import PageGoodsCategoryLevel1Style2 from '@/wegts/v2/PageGoodsCategoryLevel1Style2'
import PageGoodsCategoryLevel1Style3 from '@/wegts/v2/PageGoodsCategoryLevel1Style3'
import PageGoodsCategoryLevel2Style1 from '@/wegts/v2/PageGoodsCategoryLevel2Style1'
import PageGoodsCategoryLevel2Style2 from '@/wegts/v2/PageGoodsCategoryLevel2Style2'
import PageGoodsCategoryLevel2Style3 from '@/wegts/v2/PageGoodsCategoryLevel2Style3'
import PageGoodsCategoryLevel3Style1 from '@/wegts/v2/PageGoodsCategoryLevel3Style1'
import PageGoodsList from '@/wegts/v2/PageGoodsList'
import PageCouponList from '@/wegts/v2/PageCouponList'
import PageGoods from '@/wegts/v2/PageGoods'
import PageGroup from '@/wegts/v2/PageGroup'
import PageIntegral from '@/wegts/v2/PageIntegral'
import PageDistribution from '@/wegts/v2/PageDistribution'
import PageEntry from '@/wegts/v2/PageEntry'
import PageSign from '@/wegts/v2/PageSign'
import PageBalance from '@/wegts/v2/PageBalance'
import PageVideo_list from '@/wegts/v2/PageVideo_list'
import PageLive_list from '@/wegts/v2/PageLive_list'
import live_detail from '@/wegts/v2/PageLive_detail'
import PageBargainingList from '@/wegts/v2/PageBargainingList'
import PageSeckillList from '@/wegts/v2/PageSeckillList'

/* v2 所有链接页面 */
export const PageList = {
  member: PageMember,
  shop_car: PageShopCar,
  sales_list: PageSalesList,
  sales_detail: PageSalesDetail,
  fact_list: PageFactList,
  fact_detail: PageFactDetail,
  fact_create: PageFactCreate,
  grass_list: PageGrassList,
  grass_detail: PageGrassDetail,
  page_shareholder_index: PageShareHolderIndex,
  page_agent_index: PageAgentIndex,
  membership: PageMemberShip,
  page_captain_center: PageCaptainCenter,
  page_goods_category_level1_style1: PageGoodsCategoryLevel1Style1,
  page_goods_category_level1_style2: PageGoodsCategoryLevel1Style2,
  page_goods_category_level1_style3: PageGoodsCategoryLevel1Style3,
  page_goods_category_level2_style1: PageGoodsCategoryLevel2Style1,
  page_goods_category_level2_style2: PageGoodsCategoryLevel2Style2,
  page_goods_category_level2_style3: PageGoodsCategoryLevel2Style3,
  page_goods_category_level3_style1: PageGoodsCategoryLevel3Style1,
  article_list: PageArticles,
  article_class: PageArticlesClass,
  article: PageArticlesInfo,
  image_class: PageImageClass,
  images: PageImageInfo,
  phone: PagePhone,
  map: PageMap,
  page_goods_list: PageGoodsList, //商品列表
  page_coupon_list: PageCouponList, //优惠卷列表
  goods: PageGoods, //商品
  activity_group_list: PageGroup, //拼团列表
  integral_mall: PageIntegral, //积分商城
  page_distribution: PageDistribution, //佣金中心
  live_entry: PageEntry, //主播入驻
  mine_sign: PageSign, //签到
  balance: PageBalance, //储值
  video_list: PageVideo_list, //视频列表
  live_list: PageLive_list, //直播列表
  live_detail: live_detail, //直播详情
  activity_bargain_list: PageBargainingList, //砍价列表
  activity_seckill_list: PageSeckillList, //秒杀列表
}

/* v2 选项卡 使用的链接页面 */
export const TabPageList = {
  page: PageIndex,
  video_list: PageVideo_list, //视频列表
  live_list: PageLive_list, //直播列表
  page_goods_list: PageGoodsList, //商品列表
  fact_list: PageFactList, //爆料列表
  grass_list: PageGrassList, //种草列表
}
