/* 公用链接页面 */
import PageIndex from '@/wegts/common/PageIndex'
import PageArticles from '@/wegts/common/PageArticles'
import PageArticlesClass from '@/wegts/common/PageArticlesClass'
import PageArticlesInfo from '@/wegts/common/PageArticlesInfo'
import PageImageClass from '@/wegts/common/PageImageClass'
import PageImageInfo from '@/wegts/common/PageImageInfo'
import PageServices from '@/wegts/common/PageServices'
import PageServicesClass from '@/wegts/common/PageServicesClass'
import PageServicesInfo from '@/wegts/common/PageServicesInfo'
import PagePhone from '@/wegts/common/PagePhone'
import PageMap from '@/wegts/common/PageMap'

/* 轻站 独有链接页面 */
import PageProduct from '@/wegts/qingzhan/PageProduct'
import PageProductClass from '@/wegts/qingzhan/PageProductClass'
import PageProductInfo from '@/wegts/qingzhan/PageProductInfo'
import PageServicesOrderCreate from '@/wegts/qingzhan/PageServicesOrderCreate'
import PageServicesOrder from '@/wegts/qingzhan/PageServicesOrder'
import PageServicesOrderInfo from '@/wegts/qingzhan/PageServicesOrderInfo'
import PageRecruit from '@/wegts/qingzhan/PageRecruit'
import PageRecruitInfo from '@/wegts/qingzhan/PageRecruitInfo'
import PageVote from '@/wegts/qingzhan/PageVote'
import PageMember from '@/wegts/qingzhan/PageMember'

/* 轻站 所有链接页面 */
export const PageList = {
  article_list: PageArticles,
  article_class: PageArticlesClass,
  article: PageArticlesInfo,
  product_list: PageProduct,
  product_class: PageProductClass,
  product: PageProductInfo,
  services_list: PageServices,
  services_class: PageServicesClass,
  services: PageServicesInfo,
  services_order_create: PageServicesOrderCreate,
  services_order: PageServicesOrder,
  services_order_info: PageServicesOrderInfo,
  recruit_list: PageRecruit,
  recruit: PageRecruitInfo,
  vote_list: PageVote,
  member: PageMember,
  image_class: PageImageClass,
  images: PageImageInfo,
  phone: PagePhone,
  map: PageMap,
}

/* 轻站 选项卡 使用的链接页面 */
export const TabPageList = {
  page: PageIndex,
  article_class: PageArticlesClass,
  product_class: PageProductClass,
  services_class: PageServicesClass,
  recruits: PageRecruit,
}
