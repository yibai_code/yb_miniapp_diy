/* 公共组件 */
import Block from '@/wegts/common/Block'
import BlankLine from '@/wegts/common/BlankLine'
import ImageGroup from '@/wegts/common/ImageGroup'
import ImageGroup2 from '@/wegts/common/ImageGroup2'
import Articles from '@/wegts/common/Articles'
import Articles2 from '@/wegts/common/Articles2'
import Concern from '@/wegts/common/Concern'
import Map from '@/wegts/common/Map'
import AnnounCement from '@/wegts/common/AnnounCement'
import Tab from '@/wegts/common/Tab'
import QqAdvert from '@/wegts/common/QqAdvert'

/* 轻站组件 */
import Headline from '@/wegts/qingzhan/HeadLine'
import GridNav from '@/wegts/qingzhan/GridNav'
import Forms from '@/wegts/qingzhan/Form'
import Banner from '@/wegts/qingzhan/Banner'
import MyVideo from '@/wegts/qingzhan/Video'
import Recruit from '@/wegts/qingzhan/Recruits'
import Vote from '@/wegts/qingzhan/Vote'
import Product from '@/wegts/qingzhan/Product'
import Product2 from '@/wegts/qingzhan/Product2'
import Product3 from '@/wegts/qingzhan/Product3'
import Comment from '@/wegts/qingzhan/Comment'
import Services from '@/wegts/qingzhan/Services'
import Search from '@/wegts/qingzhan/Search'
import WxAdvert from '@/wegts/qingzhan/WxAdvert'
import RichText from '@/wegts/qingzhan/RichText'

export const Components = {
  headline: Headline,
  block: Block,
  navigation: GridNav,
  blankline: BlankLine,
  banner: Banner,
  image_group1: ImageGroup,
  image_group2: ImageGroup2,
  recruit: Recruit,
  vote: Vote,
  services1: Services,
  product1: Product2,
  product2: Product,
  product3: Product3,
  articles1: Articles,
  articles2: Articles2,
  gzh_concern: Concern,
  diy_comment: Comment,
  diy_form: Forms,
  diy_search: Search,
  diy_wx_advert: WxAdvert,
  diy_qq_advert: QqAdvert,
  diy_map: Map,
  announcement: AnnounCement,
  module_tabbar_list: Tab,
  my_video: MyVideo,
  rich_text: RichText,
}
