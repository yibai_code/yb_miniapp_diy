/* 公共组件 */
import Headline from '@/wegts/common/HeadLine'
import Block from '@/wegts/common/Block'
import BlankLine from '@/wegts/common/BlankLine'
import ImageGroup from '@/wegts/common/ImageGroup'
import ImageGroup2 from '@/wegts/common/ImageGroup2'
import Album1 from '@/wegts/common/Album'
import Album2 from '@/wegts/common/Album2'
import Album3 from '@/wegts/common/Album3'
import Album4 from '@/wegts/common/Album4'
import Articles from '@/wegts/common/Articles'
import Articles2 from '@/wegts/common/Articles2'
import Concern from '@/wegts/common/Concern'
import Map from '@/wegts/common/Map'
import AnnounCement from '@/wegts/common/AnnounCement'
import Tab from '@/wegts/common/Tab'
import QqAdvert from '@/wegts/common/QqAdvert'

/* v2组件 */
import GridNav from '@/wegts/v2/GridNav'
import GridNav2 from '@/wegts/v2/GridNav2'
import GridNav3 from '@/wegts/v2/GridNav3'
import GridNav4 from '@/wegts/v2/GridNav4'
import Forms from '@/wegts/v2/Form'
import Banner from '@/wegts/v2/Banner'
import MyVideo from '@/wegts/v2/Video'
import Search from '@/wegts/v2/Search'
import WxAdvert from '@/wegts/v2/WxAdvert'
import Services from '@/wegts/v2/Services'
import Services2 from '@/wegts/v2/Services2'
import Bargaining from '@/wegts/v2/Bargaining'
import Bargaining2 from '@/wegts/v2/Bargaining2'
import Bargaining3 from '@/wegts/v2/Bargaining3'
import RichText from '@/wegts/v2/RichText'
import Seckill from '@/wegts/v2/Seckill'
import Seckill2 from '@/wegts/v2/Seckill2'
import Seckill3 from '@/wegts/v2/Seckill3'
import Seckill4 from '@/wegts/v2/Seckill4'
import Class from '@/wegts/v2/Class'
import Shop from '@/wegts/v2/Shop'
import Shop2 from '@/wegts/v2/Shop2'
import Shop3 from '@/wegts/v2/Shop3'
import Integral from '@/wegts/v2/Integral'
import Integral2 from '@/wegts/v2/Integral2'
import Integral3 from '@/wegts/v2/Integral3'
import Coupon from '@/wegts/v2/Coupon'
import Coupon2 from '@/wegts/v2/Coupon2'
import Groups from '@/wegts/v2/Groups'
import Groups2 from '@/wegts/v2/Groups2'
import Groups3 from '@/wegts/v2/Groups3'
import Groups4 from '@/wegts/v2/Groups4'
import Groups5 from '@/wegts/v2/Groups5'
import Vine from '@/wegts/v2/Vine'
import Vine2 from '@/wegts/v2/Vine2'
import Vine3 from '@/wegts/v2/Vine3'
import Live from '@/wegts/v2/Live'
import Live2 from '@/wegts/v2/Live2'
import Live3 from '@/wegts/v2/Live3'
import Live4 from '@/wegts/v2/Live4'
import Live5 from '@/wegts/v2/Live5'
import TencentLive from '@/wegts/v2/TencentLive'
import TencentLive2 from '@/wegts/v2/TencentLive2'
import TencentLive3 from '@/wegts/v2/TencentLive3'
import TencentLive4 from '@/wegts/v2/TencentLive4'
import TencentLive5 from '@/wegts/v2/TencentLive5'
import Designer from '@/wegts/v2/Designer'
import Instructor from '@/wegts/v2/Instructor'
import Special from '@/wegts/v2/Special'
import MyAudio from '@/wegts/v2/Audio'
import HotelRoom from '@/wegts/v2/HotelRoom'
import House from '@/wegts/v2/House'
import Decoration from '@/wegts/v2/Decoration'
import HotelCheckIn from '@/wegts/v2/HotelCheckIn'
import HouseType from '@/wegts/v2/HouseType'
import DiyList from '@/wegts/v2/list'
import DiyMember from '@/wegts/v2/DiyMember'
import DiyGoodClass from '@/wegts/v2/GoodClassify'
import diyNewCoupon from '@/wegts/v2/diy_new_coupon'
import diyOrder from '@/wegts/v2/diy_order'
import diyNewCoupon1 from '@/wegts/v2/diy_new_coupon1'
import diyNewCoupon2 from '@/wegts/v2/diy_new_coupon2'
import Vote from '@/wegts/v2/Vote'
export const Components = {
  vote: Vote,
  headline: Headline,
  block: Block,
  navigation: GridNav,
  navigation2: GridNav2,
  navigation3: GridNav3,
  navigation4: GridNav4,
  blankline: BlankLine,
  image_group1: ImageGroup,
  image_group2: ImageGroup2,
  album: Album1,
  album2: Album2,
  album3: Album3,
  album4: Album4,
  articles1: Articles,
  articles2: Articles2,
  gzh_concern: Concern,
  diy_form: Forms,
  diy_qq_advert: QqAdvert,
  diy_map: Map,
  announcement: AnnounCement,
  module_tabbar_list: Tab,
  my_video: MyVideo,
  diy_wx_advert: WxAdvert,
  banner: Banner,
  diy_search: Search,
  services1: Services,
  services2: Services2,
  diy_bargain: Bargaining,
  bargaining2: Bargaining2,
  bargaining3: Bargaining3,
  rich_text: RichText,
  diy_seckill: Seckill,
  seckill2: Seckill2,
  seckill3: Seckill3,
  seckill4: Seckill4,
  diy_course: Class,
  diy_series: Special,
  diy_goods_style1: Shop,
  diy_goods_style2: Shop2,
  diy_goods_style3: Shop3,
  diy_coupon_style1: Coupon,
  diy_coupon_style2: Coupon2,
  diy_integral: Integral,
  diy_integral2: Integral2,
  diy_integral3: Integral3,
  diy_group: Groups,
  diy_group2: Groups2,
  diy_group3: Groups3,
  diy_group4: Groups4,
  diy_group5: Groups5,
  video_list: Vine,
  video_list2: Vine2,
  video_list3: Vine3,
  live_list: Live,
  live_list2: Live2,
  live_list3: Live3,
  live_list4: Live4,
  live_list5: Live5,
  tencent_live_list: TencentLive,
  tencent_live_list2: TencentLive2,
  tencent_live_list3: TencentLive3,
  tencent_live_list4: TencentLive4,
  tencent_live_list5: TencentLive5,
  building_list: House,
  house_type_list: HouseType,
  decorate_list: Decoration,
  designer_list: Designer,
  diy_lecturer: Instructor,
  diy_audio: MyAudio,
  diy_room_list: HotelRoom,
  hotelCheckIn: HotelCheckIn,
  diy_list: DiyList,
  diy_member: DiyMember,
  diy_good_class: DiyGoodClass,
  diy_new_coupon: diyNewCoupon,
  diy_order: diyOrder,
  diy_new_coupon1: diyNewCoupon1,
  diy_new_coupon2: diyNewCoupon2,
}
