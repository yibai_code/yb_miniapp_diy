/*
// mock 官网: http://mockjs.com/examples.html
import { mock } from 'mockjs'
import { handleRandomImage } from '../utils'
export default [
  {
    url: '/znhk/znhkList/tableList',
    type: 'post',
    response() {
      return {
        code: 0,
        msg: 'success',
        data: {
          list: mock({
            'data|10': [
              {
                'id|+1': 1, //id
                'class_name|1': ['@cname', '匿名网友'], //名字
                'visit_page|1': [
                  '投票详细页面',
                  '投票列表',
                  '首页',
                  '服务列表',
                  '招聘列表',
                ], //访问页面
                'platform_source|1': ['微信小程序', '手机站'], //平台来源
                'tel|1': ['暂无', /^1[385][1-9]\d{8}/],
                create_time: '@datetime', //日期
                image: handleRandomImage(150, 150),
              },
            ],
          }).data,
          totalCount: 235,
        },
      }
    },
  },
  {
    url: '/znhk/znhkList/searchBox',
    type: 'post',
    response() {
      return {
        code: 0,
        msg: 'success',
        totalCount: 999,
        data: {
          'a-title': {
            label: '文章标题',
            type: 'text',
            operate: 'like',
            value: '',
          },
          'a-is_recommend': {
            label: '推荐',
            type: 'select',
            operate: '=',
            values: [
              {
                label: '未选择',
                value: '',
              },
              {
                label: '推荐',
                value: '1',
              },
              {
                label: '不推荐',
                value: '0',
              },
            ],
            value: '',
          },
        },
      }
    },
  },
  {
    url: '/znhk/znhkList/del',
    type: 'post',
    response() {
      return {
        code: 0,
        msg: '模拟删除成功',
      }
    },
  },
  {
    url: '/znhk/znhkList/valSwitch',
    type: 'post',
    response() {
      return {
        code: 0,
        msg: '模拟操作成功',
      }
    },
  },
]
*/
