const path = require('path')
const webpack = require('webpack')
const TerserPlugin = require('terser-webpack-plugin')
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const productionGzipExtensions = ['html', 'js', 'css', 'svg']

const resolve = (dir) => {
  return path.join(__dirname, dir)
}
console.log(process.argv)
const isV2 = process.argv.some((item) => {
  return item === '--v2'
})
const isQingzhan = process.argv.some((item) => {
  return item === '--qingzhan'
})
const isVideo = process.argv.some((item) => {
  return item === '--video'
})
const isMerchant = process.argv.some((item) => {
  return item === '--merchant'
})
const isCommunity = process.argv.some((item) => {
  return item === '--community'
})
const isRestaurant = process.argv.some((item) => {
  return item === '--restaurant'
})
const isChainstore = process.argv.some((item) => {
  return item === '--chainstore'
})
const isPlatform = process.argv.some((item) => {
  return item === '--platform'
})

console.log('isV2: ', isV2)
console.log('isQingzhan: ', isQingzhan)
console.log('isVideo: ', isVideo)
console.log('isMerchant: ', isMerchant)
console.log('isPlatform: ', isPlatform)

module.exports = {
  outputDir: process.env.NODE_ENV === 'development' ? 'dev' : 'dist',
  publicPath: process.env.VUE_APP_URL,
  devServer: {
    hotOnly: true,
    hot: true,
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': resolve('src'),
      },
    },
    externals: {
      CkEditor: 'window.CKEDITOR',
      vue: 'Vue',
      vuex: 'Vuex',
      axios: 'axios',
      'element-ui': 'ELEMENT',
    },
    optimization:
      process.env.NODE_ENV === 'production'
        ? {
            minimize: true,
            minimizer: [
              new TerserPlugin({
                terserOptions: {
                  ecma: undefined,
                  warnings: false,
                  parse: {},
                  compress: {
                    drop_console: true,
                  },
                },
              }),
            ],
          }
        : {},
  },
  chainWebpack(config) {
    config.module
      .rule('scss')
      .use('js-conditional-compile-loader')
      .loader('js-conditional-compile-loader')
      .options({
        isQingzhan: isQingzhan, // enabled when running command: `npm run build --myflag`
        isV2: isV2,
        isVideo: isVideo,
        isMerchant: isMerchant,
        isPlatform: isPlatform,
        isCommunity: isCommunity,
        isRestaurant: isRestaurant,
        isChainstore: isChainstore,
        isDebug: process.env.NODE_ENV === 'development',
        isProduction: process.env.NODE_ENV === 'production',
      })
      .end()
    config.module
      .rule('js-conditional-compile-loader')
      .test(/\.(vue|js|scss|css)$/)
      .use('js-conditional-compile-loader')
      .loader('js-conditional-compile-loader')
      .options({
        isQingzhan: isQingzhan, // enabled when running command: `npm run build --myflag`
        isV2: isV2,
        isVideo: isVideo,
        isMerchant: isMerchant,
        isPlatform: isPlatform,
        isCommunity: isCommunity,
        isRestaurant: isRestaurant,
        isChainstore: isChainstore,
        isDebug: process.env.NODE_ENV === 'development',
        isProduction: process.env.NODE_ENV === 'production',
      })
      .end()
    config.module.rules.delete('svg') // 重点:删除默认配置中处理svg,
    config.module
      .rule('svg-inline-loader')
      .test(/\.svg$/)
      .use('svg-inline-loader')
      .loader('svg-inline-loader')
      .options({
        removeTags: true,
        removingTags: ['p-id', 'id', 'class', 'title', 'desc', 'defs', 'style'],
        removingTagAttrs: [
          'fill',
          't',
          'version',
          'p-id',
          'id',
          'class',
          'title',
          'desc',
          'defs',
          'style',
          'width',
          'height',
          'xmlns',
          'xmlns:xlink',
        ],
      })
      .end()
    config.module
      .rule('raw-loader')
      .test(/\.(tmpl|txt)$/)
      .use('raw-loader')
      .loader('raw-loader')
      .end()
    config.when(process.env.NODE_ENV === 'development', (config) => {
      config.devtool('source-map')
    })
    config.when(process.env.NODE_ENV !== 'development', (config) => {
      config.performance.set('hints', false)
      config.devtool('none')
      config.optimization.splitChunks({
        chunks: 'all',
        cacheGroups: {
          libs: {
            name: 'chunk-libs',
            test: /[\\/]node_modules[\\/]/,
            priority: 10,
            chunks: 'initial',
          },
          elementUI: {
            name: 'chunk-elementUI',
            priority: 20,
            test: /[\\/]node_modules[\\/]_?element-ui(.*)/,
          },
          fortawesome: {
            name: 'chunk-fortawesome',
            priority: 20,
            test: /[\\/]node_modules[\\/]_?@fortawesome(.*)/,
          },
          commons: {
            name: 'chunk-commons',
            test: resolve('src/components'),
            minChunks: 3,
            priority: 5,
            reuseExistingChunk: true,
          },
        },
      })
      config
        .plugin('compression')
        .use(CompressionWebpackPlugin, [
          {
            filename: '[path].gz[query]',
            algorithm: 'gzip',
            test: new RegExp(
              '\\.(' + productionGzipExtensions.join('|') + ')$'
            ),
            threshold: 8192,
            minRatio: 0.8,
          },
        ])
        .end()
      config.module
        .rule('images')
        .use('image-webpack-loader')
        .loader('image-webpack-loader')
        .options({
          bypassOnDebug: true,
        })
        .end()
    })
  },
  runtimeCompiler: true,
  productionSourceMap: false,
  css: {
    extract: true,
    sourceMap: false,
    loaderOptions: {
      css: {
        // 这里的选项会传递给 css-loader
        importLoaders: 1,
      },
      less: {
        // 这里的选项会传递给 postcss-loader
        importLoaders: 1,
      },
      postcss: {
        importLoaders: 1,
      },
      scss: {
        additionalData(content, loaderContext) {
          const { resourcePath, rootContext } = loaderContext
          const relativePath = path.relative(rootContext, resourcePath)
          if (
            relativePath.replace(/\\/g, '/').indexOf('src/styles/variables/') <
            0
          ) {
            if (isQingzhan) {
              content = '@import "~@/styles/variables/qingzhan.scss";' + content
            } else if (isV2 || isCommunity || isRestaurant || isPlatform) {
              content = '@import "~@/styles/variables/v2.scss";' + content
            } else if (isVideo) {
              content = '@import "~@/styles/variables/video.scss";' + content
            } else if (isMerchant) {
              content = '@import "~@/styles/variables/merchant.scss";' + content
            } else if (isChainstore) {
              content = '@import "~@/styles/variables/merchant.scss";' + content
            }
          }
          if (
            relativePath
              .replace(/\\/g, '/')
              .indexOf(
                '@yb_fromwork/admin-base/src/styles/variables/variables.scss'
              ) < 0
          ) {
            content =
              '@import "~@yb_fromwork/admin-base/src/styles/variables/variables.scss";' +
              content
          }
          return content
        },
      },
    },
  },
}
