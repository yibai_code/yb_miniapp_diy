const _fs = require('fs')
const _path = require('path')
const cache = _path.join(_path.dirname(__dirname), 'node_modules/.cache')
function deleteFolder(path) {
  let files = []
  if (_fs.existsSync(path)) {
    files = _fs.readdirSync(path)
    files.forEach(function (file, index) {
      let curPath = path + '/' + file
      if (_fs.statSync(curPath).isDirectory()) {
        deleteFolder(curPath)
      } else {
        _fs.unlinkSync(curPath)
      }
    })
    _fs.rmdirSync(path)
  }
}
deleteFolder(cache)
